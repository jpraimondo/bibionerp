﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class Supplier:EntityBase
    {
        [MaxLength(50)]
        public string Name { get; set; } = string.Empty;

        public string Document { get; set; } = string.Empty;
        [MaxLength(50)]
        public string ContactPerson { get; set; } = string.Empty;
        [MaxLength(100)]
        public string Email { get; set; } = string.Empty;
        [MaxLength(10)]
        public string Phone { get; set; } = string.Empty;
        [MaxLength(50)]
        public string Address { get; set; } = string.Empty;
        [MaxLength(50)]
        public string Location { get; set; } = string.Empty;
    }
}
