﻿using EntityFrameworkCore.EncryptColumn.Attribute;
using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class Company:EntityBase
    {
        [MaxLength(100)]
        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;
        
        [MaxLength(50)]
        public string Document { get; set; } = string.Empty;
        
        [MaxLength(50)] 
        public string Address { get; set; } = string.Empty;
        
        [MaxLength(50)]
        public string Email { get; set; } = string.Empty;
        [MaxLength(50)]

        public string Phone { get; set; } = string.Empty;

        public int CustomerId { get; set; }

        public virtual Customer? Customer { get; set; }

        public ICollection<CompanyService>? CompanyServices { get; set; }
    
    
    }
}
