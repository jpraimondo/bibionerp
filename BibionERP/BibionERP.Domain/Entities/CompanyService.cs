﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BibionERP.Domain.Entities
{
    public class CompanyService
    {
        [Key,Column(Order = 0)]
        public int CompanyId { get; set; }

        [Key,Column(Order = 1)]
        public int ServiceId { get; set; }

        public virtual Company? Company { get; set; }
        public virtual Service? Service { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int HoursWeek { get; set; }

        public string RepeatDays { get; set; } = string.Empty;

        public decimal TotalCost { get; set; } = decimal.Zero;

        public decimal DiscountPercentage { get; set; } = decimal.Zero;

        public decimal FinalPrice { get; set; } = decimal.Zero;
    }
}
