﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class DeliveryNoteDetail :EntityBase
    {
        
        public int DeliveryNoteId { get; set; }
        public virtual DeliveryNote? DeliveryNote { get; set; }

        public int ItemId { get; set; }
        public virtual Item? Item { get; set;}

        public int Quantity { get; set; }

    }
}
