﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class DeliveryNote:EntityBase
    {
        public int EmployeeId { get; set; }
        public virtual Employee? Employee { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get;set; }

        public int CategoryDeliveryNoteId { get; set; }
        public virtual CategoryDeliveryNote? CategoryDeliveryNote { get; set; }
        
        public int WarehouseId { get; set; }
        public virtual Warehouse? Warehouse { get; set; }

        [MaxLength(50)]
        public string Location { get; set; } = string.Empty;

        public string Note { get; set; } = string.Empty;

        public virtual ICollection<DeliveryNoteDetail>? Details { get; set; }

    }
}
