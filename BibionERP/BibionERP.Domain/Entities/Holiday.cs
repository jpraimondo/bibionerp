﻿using BibionERP.Domain.Enums;

namespace BibionERP.Domain.Entities
{
    public class Holiday:EntityBase
    {

        public int EmployeeId { get; set; }
        public virtual Employee? Employee { get; set; }

        public HolidayType HolidayType { get; set; } = HolidayType.ANNUALLEAVE;

        public DateTime? StartHoliday { get; set; }
        public DateTime? EndHoliday { get; set; } = DateTime.MinValue;

        public int ApproverId { get; set; }
        public virtual ApplicationUser? Approver { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public HolidayStatus HolidayStatus { get; set; } = HolidayStatus.APPROVE;

        public string Details { get; set; } = string.Empty;



    }
}
