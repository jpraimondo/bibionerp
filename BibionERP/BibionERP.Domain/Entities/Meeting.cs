﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class Meeting: EntityBase    
    {

        public IEnumerable<MeetingAttendance>? MeetingAttendance { get; set; }

        public DateTime? Date { get; set; }
        [MaxLength(50)]
        public string Location { get; set; } = string.Empty;

        public string Notes { get; set; } = string.Empty;

        public string Task { get; set; } = string.Empty;



    }
}
