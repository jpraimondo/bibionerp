﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class Warehouse:EntityBase
    {
        [MaxLength(50)]
        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public virtual IEnumerable<WarehouseItem>? WarehouseItem { get; set; }
    }
}
