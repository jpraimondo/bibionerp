﻿using BibionERP.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Domain.Entities
{
    public class HolidayEmployee:EntityBase
    {


        public string HolidayName { get; set; } = string.Empty;

       public int EmployeeId { get; set; }
       public virtual Employee? Employee { get; set; }

       public int HolidayDays { get; set; }

        public DateTime? HolidayExpiration { get; set; } = DateTime.MaxValue;

        public HolidayType HolidayType { get; set; }

        public bool RequiredDocumentation { get; set; } = false;

    }
}
