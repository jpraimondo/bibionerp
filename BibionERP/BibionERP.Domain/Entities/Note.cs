﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class Note:EntityBase
    {
        public int UserSenderId { get; set; }

        public int UserReceiverId { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

        [MaxLength(250)]
        public string Title { get; set; } = string.Empty;

        public string Message { get; set; } = string.Empty;

        public bool IsTask { get; set; } = false;

        public bool IsCompleted { get; set; } = false;

        public string Tags { get; set; } = string.Empty;

        public virtual ApplicationUser? UserSender { get; set; }

        public virtual ApplicationUser? UserReceiver { get; set; }

    }
}
