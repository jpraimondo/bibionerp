﻿using EntityFrameworkCore.EncryptColumn.Attribute;
using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class Customer:EntityBase
    {
        [MaxLength(100)]
        [EncryptColumn]
        public string FirstName { get; set; } = string.Empty;
        
        [MaxLength(100)]
        [EncryptColumn]
        public string LastName { get; set; } = string.Empty;
        
        [MaxLength(100)]
        [EncryptColumn]
        public string CustomerType { get; set; } = string.Empty;
        
        [MaxLength(100)]
        [EncryptColumn]
        public string Phone { get; set; } = string.Empty;

        [MaxLength(100)]
        [EncryptColumn] 
        public string Email { get; set; } = string.Empty;
        
        [MaxLength(100)]
        [EncryptColumn]
        public string Address { get; set; } = string.Empty;
        [EncryptColumn]
        public string Notes { get; set; } = string.Empty;
    }
}
