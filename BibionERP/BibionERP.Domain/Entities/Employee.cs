﻿using EntityFrameworkCore.EncryptColumn.Attribute;
using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class Employee:EntityBase
    {
        [MaxLength(100)]
        [EncryptColumn]
        public string FirstName { get; set; } = string.Empty;
        
        [MaxLength(100)]
        [EncryptColumn]
        public string LastName { get; set; } = string.Empty;

        public DateTime? Birthday { get; set; }

        [EncryptColumn]
        public string Document { get; set; } = string.Empty;

        [MaxLength(100)]
        [EncryptColumn]
        public string Designature { get; set; } = string.Empty;
        [MaxLength(100)]
        [EncryptColumn]
        public string Section { get; set; } = string.Empty;

        public DateTime? JoiningDate { get; set; }

        public DateTime? LeavingDate { get; set; }
        
        [MaxLength(100)]
        [EncryptColumn]
        public string Phone { get; set; } = string.Empty;
        
        [MaxLength(100)]
        [EncryptColumn]
        public string Email { get; set; } = string.Empty;
        
        [MaxLength(100)]
        [EncryptColumn]
        public string Address { get; set; } = string.Empty;
        
        [MaxLength(100)]
        [EncryptColumn]
        public string Location { get; set; } = string.Empty;
    }
}
