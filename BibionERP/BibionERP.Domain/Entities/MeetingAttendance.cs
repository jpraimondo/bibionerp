﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class MeetingAttendance:EntityBase
    {

        public int MeetingId { get; set; }

        public int AttendanceId { get; set; }

        [StringLength(100)]
        public string AttendanceName { get; set; } = string.Empty;

        [StringLength(30)]
        public string AttendanceType { get; set; } = string.Empty;

        public virtual Meeting? Meeting { get; set; }
        public virtual Customer? CustomerAttendance { get; set; }
        public virtual Employee? EmployeeAttendance { get; set; }


    }
}
