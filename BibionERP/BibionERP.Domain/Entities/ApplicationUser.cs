﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EntityFrameworkCore.EncryptColumn.Attribute;

namespace BibionERP.Domain.Entities
{
    public class ApplicationUser : EntityBase
    {
        [MaxLength(50)]
        [EncryptColumn]
        public string UserName { get; set; } = string.Empty;
        [MaxLength(50)]
        [EncryptColumn]
        public string FirstName { get; set; } = string.Empty;
        [MaxLength(50)]
        [EncryptColumn]
        public string LastName { get; set; } = string.Empty;
        [MaxLength(50)]
        [EncryptColumn]
        public string Document { get; set; } = string.Empty;

        [DataType(DataType.Password)]
        public string HashPassword { get; set; } = string.Empty;

        public DateTime? Birthday { get; set; }

        public int EmployeeId { get; set; }
        [MaxLength(50)]
        [EncryptColumn]
        public string Email { get; set; } = string.Empty;
        [MaxLength(50)]
        [EncryptColumn]
        public string PhoneNumber { get; set; } = string.Empty;
        [MaxLength(50)]
        [EncryptColumn]
        public string Address { get; set; } = string.Empty;
        [MaxLength(50)]
        [EncryptColumn]
        public string Location { get; set; } = string.Empty;

        public DateTime LastLogin { get; set; } = DateTime.MinValue;    

        [ForeignKey("RolId")]
        public int RolId { get; set; }
        public virtual ApplicationRol? Rol { get; set; }

    }
}
