﻿using EntityFrameworkCore.EncryptColumn.Attribute;
using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class Item:EntityBase
    {
        [MaxLength(100)]
        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;
        
        public decimal CostPrice { get; set; } = decimal.Zero;

        public decimal TradePrice { get; set; } = decimal.Zero;

        public int CategoryItemId { get; set; }
    
        public virtual CategoryItem? CategoryItem { get; set; }

        public int SupplierId { get; set; }

        public virtual Supplier? Supplier { get;set; }
        [MaxLength(50)]
        public string Code { get; set; } = string.Empty;

        public virtual ICollection<WarehouseItem>? WarehouseItem { get; set; }

        public DateTime? ManufactureDate { get; set; }

        public DateTime? ExpirationDate { get; set;}

        public string Note { get; set; } = string.Empty;


    }
}
