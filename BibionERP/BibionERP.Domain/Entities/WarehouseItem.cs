﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class WarehouseItem
    {
        public int WarehouseId { get; set; }
        public virtual Warehouse? Warehouse { get; set; }
        
        public int ItemId { get; set; }
        public virtual Item? Item { get; set; }

        [Required]
        public int Quantity { get; set; }
    
    }
}
