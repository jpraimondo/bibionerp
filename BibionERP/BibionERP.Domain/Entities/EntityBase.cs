﻿using EntityFrameworkCore.EncryptColumn.Attribute;
using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class EntityBase
    {
        [Key]
        public int Id { get; set; }

        public int TennantId { get; set; }

        public DateTime? CreateTime { get; set; }

        [MaxLength(100)]
        public string? CreateBy { get; set; } = string.Empty;

        public DateTime? EditTime { get; set; }

        [MaxLength(100)]
        public string? EditBy { get; set; } = string.Empty;

        public DateTime? DeleteTime { get; set; }

        [MaxLength(100)]
        public string? DeleteBy { get; set; } = string.Empty;

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

    }
}
