﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class Service:EntityBase
    {
        [MaxLength(50)]
        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public decimal CostPrice { get; set; } = decimal.Zero;

        public decimal TotalPrice { get; set;} = decimal.Zero;

        public ICollection<CompanyService>? CompanyServices { get; set; }
    }
}
