﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Domain.Entities
{
    public class Social : EntityBase
    {
        [MaxLength(50)]
        public string Name { get; set; } = string.Empty;

        [MaxLength(255)]
        public string URL { get; set; } = string.Empty;
        
        [MaxLength(50)]
        public string Icons { get; set; } = string.Empty;

        public bool Visible { get; set; } = false;

    }
}
