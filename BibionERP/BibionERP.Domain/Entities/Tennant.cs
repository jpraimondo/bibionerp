﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Domain.Entities
{
    public class Tennant
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; } = string.Empty;

        [MaxLength(100)]
        public string Identificador { get; set; } = string.Empty;

        [MaxLength(500)]
        public string Description { get; set; } = string.Empty;



    }
}
