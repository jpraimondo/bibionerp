﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Domain.Enums
{
    public enum HolidayType
    {
        ANNUALLEAVE = 0,
        STUDYLEAVE = 1,
        HEALTHLEAVE = 2,

    }
}
