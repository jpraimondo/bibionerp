﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Domain.Enums
{
    public enum HolidayStatus
    {

        APPROVE=0,
        APPROVED=1,
        REJECTED=2,
        CANCELED=3,

    }
}
