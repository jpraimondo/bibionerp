﻿namespace BibionERP.Domain.Identity
{
    public class AuthResponse
    {
        public int UserId { get; set; }
        public int EmployeeId { get; set; }

        public string FullName { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public string Role { get; set; } = string.Empty;

    }
}
