﻿namespace BibionERP.Domain.Identity
{
    public class RegistrationRequest
    {

        public string FirstName { get; set; } = string.Empty;

        public string LastName { get; set; } = string.Empty;

        public string Document { get; set; } = string.Empty;

        public int EmployeeId { get; set; }

        public string Email { get; set; } = string.Empty;

        public string PhoneNumber { get; set; } = string.Empty;

        public string Address { get; set; } = string.Empty;

        public string Location { get; set; } = string.Empty;

        public string Password { get; set; } = string.Empty;
    }
}
