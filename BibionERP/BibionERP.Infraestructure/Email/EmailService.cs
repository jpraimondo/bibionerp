﻿using BibionERP.Application.Infraestructure.Interface;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BibionERP.Infraestructure.Email
{
    public class EmailService : IEmailService
    {
        public EmailSettings _emailSettings { get; }
        public ILogger<EmailService> _logger { get; }

        public EmailService(IOptions<EmailSettings> emailSettings, ILogger<EmailService> logger)
        {
            _emailSettings = emailSettings.Value;
            _logger = logger;
        }

        public Task<bool> SendEmail(Domain.Email.Email email)
        {
            throw new NotImplementedException();
        }
    }
}
