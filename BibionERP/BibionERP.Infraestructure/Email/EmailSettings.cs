﻿namespace BibionERP.Infraestructure.Email;

public class EmailSettings
{
    public string ApiKey { get; set; } = "";
    public string FromAddress { get; set; } = "contacto@quammit.com";
    public string FromName { get; set; } = "QuammIT Contact";
}
