﻿using BibionERP.Infraestructure.Email;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using BibionERP.Application.Infraestructure.Interface;

namespace BibionERP.Infraestructure
{
    public static class InfraestructureServiceRegistration
    {
        public static IServiceCollection AddConfigureInfraestructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            
            //services.AddSingleton(new BlobServiceClient(configuration.GetConnectionString("BlobStorage")));
            //services.AddSingleton<IFileService, ImgbbFileService>();

            services.Configure<EmailSettings>(c => configuration.GetSection("EmailSettings").GetChildren().ToList());
            services.AddTransient<IEmailService, EmailService>();

            return services;
        }
    }
}
