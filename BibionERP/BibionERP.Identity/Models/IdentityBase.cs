﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Identity.Models
{
    public class IdentityBase
    {
        [Key]
        public string Id { get; set; } = Guid.NewGuid().ToString();

        public DateTime? CreateTime { get; set; }

        [StringLength(30)]
        public string? CreateBy { get; set; } = string.Empty;

        public DateTime? EditTime { get; set; }

        [StringLength(30)]
        public string? EditBy { get; set; } = string.Empty;

        public DateTime? DeleteTime { get; set; }

        [StringLength(30)]
        public string? DeleteBy { get; set; } = string.Empty;

        public bool IsActve { get; set; }

        public bool IsDelete { get; set; }

    }
}
