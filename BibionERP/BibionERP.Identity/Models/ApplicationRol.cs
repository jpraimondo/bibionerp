﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Identity.Models
{
    public class ApplicationRol:IdentityBase
    {
        [MaxLength(50)]
        public string Name { get; set; } = string.Empty;
        
        [MaxLength(50)]
        public string NormalizedName { get; set; } = string.Empty;  

        public virtual ICollection<ApplicationUser>? Users { get; set; }

    }
}
