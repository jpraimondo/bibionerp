﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BibionERP.Identity.Models
{
    public class ApplicationUser: IdentityBase
    {
        public string UserName { get; set; } = string.Empty;

        public string FirstName { get; set; } = string.Empty;

        public string LastName { get; set; } = string.Empty;

        public string Document { get; set; } = string.Empty;

        [DataType(DataType.Password)]
        public string HashPassword { get; set; } = string.Empty;

        public DateTime? Birthday { get; set; }

        public int EmployeeId { get; set; }

        public string Email { get; set; } = string.Empty;

        public string PhoneNumber { get; set; } = string.Empty;

        public string Address { get; set; } = string.Empty;

        public string Location { get; set; } = string.Empty;

        [ForeignKey("RolId")]
        public string RolId { get; set; } = string.Empty;
        public virtual ApplicationRol? Rol { get; set; }

    }
}
