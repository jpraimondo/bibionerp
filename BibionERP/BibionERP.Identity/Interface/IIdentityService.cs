﻿using BibionERP.Domain.Identity;
using BibionERP.Identity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Identity.Interface
{
    public interface IIdentityService
    {
        Task<AuthResponse> Login(AuthRequest request);

        Task<AuthResponse> Register(RegistrationRequest request);

        Task<AuthResponse> ChangePassword(RegistrationRequest request);

        Task<AuthResponse> SetPassword(RegistrationRequest request);
    }
}
