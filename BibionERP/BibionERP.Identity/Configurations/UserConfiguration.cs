﻿using BibionERP.Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanArchitecture.Identity.Configurations
{
    internal class UserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {

        

        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            var hasher = new PasswordHasher<ApplicationUser>();

            var usuario = new ApplicationUser
            {
                Id = "c2711be2-af4c-43a3-8d9b-0791228cf5bd",
                Email = "admin@localhost.com",
                FirstName = "Admin",
                LastName = "User",
                Document = "99999999",
                EmployeeId = 123,
                Address = "Mi direccion 1454",
                Location = "Montevideo",
                UserName = "administrator",
                RolId = "29ae0766-2113-4869-bf03-44e11ba3845b",
            };

            usuario.HashPassword = hasher.HashPassword(usuario, "MiPassword");


            builder.HasData(usuario);

        }
    }
}
