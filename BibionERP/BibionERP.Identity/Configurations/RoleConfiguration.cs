﻿using BibionERP.Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanArchitecture.Identity.Configurations
{
    internal class RoleConfiguration : IEntityTypeConfiguration<ApplicationRol>
    {
        public void Configure(EntityTypeBuilder<ApplicationRol> builder)
        {
            builder.HasData(
                new ApplicationRol
                {
                    Id = "29ae0766-2113-4869-bf03-44e11ba3845b",
                    Name = "Administrator",
                    NormalizedName="ADMINISTRATOR"
                },
                new ApplicationRol
                {
                    Id = "0430b079-d867-40e9-8b3d-02735f35f5d9",
                    Name = "Operator",
                    NormalizedName = "OPERATOR"
                }
                ) ;
        }
    }
}
