﻿using BibionERP.Identity.Models;
using BibionERP.Identity.Persistence;
using BibionERP.Identity.Repositories.Interfaces;

namespace BibionERP.Identity.Repositories
{
    public class IdentityUnitOfWork : IIdentityUnitOfWork
    {

        private readonly BibionIdentityDbContext context;


        private IIdentityRepository<ApplicationUser>? usersRepository;

        private IIdentityRepository<ApplicationRol>? rolesRepository;
 

        public IIdentityRepository<ApplicationUser> UsersRepository
        {
            get
            {
                if (usersRepository == null)
                {
                    usersRepository = new IdentityRepository<ApplicationUser>(context);
                }
                return usersRepository;
            }
        }

        public IIdentityRepository<ApplicationRol> RolesRepository
        {
            get
            {
                if (rolesRepository == null)
                {
                    rolesRepository = new IdentityRepository<ApplicationRol>(context);
                }
                return rolesRepository;
            }
        }

        public IdentityUnitOfWork(BibionIdentityDbContext ctx)
        {
            context = ctx;
        }

        public IdentityUnitOfWork()
        {
            
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public async Task Save()
        {
            await context.SaveChangesAsync();
        }
    }
}
