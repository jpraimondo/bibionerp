﻿using BibionERP.Identity.Models;
using BibionERP.Identity.Persistence;
using BibionERP.Identity.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace BibionERP.Identity.Repositories
{
    public class IdentityRepository<T> : IIdentityRepository<T> where T : IdentityBase
    {
        protected readonly BibionIdentityDbContext context;
        protected DbSet<T> entities;

        public IdentityRepository(BibionIdentityDbContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }

        public Task Activate(T entity)
        {
            entity.IsActve = true;
            entity.EditTime = DateTime.Now;

            entities.Update(entity);

            return Task.CompletedTask;
        }

        public Task Delete(T entity)
        {
            entity.IsDelete = true;
            entity.DeleteTime = DateTime.Now;

            entities.Update(entity);

            return Task.CompletedTask;
        }

        public Task Inactivate(T entity)
        {
            entity.IsActve = false;
            entity.EditTime = DateTime.Now;

            entities.Update(entity);

            return Task.CompletedTask;
        }

        public async Task Insert(T entity)
        {
            entity.CreateTime = DateTime.Now;
            entity.IsDelete = false;

            await entities.AddAsync(entity);

            
        }

        public Task Update(T entity)
        {
            entity.EditTime = DateTime.Now;

            entities.Update(entity);

            return Task.CompletedTask;
        }

        #region FuncionesGet

        public async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate)
        {
            return await entities.Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate,
                                       Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                       string includeString = null,
                                       bool disableTracking = true)
        {
            IQueryable<T> query = entities;

            if (disableTracking) query = query.AsNoTracking();

            if (!string.IsNullOrWhiteSpace(includeString)) query = query.Include(includeString);

            if (predicate != null) query = query.Where(predicate);

            if (orderBy != null)
                return await orderBy(query).ToListAsync();


            return await query.ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await entities.Where(e => e.IsDelete.Equals(false)).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllAndDelete()
        {
            return await entities.ToListAsync();
        }

        public async Task<T> GetByGuid(string guid)
        {
            return await entities.Where(x => x.Id.Equals(guid)).FirstAsync();
        }


        #endregion

    }
}
