﻿using BibionERP.Domain.Entities;
using BibionERP.Identity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Identity.Repositories.Interfaces
{
    public interface IIdentityUnitOfWork : IDisposable
    {
       
        public IIdentityRepository<ApplicationRol> RolesRepository { get; }
        public IIdentityRepository<ApplicationUser> UsersRepository { get; }


        Task Save();
    }
}
