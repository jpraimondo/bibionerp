﻿using BibionERP.Domain.Identity;
using BibionERP.Identity.Interface;
using BibionERP.Identity.Models;
using BibionERP.Identity.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace BibionERP.Identity.Services
{
    public class IdentityService : IIdentityService
    {
        
        private readonly IIdentityUnitOfWork _unitOfWork;


        public IdentityService(IIdentityUnitOfWork unitOfWork)
        {
            _unitOfWork= unitOfWork;
        }

        public Task<AuthResponse> ChangePassword(RegistrationRequest request)
        {
            throw new NotImplementedException();
        }

        public async Task<AuthResponse> Login(AuthRequest request)
        {
            var user = (await _unitOfWork.UsersRepository.Find(x => x.Email.Equals(request.Email))).FirstOrDefault();

            if (user is null)
            {
                throw new Exception($"Error en email o password");
            }

            var hasher = new PasswordHasher<ApplicationUser>();

            var resultado = hasher.VerifyHashedPassword(user, user.HashPassword, request.Password);

            

            if (resultado.Equals(0))
            {
                throw new Exception($"Error en email o password");
            }

            var auth = new AuthResponse
            {
                EmployeeId = user.EmployeeId,
                Email = user.Email,
                FullName = $"{user.FirstName} {user.LastName}",

            };

           return auth;

        }


        public async Task<AuthResponse> Register(RegistrationRequest request)
        {
            string error = "";
            var hasher = new PasswordHasher<ApplicationUser>();

            try
            {


            var existingMail = (await _unitOfWork.UsersRepository.Find(x => x.Email.Equals(request.Email))).FirstOrDefault();

            if (existingMail != null)
            {
                throw new Exception($"El correo ya esta utilizado");
            }

            var user = new ApplicationUser
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                UserName = request.Email,
                Document = request.Document,
                PhoneNumber = request.PhoneNumber,
                EmployeeId = request.EmployeeId,
                Location = request.Location,
                
            };

            user.HashPassword = hasher.HashPassword(user,request.Password);

            user.RolId = "0430b079-d867-40e9-8b3d-02735f35f5d9";

            await _unitOfWork.UsersRepository.Insert(user);


                return new AuthResponse
                {
                    Email = user.Email,
                    FullName = $"{user.FirstName} {user.LastName}",
                    EmployeeId = user.EmployeeId,
                };



            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

            

            
        }

        public Task<AuthResponse> SetPassword(RegistrationRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
