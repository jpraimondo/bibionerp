﻿using BibionERP.Identity.Interface;
using BibionERP.Identity.Persistence;
using BibionERP.Identity.Repositories;
using BibionERP.Identity.Repositories.Interfaces;
using BibionERP.Identity.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CleanArchitecture.Identity
{
    public static class IdentityServiceRegistration
    {


        public static IServiceCollection AddConfigureIdentityServices(this IServiceCollection services,IConfiguration configuration)
        {
            
            var connectionString = configuration.GetConnectionString("WebApiIdentity");

            services.AddDbContext<BibionIdentityDbContext>(options =>
            options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString)
            ));

            services.AddScoped<IIdentityUnitOfWork, IdentityUnitOfWork>();

           
            
            return services;
        }

    }
}
