﻿using BibionERP.Identity.Models;
using CleanArchitecture.Identity.Configurations;
using Microsoft.EntityFrameworkCore;

namespace BibionERP.Identity.Persistence
{
    public class BibionIdentityDbContext:DbContext
    {
        public BibionIdentityDbContext(DbContextOptions<BibionIdentityDbContext> options)
         : base(options)
        {

        }

        public DbSet<ApplicationUser>? ApplicationUsers { get; set; }
        public DbSet<ApplicationRol>? ApplicationRols { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new RoleConfiguration());
            builder.ApplyConfiguration(new UserConfiguration());
           

        }
    }
}
