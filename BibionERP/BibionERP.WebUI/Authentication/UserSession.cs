﻿namespace BibionERP.WebUI.Authentication
{
    public class UserSession
    {

        public string UserId { get; set; } = string.Empty;
        public string EmployeeId { get; set; } = string.Empty;

        public string FullName { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public string Role { get; set; } = string.Empty;

        public string Expiration { get; set; } = string.Empty;
    }
}
