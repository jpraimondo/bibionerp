﻿using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;
using System.Security.Claims;

namespace BibionERP.WebUI.Authentication
{
    public class CustomAuthenticationStateProvider : AuthenticationStateProvider
    {
        private readonly ProtectedSessionStorage _sessionStorage;

        private readonly ProtectedLocalStorage _localStorage;

        private ClaimsPrincipal _anonymuous = new ClaimsPrincipal(new  ClaimsIdentity());


        public CustomAuthenticationStateProvider(ProtectedSessionStorage sessionStorage, ProtectedLocalStorage localStorage)
        {
            _sessionStorage = sessionStorage;
            _localStorage = localStorage;   
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            try
            {

                var userSessionStorageResult = await _sessionStorage.GetAsync<UserSession>("UserSession");

                var userSession = userSessionStorageResult.Success ? userSessionStorageResult.Value : null;

               

                if (userSession is null || DateTime.Parse(userSession.Expiration) < DateTime.Now)
                {
                    var userLocalStorageResult = await _localStorage.GetAsync<UserSession>("UserSession");

                    userSession = userLocalStorageResult.Success ? userLocalStorageResult.Value : null;

                    if (userSession is null || DateTime.Parse(userSession.Expiration) < DateTime.Now)
                        return await Task.FromResult(new AuthenticationState(_anonymuous));


                    await _sessionStorage.SetAsync("UserSession", userSession);
                }

                var claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>
            {
                new Claim(ClaimTypes.Name,userSession.FullName),
                new Claim(ClaimTypes.Role,userSession.Role),
                new Claim(ClaimTypes.Email,userSession.Email),
                new Claim(ClaimTypes.Expiration,userSession.Expiration),

            }, "CustomAuth"));

                return await Task.FromResult(new AuthenticationState(claimsPrincipal));

            }
            catch (Exception ex)
            {

                return await Task.FromResult(new AuthenticationState(_anonymuous));
            } 

        }

        public async Task UpdateAuthenticationState(UserSession userSession, bool? recorder = false)
        {
            ClaimsPrincipal claimsPrincipal;

            if (userSession != null)
            {
                userSession.Expiration = DateTime.Now.AddMinutes(60).ToString();
                await _sessionStorage.SetAsync("UserSession", userSession);


                if(recorder.Equals(true))
                {
                    userSession.Expiration = DateTime.Now.AddDays(1).ToString();
                    await _localStorage.SetAsync("UserSession", userSession);
                }


                claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>
                {
                    new Claim(ClaimTypes.Name,userSession.FullName),
                    new Claim(ClaimTypes.Role,userSession.Role),
                    new Claim(ClaimTypes.Email,userSession.Email),
                    new Claim(ClaimTypes.Expiration,userSession.Expiration),
                }));
            
            }
            else
            {
                await _sessionStorage.DeleteAsync("UserSession");
                await _localStorage.DeleteAsync("UserSession");
                claimsPrincipal = _anonymuous;
            }

            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(claimsPrincipal)));
        }
    
    }
}
