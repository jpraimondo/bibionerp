﻿

using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.WebUI.Helpers.Interface;

namespace BibionERP.WebUI.Helpers
{
    public class SettingsHelper:ISettingsHelper
    {
        private readonly ISettingsService _settingsService;


        public SettingsHelper(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        public Task<ViewDetailSettingsDto> GetSettings()
        {
            return _settingsService.GetSettings();
        }

        public Task<ViewDetailSettingsDto> UpdateSettings(CreateSettingsDto settingsDto, string? userName)
        {
            return _settingsService.UpdateSettings(settingsDto, userName);
        }
    }
}
