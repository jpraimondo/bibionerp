﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.ViewDetail;

namespace BibionERP.WebUI.Helpers.Interface
{
    public interface ISettingsHelper
    {

        Task<ViewDetailSettingsDto> GetSettings();
        Task<ViewDetailSettingsDto> UpdateSettings(CreateSettingsDto settingsDto, string? userName);

    }
}
