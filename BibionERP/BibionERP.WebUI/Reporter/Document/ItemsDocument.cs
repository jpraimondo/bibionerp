﻿using QuestPDF.Infrastructure;
using QuestPDF.Fluent;
using QuestPDF.Helpers;
using BibionERP.Application.DTOs.View;

namespace BibionERP.WebUI.Reporter.Document
{
    public class ItemsDocument : IDocument
    {
        
        public static Image LogoImage { get; } = Image.FromFile("Reporter/Images/logo_solo.png");

        public IEnumerable<ViewItemDto> Model { get; }
        public string TitleDocument { get; set; } = string.Empty;
        public string DescriptionDocument { get; set; }

        public ItemsDocument(IEnumerable<ViewItemDto> model, string titleDocument, string? descriptionDocument = null)
        {
            Model = model;
            TitleDocument = titleDocument;
            DescriptionDocument = descriptionDocument;
        }

        public DocumentMetadata GetMetadata() => DocumentMetadata.Default;

        public void Compose(IDocumentContainer container)
        {
            container
                .Page(page =>
                {
                    page.Size(PageSizes.A4);
                    page.Margin(40);

                    page.Header().Element(ComposeHeader);
                    page.Content().Element(ComposeContent);

                    page.Footer().AlignCenter().Text(text =>
                    {
                        text.CurrentPageNumber();
                        text.Span(" / ");
                        text.TotalPages();
                    });
                });
        }

        void ComposeHeader(IContainer container)
        {
            container.Row(row =>
            {
                row.ConstantItem(50).Image(LogoImage);

                row.RelativeItem().AlignCenter().Column(column =>
                {
                    column
                        .Item().Text($"{TitleDocument}")
                        .FontSize(20).SemiBold().FontColor(Colors.Blue.Medium);
                            
                    if(!string.IsNullOrEmpty(DescriptionDocument))
                        column
                        .Item().Text($"{DescriptionDocument}")
                        .FontSize(12).SemiBold().FontColor(Colors.Blue.Medium);

                });

                row.RelativeItem().AlignRight().Column(column =>
                 column.Item().Text(text =>
                 {
                     text.Span("Fecha: ").SemiBold();
                     text.Span($"{DateTime.Now.ToShortDateString().ToString()}");
                 })
                 );

            });
        }

        void ComposeContent(IContainer container)
        {
            container.PaddingVertical(40).Column(column =>
            {
                column.Spacing(20);

                column.Item().Element(ComposeTable);

                if (!string.IsNullOrWhiteSpace("Aca pueden ir comentarios adicionales."))
                    column.Item().PaddingTop(25).Element(ComposeComments);
            });
        }

        void ComposeTable(IContainer container)
        {
            var headerStyle = TextStyle.Default.SemiBold();

            var cellStyle = TextStyle.Default.FontSize(8).Medium().FontColor(Colors.Grey.Darken3);


            container.Table(table =>
            {
                table.ColumnsDefinition(columns =>
                {
                    columns.ConstantColumn(25);
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.RelativeColumn();
                    columns.ConstantColumn(50);
                    columns.ConstantColumn(75);
                });

                table.Header(header =>
                {
                   
                    header.Cell().Text("Id").Style(headerStyle);
                    header.Cell().Text("Nombre").Style(headerStyle);
                    header.Cell().Text("Codigo").Style(headerStyle);
                    header.Cell().Text("Proveedor").Style(headerStyle);
                    header.Cell().Text("Cantidad").Style(headerStyle);
                    header.Cell().Text("Precio").Style(headerStyle);

                    header.Cell().ColumnSpan(6).PaddingTop(5).BorderBottom(1).BorderColor(Colors.Black);
                });

                foreach (var item in Model)
                {
                   
                    table.Cell().Element(CellStyle).Text($"{item.Id}");
                    table.Cell().Element(CellStyle).Text($"{item.Name}");
                    table.Cell().Element(CellStyle).Text($"{item.Code}");
                    table.Cell().Element(CellStyle).Text($"{item.SupplierName}");
                    table.Cell().Element(CellStyle).Text($"{item.Quantity}");
                    table.Cell().Element(CellStyle).Text($"{string.Format("{0:0.00}", item.TradePrice)}");


                    static IContainer CellStyle(IContainer container) => container.BorderBottom(1).BorderColor(Colors.Grey.Lighten2).PaddingVertical(2);
                }
            });
        }

        void ComposeComments(IContainer container)
        {
            container.ShowEntire().Background(Colors.Grey.Lighten3).Padding(10).Column(column =>
            {
                column.Spacing(5);
                column.Item().Text("Comments").FontSize(14).SemiBold();
                column.Item().Text("Aca pueden ir comentarios extra");
            });
        }


    }

}
