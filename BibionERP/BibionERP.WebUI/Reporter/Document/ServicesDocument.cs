﻿using QuestPDF.Infrastructure;
using QuestPDF.Fluent;
using QuestPDF.Helpers;
using BibionERP.Application.DTOs.View;

namespace BibionERP.WebUI.Reporter.Document
{
    public class ServicesDocument: IDocument
    {
        
        public static Image LogoImage { get; } = Image.FromFile("Reporter/Images/logo_solo.png");

        public IEnumerable<ViewServiceDto> Model { get; }

        public ServicesDocument(IEnumerable<ViewServiceDto> model)
        {
            Model = model;
        }

        public DocumentMetadata GetMetadata() => DocumentMetadata.Default;

        public void Compose(IDocumentContainer container)
        {
            container
                .Page(page =>
                {
                    page.Size(PageSizes.A4);
                    page.Margin(40);

                    page.Header().Element(ComposeHeader);
                    page.Content().Element(ComposeContent);

                    page.Footer().AlignCenter().Text(text =>
                    {
                        text.CurrentPageNumber();
                        text.Span(" / ");
                        text.TotalPages();
                    });
                });
        }

        void ComposeHeader(IContainer container)
        {
            container.Row(row =>
            {
                row.ConstantItem(50).Image(LogoImage);

                row.RelativeItem().AlignCenter().Column(column =>
                {
                    column
                        .Item().Text($"Listado de Servicios")
                        .FontSize(20).SemiBold().FontColor(Colors.Blue.Medium);


                });

                row.RelativeItem().AlignRight().Column(column =>
                 column.Item().Text(text =>
                 {
                     text.Span("Fecha: ").SemiBold();
                     text.Span($"{DateTime.Now.ToShortDateString().ToString()}");
                 })
                 );

            });
        }

        void ComposeContent(IContainer container)
        {
            container.PaddingVertical(40).Column(column =>
            {
                column.Spacing(20);

                column.Item().Element(ComposeTable);

                if (!string.IsNullOrWhiteSpace("Aca pueden ir comentarios adicionales."))
                    column.Item().PaddingTop(25).Element(ComposeComments);
            });
        }

        void ComposeTable(IContainer container)
        {
            var headerStyle = TextStyle.Default.SemiBold();

            var cellStyle = TextStyle.Default.FontSize(8).Medium().FontColor(Colors.Grey.Darken3);


            container.Table(table =>
            {
                table.ColumnsDefinition(columns =>
                {
                    columns.ConstantColumn(25);
                    columns.ConstantColumn(150);
                    columns.RelativeColumn();
                    columns.ConstantColumn(75);
                    
                });

                table.Header(header =>
                {
                   
                    header.Cell().AlignCenter().Text("Id").Style(headerStyle);
                    header.Cell().AlignCenter().Text("Nombre").Style(headerStyle);
                    header.Cell().AlignCenter().Text("Descripción").Style(headerStyle);
                    header.Cell().AlignCenter().Text("Precio").Style(headerStyle);
                    

                    header.Cell().ColumnSpan(4).PaddingTop(5).BorderBottom(1).BorderColor(Colors.Black);
                });

                foreach (var item in Model)
                {
                   
                    table.Cell().Element(CellStyle).Text($"{item.Id}");
                    table.Cell().Element(CellStyle).Text(item.Name);
                    table.Cell().Element(CellStyle).Text($"{item.Description}");
                    table.Cell().Element(CellStyle).Text($"{string.Format("{0:0.00}", item.TotalPrice)}");
                    
                    
                    static IContainer CellStyle(IContainer container) => container.BorderBottom(1).BorderColor(Colors.Grey.Lighten2).PaddingVertical(2);
                }
            });
        }

        void ComposeComments(IContainer container)
        {
            container.ShowEntire().Background(Colors.Grey.Lighten3).Padding(10).Column(column =>
            {
                column.Spacing(5);
                column.Item().Text("Comments").FontSize(14).SemiBold();
                column.Item().Text("Aca pueden ir comentarios extra");
            });
        }


    }

}
