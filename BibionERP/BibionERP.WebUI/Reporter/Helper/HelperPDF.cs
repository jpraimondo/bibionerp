﻿using QuestPDF.Fluent;
using QuestPDF.Infrastructure;
using QuestPDF;
using BibionERP.Application.DTOs.Interface;
using BibionERP.WebUI.Reporter.Document;
using BibionERP.Application.DTOs.View;
using System.Diagnostics;

namespace BibionERP.WebUI.Reporter.Helper
{
    public static class HelperPDF
    {
        static readonly string path = Path.GetTempPath();

           
        public static void GenerateEmployeePDF(List<ViewEmployeeDto> modelEmployee)
        {
            Settings.License = LicenseType.Community;

            var document = new EmployeeDocument(modelEmployee);
            
            var filePath = $"{path}{Guid.NewGuid()}.pdf";

            document.GeneratePdf(filePath);

            Process.Start("explorer.exe", filePath);

           
        }

        public static void GenerateUsersPDF(List<ViewUserDto> modelUsers)
        {
            Settings.License = LicenseType.Community;

            var document = new UsersDocument(modelUsers);

            var filePath = $"{path}{Guid.NewGuid()}.pdf";

            document.GeneratePdf(filePath);

            Process.Start("explorer.exe", filePath);

        }

        public static void GenerateServicesPDF(List<ViewServiceDto> modelServices)
        {
            Settings.License = LicenseType.Community;

            var document = new ServicesDocument(modelServices);

            var filePath = $"{path}{Guid.NewGuid()}.pdf";

            document.GeneratePdf(filePath);

            Process.Start("explorer.exe", filePath);

        }

        public static void GenerateCustomerPDF(List<ViewCustomerDto> modelServices)
        {
            Settings.License = LicenseType.Community;

            var document = new CustomersDocument(modelServices);

            var filePath = $"{path}{Guid.NewGuid()}.pdf";

            document.GeneratePdf(filePath);

            Process.Start("explorer.exe", filePath);

        }

        public static void GenerateItemsPDF(List<ViewItemDto> modelItems, string titleDocument = "Todos los articulos")
        {
            Settings.License = LicenseType.Community;
            
            var document = new ItemsDocument(modelItems, titleDocument);
            
            var filePath = $"{path}{Guid.NewGuid()}.pdf";
            
            document.GeneratePdf(filePath);
            
            Process.Start("explorer.exe", filePath);
            
        }

    }
}
