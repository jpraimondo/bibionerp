﻿using BibionERP.Domain.Entities;
using BibionERP.Persistence.Configuration;
using CleanArchitecture.Identity.Configurations;
using Microsoft.EntityFrameworkCore;
using EntityFrameworkCore.EncryptColumn.Extension;
using EntityFrameworkCore.EncryptColumn.Interfaces;
using EntityFrameworkCore.EncryptColumn.Util;

namespace BibionERP.Persistence.Persistence
{
    public class BibionDataDbContext : DbContext
    {

        private readonly IEncryptionProvider _provider;

        public BibionDataDbContext(DbContextOptions<BibionDataDbContext> options)
            :base(options)
        {
            this._provider = new GenerateEncryptionProvider("v5SPN6Y29rpyx5nA");
        }

        public DbSet<CategoryItem>? CategoryItems { get; set; }
        public DbSet<CategoryDeliveryNote>? CategoryDeliveryNote { get; set; }
        public DbSet<Company>? Companies { get; set;}
        public DbSet<CompanyService>? CompaniesServices { get; set; }
        public DbSet<Customer>? Customers { get; set; }
        public DbSet<DeliveryNote>? DeliveryNotes { get; set;}
        public DbSet<DeliveryNoteDetail>? DeliveryNoteDetails { get; set; }
        public DbSet<Employee>? Employees { get; set; }
        public DbSet<Item>? Items { get; set; }
        public DbSet<Meeting>? Meetings { get; set; }
        public DbSet<Service>? Services { get; set; }
        public DbSet<Supplier>? Suppliers { get; set; }
        public DbSet<Warehouse>? Warehouses { get; set; }
        public DbSet<ApplicationUser>? ApplicationUsers { get; set; }
        public DbSet<ApplicationRol>? ApplicationRols { get; set; }
        public DbSet<Settings>? Settings { get; set; }
        public DbSet<Social>? SocialNetworks { get; set; }
        public DbSet<MeetingAttendance>? MettingAttendance { get; set; }
        public DbSet<WarehouseItem>? WarehouseItems { get; set; }
        public DbSet<Holiday>? Holidays { get; set; }
        public DbSet<HolidayEmployee>? HolidayEmployee { get; set; }
        public DbSet<Note>? Notes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseEncryption(this._provider);

            

            modelBuilder.Entity<CompanyService>()
                .HasKey(cs => new { cs.CompanyId, cs.ServiceId });

            modelBuilder.Entity<CompanyService>()
                .HasOne(cs => cs.Company)
                .WithMany(c => c.CompanyServices)
                .HasForeignKey(cs => cs.CompanyId);

            modelBuilder.Entity<CompanyService>()
                .HasOne(cs => cs.Service)
                .WithMany(s => s.CompanyServices)
                .HasForeignKey(cs => cs.ServiceId);

            modelBuilder.Entity<WarehouseItem>()
                .HasKey(wi => new { wi.WarehouseId, wi.ItemId });

            modelBuilder.Entity<WarehouseItem>()
                .HasOne(wi => wi.Warehouse)
                .WithMany(w => w.WarehouseItem)
                .HasForeignKey(wi => wi.WarehouseId);

            modelBuilder.Entity<WarehouseItem>()
                .HasOne(wi => wi.Item)
                .WithMany(i => i.WarehouseItem)
                .HasForeignKey(ai => ai.ItemId);

            modelBuilder.Entity<MeetingAttendance>()
                .HasOne(ma => ma.Meeting)
                .WithMany(m => m.MeetingAttendance)
                .HasForeignKey(ma => ma.MeetingId);

            modelBuilder.Entity<MeetingAttendance>()
                .HasOne(ma => ma.CustomerAttendance)
                .WithMany()
                .HasForeignKey(ma => ma.AttendanceId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(false); // Si la eliminación en cascada no es necesaria

            modelBuilder.Entity<MeetingAttendance>()
                .HasOne(ma => ma.EmployeeAttendance)
                .WithMany()
                .HasForeignKey(ma => ma.AttendanceId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(false); // Si la eliminación en cascada no es necesaria

            modelBuilder.Entity<Note>()
            .HasOne(n => n.UserSender)
            .WithMany()
            .HasForeignKey(n => n.UserSenderId)
            .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Note>()
                .HasOne(n => n.UserReceiver)
                .WithMany()
                .HasForeignKey(n => n.UserReceiverId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Item>().Property(m => m.CostPrice).HasPrecision(10, 2);
            modelBuilder.Entity<Item>().Property(m => m.TradePrice).HasPrecision(10, 2);

            modelBuilder.Entity<Service>().Property(m => m.CostPrice).HasPrecision(10, 2);
            modelBuilder.Entity<Service>().Property(m => m.TotalPrice).HasPrecision(10, 2);

            modelBuilder.Entity<CompanyService>().Property(m => m.DiscountPercentage).HasPrecision(5, 2);
            modelBuilder.Entity<CompanyService>().Property(m => m.TotalCost).HasPrecision(10, 2);
            modelBuilder.Entity<CompanyService>().Property(m => m.FinalPrice).HasPrecision(10, 2);

            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new WarehouseConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryDeliveryNoteConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryItemConfiguration());
            modelBuilder.ApplyConfiguration(new SupplierConfiguration());
            modelBuilder.ApplyConfiguration(new ItemConfiguration());
            modelBuilder.ApplyConfiguration(new ServiceConfiguration());
            modelBuilder.ApplyConfiguration(new EmployeeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new CompanyConfiguration());
            modelBuilder.ApplyConfiguration(new WarehouseItemConfiguration());
            modelBuilder.ApplyConfiguration(new SettingsConfiguration());
        }


    }
}
