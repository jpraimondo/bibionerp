﻿using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using BibionERP.Persistence.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace BibionERP.Persistence.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : EntityBase
    {
        protected readonly BibionDataDbContext context;
        protected DbSet<T> entities;

        public GenericRepository(BibionDataDbContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }

        public Task Activate(T entity)
        {
            entity.IsActive = true;
            entity.EditTime = DateTime.Now;

            entities.Update(entity);

            return Task.CompletedTask;
        }

        public Task Delete(T entity)
        {
            entity.IsDelete = true;
            entity.DeleteTime = DateTime.Now;

            entities.Update(entity);

            return Task.CompletedTask;
        }

        public Task Inactivate(T entity)
        {
            entity.IsActive = false;
            entity.EditTime = DateTime.Now;

            entities.Update(entity);

            return Task.CompletedTask;
        }

        public async Task Insert(T entity)
        {
            entity.CreateTime = DateTime.Now;
            entity.IsDelete = false;

            await entities.AddAsync(entity);


        }

        public Task Update(T entity)
        {
            entity.EditTime = DateTime.Now;

            entities.Update(entity);

            return Task.CompletedTask;
        }

        #region FuncionesGet

        public async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate)
        {
            return await entities.AsNoTracking().Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate,
                                       Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                       string includeString = null,
                                       bool disableTracking = true)
        {
            IQueryable<T> query = entities;

            if (disableTracking) query = query.AsNoTracking();

            //if (!string.IsNullOrWhiteSpace(includeString)) query = query.Include(includeString);

            if (!string.IsNullOrWhiteSpace(includeString))
            {
                var includes = includeString.Split(',');

                foreach (var include in includes)
                {
                    query = query.Include(include);
                }

            }


            if (predicate != null) query = query.Where(predicate);

            if (orderBy != null)
                return await orderBy(query).ToListAsync();


            return await query.ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAll()
        {

            IEnumerable<T> list = await entities.AsNoTracking().Where(e => e.IsDelete.Equals(false)).ToListAsync();
            return list;
        }

        public async Task<IEnumerable<T>> GetAll(string includeStrings)
        {
            IQueryable<T> query = entities;

            query = entities.AsNoTracking().Where(e => e.IsDelete.Equals(false));

            if (includeStrings != null)
            {
                var includes = includeStrings.Split(',');

                foreach (var include in includes)
                {
                    query = query.Include(include);
                }

            }

            return await query.ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllAndDelete()
        {
            return await entities.ToListAsync();
        }

        public async Task<T> GetById(int id)
        {
            return await entities.FindAsync(id);
        }

        public async Task<T> GetById(int id, string includeString)
        {

            IQueryable<T> query = entities;

            if (includeString != null)
            {
                var includes = includeString.Split(',');

                foreach (var include in includes)
                {
                    query = query.Include(include);
                }

            }

            return await query.FirstAsync(x => x.Id.Equals(id));
        }

        #endregion

    }
}
