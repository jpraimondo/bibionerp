﻿using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using BibionERP.Persistence.Persistence;
using Microsoft.EntityFrameworkCore.Storage;
using System.Net.Http;

namespace BibionERP.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly BibionDataDbContext context;

        private IGenericRepository<CategoryDeliveryNote>? categoryDeliveryNoteRepository;

        private IGenericRepository<CategoryItem>? categoryItemRepository;

        private IGenericRepository<Company>? companyRepository;

        private IGenericRepository<Customer>? customerRepository;

        private IGenericRepository<DeliveryNoteDetail>? deliveryNoteDetailRepository;

        private IGenericRepository<DeliveryNote>? deliveryNoteRepository;

        private IGenericRepository<Employee>? employeeRepository;

        private IGenericRepository<Item>? itemRepository;

        private IGenericRepository<Meeting>? meetingRepository;

        private IGenericRepository<Service>? serviceRepository;

        private IGenericRepository<Supplier>? supplierRepository;

        private IGenericRepository<Warehouse>? warehouseRepository;

        private IGenericRepository<ApplicationUser>? usersRepository;

        private IGenericRepository<ApplicationRol>? rolesRepository;

        private IGenericRepository<Settings>? settingsRepository;

        private IGenericRepository<MeetingAttendance>? meetingAttendance;

        private IGenericRepository<Holiday>? holidayRepository;

        private IGenericRepository<HolidayEmployee>? holidayEmployeeRepository;

        private IGenericRepository<Note>? noteRepository;

        private IGenericRepository<Social>? socialRepository;

        public IGenericRepository<Settings> SettingsRepository
        {
            get
            {
                if (settingsRepository == null)
                {
                    settingsRepository = new GenericRepository<Settings>(context);
                }
                return settingsRepository;
            }
        }

        public IGenericRepository<CategoryDeliveryNote> CategoryDeliveryNoteRepository
        {
            get
            {
                if (categoryDeliveryNoteRepository == null)
                {
                    categoryDeliveryNoteRepository = new GenericRepository<CategoryDeliveryNote>(context);
                }
                return categoryDeliveryNoteRepository;
            }
        }

        public IGenericRepository<CategoryItem> CategoryItemRepository {
            get
            {
                if (categoryItemRepository == null)
                {
                    categoryItemRepository = new GenericRepository<CategoryItem>(context);
                }
                return categoryItemRepository;
            }
        }

        public IGenericRepository<Company> CompanyRepository
        {
            get
            {
                if (companyRepository == null)
                {
                    companyRepository = new GenericRepository<Company>(context);
                }
                return companyRepository;
            }
        }

        public IGenericRepository<Customer> CustomerRepository
        {
            get
            {
                if (customerRepository == null)
                {
                    customerRepository = new GenericRepository<Customer>(context);
                }
                return customerRepository;
            }
        }

        public IGenericRepository<DeliveryNoteDetail> DeliveryNoteDetailRepository
        {
            get
            {
                if (deliveryNoteDetailRepository == null)
                {
                    deliveryNoteDetailRepository = new GenericRepository<DeliveryNoteDetail>(context);
                }
                return deliveryNoteDetailRepository;
            }
        }

        public IGenericRepository<DeliveryNote> DeliveryNoteRepository
        {
            get
            {
                if (deliveryNoteRepository == null)
                {
                    deliveryNoteRepository = new GenericRepository<DeliveryNote>(context);
                }
                return deliveryNoteRepository;
            }
        }

        public IGenericRepository<Employee> EmployeeRepository
        {
            get
            {
                if (employeeRepository == null)
                {
                    employeeRepository = new GenericRepository<Employee>(context);
                }
                return employeeRepository;
            }
        }

        public IGenericRepository<Item> ItemRepository
        {
            get
            {
                if (itemRepository == null)
                {
                    itemRepository = new GenericRepository<Item>(context);
                }
                return itemRepository;
            }
        }

        public IGenericRepository<Meeting> MeetingRepository
        {
            get
            {
                if (meetingRepository == null)
                {
                    meetingRepository = new GenericRepository<Meeting>(context);
                }
                return meetingRepository;
            }
        }

        public IGenericRepository<Service> ServiceRepository
        {
            get
            {
                if (serviceRepository == null)
                {
                    serviceRepository = new GenericRepository<Service>(context);
                }
                return serviceRepository;
            }
        }

        public IGenericRepository<Supplier> SupplierRepository
        {
            get
            {
                if (supplierRepository == null)
                {
                    supplierRepository = new GenericRepository<Supplier>(context);
                }
                return supplierRepository;
            }
        }

        public IGenericRepository<Warehouse> WarehouseRepository
        {
            get
            {
                if (warehouseRepository == null)
                {
                    warehouseRepository = new GenericRepository<Warehouse>(context);
                }
                return warehouseRepository;
            }
        }

        public IGenericRepository<ApplicationUser> UsersRepository
        {
            get
            {
                if (usersRepository == null)
                {
                    usersRepository = new GenericRepository<ApplicationUser>(context);
                }
                return usersRepository;
            }
        }

        public IGenericRepository<ApplicationRol> RolesRepository
        {
            get
            {
                if (rolesRepository == null)
                {
                    rolesRepository = new GenericRepository<ApplicationRol>(context);
                }
                return rolesRepository;
            }
        }

        public IGenericRepository<MeetingAttendance> MeetingAttendanceRepository
        {
            get
            {
                if (meetingAttendance == null)
                {
                    meetingAttendance = new GenericRepository<MeetingAttendance>(context);
                }
                return meetingAttendance;
            }
        }

        public IGenericRepository<Holiday> HolidayRepository
        {
            get
            {
                if (holidayRepository == null)
                {
                    holidayRepository = new GenericRepository<Holiday>(context);
                }
                return holidayRepository;
            }
        }

        public IGenericRepository<HolidayEmployee> HolidayEmployeeRepository
        {
            get
            {
                if (holidayEmployeeRepository == null)
                {
                    holidayEmployeeRepository = new GenericRepository<HolidayEmployee>(context);
                }
                return holidayEmployeeRepository;
            }
        }

        public IGenericRepository<Note> NoteRepository
        {
            get
            {
                if (noteRepository == null)
                {
                    noteRepository = new GenericRepository<Note>(context);
                }
                return noteRepository;
            }
        }

        public IGenericRepository<Social> SocialRepository
        {
            get
            {
                if (socialRepository == null)
                {
                    socialRepository = new GenericRepository<Social>(context);
                }
                return socialRepository;
            }
        }


        public UnitOfWork(BibionDataDbContext ctx)
        {
            context = ctx;
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public async Task Save()
        {
            await context.SaveChangesAsync();
        }
      
        public IDbContextTransaction BeginTransaction()
        {
            return context.Database.BeginTransaction();
        }

        public async Task RollBackTransaction()
        {
            await context.Database.RollbackTransactionAsync();
        }
    
    }
}
