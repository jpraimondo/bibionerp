﻿using BibionERP.Persistence.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using BibionERP.Persistence.Persistence;
using Microsoft.EntityFrameworkCore;
using BibionERP.Application.Persistence.Interface;

namespace BibionERP.Persistence
{
    public static class PersistenceServiceRegistration
    {
        public static IServiceCollection AddPersistanceService(this IServiceCollection services, IConfiguration configuration)
        {

            var connectionString = configuration.GetConnectionString("WebApiDatabaseDev");
            services.AddDbContext<BibionDataDbContext>(options =>
            options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString)
            ));

            //var connectionString = configuration.GetConnectionString("WebApiDatabaseSql");

            //services.AddDbContextFactory<BibionDataDbContext>(options => options.UseSqlServer(connectionString).EnableSensitiveDataLogging());


            //var connectionString = configuration.GetConnectionString("LocalDb");

            //services.AddDbContextFactory<BibionDataDbContext>(options => options.UseSqlServer(connectionString).EnableSensitiveDataLogging());


            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }

    }
}
