﻿using BibionERP.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Persistence.Configuration
{
    internal class SupplierConfiguration : IEntityTypeConfiguration<Supplier>
    {
        public void Configure(EntityTypeBuilder<Supplier> builder)
        {
            builder.HasData(
                new Supplier
                {
                    Id = 1,
                    Name = "Productos Limpieza S.A.",
                    Document = "123456789",
                    ContactPerson = "Juan Pérez",
                    Email = "contacto@productoslimpieza.com",
                    Phone = "555-1234",
                    Address = "Calle 123",
                    Location = "Ciudad",
                    IsActive = true,
                    IsDelete = false,
                },
                new Supplier
                {
                    Id = 2,
                    Name = "Herramientas y Equipos S.L.",
                    Document = "987654321",
                    ContactPerson = "María Gómez",
                    Email = "contacto@herramientasyequipos.com",
                    Phone = "555-5678",
                    Address = "Calle 456",
                    Location = "Ciudad",
                    IsActive = true,
                    IsDelete = false,
                },
                new Supplier
                {
                    Id = 3,
                    Name = "Uniformes Corporativos S.A.",
                    Document = "456789123",
                    ContactPerson = "Pedro Rodríguez",
                    Email = "contacto@uniformescorporativos.com",
                    Phone = "555-2468",
                    Address = "Calle 789",
                    Location = "Ciudad",
                    IsActive = true,
                    IsDelete = false,
                },
                new Supplier
                {
                    Id = 4,
                    Name = "Suministros de Oficina S.L.",
                    Document = "654321789",
                    ContactPerson = "Ana García",
                    Email = "contacto@suministrosdeoficina.com",
                    Phone = "555-3691",
                    Address = "Calle 012",
                    Location = "Ciudad",
                    IsActive = true,
                    IsDelete = false,
                });
        }
    }
}
