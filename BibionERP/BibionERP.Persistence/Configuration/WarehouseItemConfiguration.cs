﻿using BibionERP.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Persistence.Configuration
{
    internal class WarehouseItemConfiguration : IEntityTypeConfiguration<WarehouseItem>
    {
        public void Configure(EntityTypeBuilder<WarehouseItem> builder)
        {
            builder.HasData(

                new WarehouseItem
                {
                    WarehouseId = 1,
                    ItemId = 1,
                    Quantity = 50,
                },
                new WarehouseItem
                {
                    WarehouseId = 1,
                    ItemId = 2,
                    Quantity = 30,
                },
                new WarehouseItem
                {
                    WarehouseId = 1,
                    ItemId = 3,
                    Quantity = 70,
                },
                new WarehouseItem
                {
                    WarehouseId = 1,
                    ItemId = 4,
                    Quantity = 50,
                },
                new WarehouseItem
                {
                    WarehouseId = 1,
                    ItemId = 5,
                    Quantity = 200,
                },
                new WarehouseItem
                {
                    WarehouseId = 1,
                    ItemId = 6,
                    Quantity = 150,
                },
                new WarehouseItem
                {
                    WarehouseId = 1,
                    ItemId = 7,
                    Quantity = 25,
                },
                new WarehouseItem
                {
                    WarehouseId = 1,
                    ItemId = 8,
                    Quantity = 20,
                },
                new WarehouseItem
                {
                    WarehouseId = 1,
                    ItemId = 9,
                    Quantity = 100,
                },
                new WarehouseItem
                {
                    WarehouseId = 1,
                    ItemId = 10,
                    Quantity = 120,
                },
                new WarehouseItem
                {
                    WarehouseId = 2,
                    ItemId = 1,
                    Quantity = 50,
                },
                new WarehouseItem
                {
                    WarehouseId = 2,
                    ItemId = 2,
                    Quantity = 70,
                },
                new WarehouseItem
                {
                    WarehouseId = 2,
                    ItemId = 7,
                    Quantity = 50,
                },
                new WarehouseItem
                {
                    WarehouseId = 2,
                    ItemId = 8,
                    Quantity = 100,
                },
                new WarehouseItem
                {
                    WarehouseId = 2,
                    ItemId = 9,
                    Quantity = 60,
                },
                new WarehouseItem
                {
                    WarehouseId = 2,
                    ItemId = 10,
                    Quantity = 10,
                },
                new WarehouseItem
                {
                    WarehouseId = 2,
                    ItemId = 11,
                    Quantity = 20,
                }
               );
        
        
        
        
        }
    }
}
