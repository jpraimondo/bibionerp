﻿using BibionERP.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Persistence.Configuration
{
    internal class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.HasData(
                new Employee
                {
                    Id = 1,
                    FirstName = "Juan",
                    LastName = "García",
                    Document = "12345678",
                    Email = "juan.garcia@example.com",
                    Phone = "099123456",
                    Address = "Av. Italia 1234",
                    Location = "Montevideo, Uruguay",
                    Birthday = new DateTime(1975, 11, 24),
                    JoiningDate = new DateTime(2022, 1, 1),
                    LeavingDate = new DateTime(2001, 1, 1),
                    IsActive = true,
                    IsDelete = false
                },
new Employee
{
    Id = 2,
    FirstName = "María",
    LastName = "Pérez",
    Document = "23456789",
    Birthday = new DateTime(1985, 5, 12),
    JoiningDate = new DateTime(2022, 1, 1),
    LeavingDate = new DateTime(2001,1,1),
    Email = "maria.perez@example.com",
    Phone = "099234567",
    Address = "Av. Libertador 5678",
    Location = "Canelones, Uruguay",
    IsActive = true,
    IsDelete = false
},
new Employee
{
    Id = 3,
    FirstName = "Pedro",
    LastName = "López",
    Document = "34567890",
    Email = "pedro.lopez@example.com",
    Phone = "099345678",
    Address = "Av. 18 de Julio 4321",
    Location = "Montevideo, Uruguay",
    Birthday = new DateTime(1993, 1,23),
    JoiningDate = new DateTime(2022, 1, 1),
    LeavingDate = new DateTime(2001, 1, 1),
    IsActive = true,
    IsDelete = false
},
new Employee
{
    Id = 4,
    FirstName = "Ana",
    LastName = "Martínez",
    Document = "45678901",
    Email = "ana.martinez@example.com",
    Phone = "099456789",
    Address = "Calle San José 876",
    Location = "San José, Uruguay",
    Birthday = new DateTime(1985, 5, 12),
    JoiningDate = new DateTime(2022, 1, 1),
    LeavingDate = new DateTime(2001, 1, 1),
    IsActive = true,
    IsDelete = false
},
new Employee
{
    Id = 5,
    FirstName = "Diego",
    LastName = "Gómez",
    Document = "56789012",
    Email = "diego.gomez@example.com",
    Phone = "099567890",
    Address = "Av. General Flores 987",
    Location = "Montevideo, Uruguay",
    Birthday = new DateTime(1985, 5, 12),
    JoiningDate = new DateTime(2022, 1, 1),
    LeavingDate = new DateTime(2001, 1, 1),
    IsActive = true,
    IsDelete = false
}) ;
        }
    }
}
