﻿using BibionERP.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanArchitecture.Identity.Configurations
{
    internal class RoleConfiguration : IEntityTypeConfiguration<ApplicationRol>
    {
        public void Configure(EntityTypeBuilder<ApplicationRol> builder)
        {
            builder.HasData(
                new ApplicationRol
                {
                    Id = 1,
                    Name = "Administrator",
                    NormalizedName="ADMINISTRATOR",
                    IsActive = true,
                    IsDelete = false,
                },
                new ApplicationRol
                {
                    Id = 2,
                    Name = "Operator",
                    NormalizedName = "OPERATOR",
                    IsActive = true,
                    IsDelete = false,
                },
                new ApplicationRol
                {
                    Id = 3,
                    Name = "Maintainer",
                    NormalizedName = "MAINTAINER",
                    IsActive = true,
                    IsDelete = false,
                }
                );
        }
    }
}
