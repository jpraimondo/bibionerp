﻿using BibionERP.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanArchitecture.Identity.Configurations
{
    internal class CategoryDeliveryNoteConfiguration : IEntityTypeConfiguration<CategoryDeliveryNote>
    {

        public void Configure(EntityTypeBuilder<CategoryDeliveryNote> builder)
        {
            builder.HasData(
                new CategoryDeliveryNote
                {
                    Id = 1,
                    Name = "Entrega de Uniforme",
                    Description="Boleta de entrega de uniforme a funcionario",
                    AddItem = false,
                    IsActive = true,
                    IsDelete = false,
                },
                new CategoryDeliveryNote
                {
                    Id = 2,
                    Name = "Entrega de Productos",
                    Description = "Boleta de entrega de productos para la utilizacion en servicio al funcionario",
                    AddItem = false,
                    IsActive = true,
                    IsDelete = false,
                },
                new CategoryDeliveryNote
                {
                    Id = 3,
                    Name = "Devolución de Uniforme",
                    Description = "Boleta de devolucion de uniforme por el funcionario",
                    AddItem = true,
                    IsActive = true,
                    IsDelete = false,
                },
                new CategoryDeliveryNote
                {
                    Id = 4,
                    Name = "Devolución de Materiales",
                    Description = "Boleta de devolucion de materiales por el funcionario",
                    AddItem = true,
                    IsActive = true,
                    IsDelete = false,
                },
                new CategoryDeliveryNote
                {
                    Id = 5,
                    Name = "Descartar Materiales",
                    Description = "Boleta de descarte de materiales o productos",
                    AddItem = false,
                    IsActive = true,
                    IsDelete = false,
                },
                new CategoryDeliveryNote
                {
                    Id = 6,
                    Name = "Compra de Materiales",
                    Description = "Registro de boleta de compra de materiales.",
                    AddItem = true,
                    IsActive = true,
                    IsDelete = false,
                },
                new CategoryDeliveryNote
                {
                    Id = 7,
                    Name = "Compra de Productos",
                    Description = "Registro de boleta de compra de productos.",
                    AddItem = true,
                    IsActive = true,
                    IsDelete = false,
                }
                );
        }
    }
}
