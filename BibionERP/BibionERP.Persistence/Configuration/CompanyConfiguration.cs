﻿using BibionERP.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Reflection.Metadata;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace BibionERP.Persistence.Configuration
{
    internal class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.HasData(
                new Company
                {
                    Id = 1,
                    Name = "ABC S.A.",
                    Description = "Empresa dedicada a la venta de electrodomésticos",
                    Document = "0123456789",
                    Address = "Av. del Libertador 1234",
                    Email = "ventas@abc.com",
                    Phone = "2700-1234",
                    CustomerId = 2,
                    IsDelete = false,
                    IsActive = true
                },
                new Company
                {
                    Id = 2,
                    Name = "XYZ S.A.",
                    Description = "Empresa de transporte de carga",
                    Document = "0234567890",
                    Address = "Calle Falsa 567",
                    Email = "info@xyz.com",
                    Phone = "2500-9876",
                    CustomerId = 4,
                    IsDelete = false,
                    IsActive = true
                },
                new Company
                {
                    Id = 3,
                    Name = "MNO S.A.",
                    Description = "Empresa de servicios de limpieza",
                    Document = "0345678901",
                    Address = "Av. de la Paz 456",
                    Email = "info@mno.com",
                    Phone = "2800-5678",
                    CustomerId = 1,
                    IsDelete = false,
                    IsActive = true
                },
                new Company
                {
                    Id = 4,
                    Name = "PQR S.A.",
                    Description = "Empresa de servicios de construcción",
                    Document = "0123456789",
                    Address = "Calle 123, Ciudad ABC",
                    Email = "info@pqr.com",
                    Phone = "555-1234",
                    CustomerId = 3,
                    IsDelete = false,
                    IsActive = true,
                },
                new Company
                {
                Id = 5,
                Name = "Restaurante XYZ",
                Description = "Comida internacional y bebidas",
                Document = "0987654321",
                Address = "Avenida 456, Ciudad XYZ",
                Email = "info@restaurante.xyz",
                Phone = "555-4321",
                CustomerId = 5,
                IsDelete = false,
                IsActive = true,
                }
                );
        }
    }
}
