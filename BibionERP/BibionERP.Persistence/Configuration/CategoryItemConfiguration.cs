﻿using BibionERP.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BibionERP.Persistence.Configuration
{
    internal class CategoryItemConfiguration : IEntityTypeConfiguration<CategoryItem>
    {
        public void Configure(EntityTypeBuilder<CategoryItem> builder)
        {
            builder.HasData(
                new CategoryItem
                {
                    Id=1,
                    Name= "Productos de Limpieza",
                    Description= "Artículos de limpieza para uso general",
                    IsActive= true,
                    IsDelete= false,

                },
                new CategoryItem
                {
                    Id = 2,
                    Name = "Herramientas de Limpieza",
                    Description = "erramientas y accesorios para la limpieza",
                    IsActive = true,
                    IsDelete = false,

                },
                new CategoryItem
                {
                    Id = 3,
                    Name = "Uniformes de Personal",
                    Description = "Ropa y accesorios para el personal de limpieza",
                    IsActive = true,
                    IsDelete = false,

                } );
        }
    }
}
