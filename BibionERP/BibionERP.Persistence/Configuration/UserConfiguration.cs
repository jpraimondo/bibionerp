﻿using BibionERP.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanArchitecture.Identity.Configurations
{
    internal class UserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {

        

        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            var hasher = new PasswordHasher<ApplicationUser>();

            var usuarios = new List<ApplicationUser>();

            var usuario = new ApplicationUser 
            {
                Id = 1,
                Email = "admin@localhost.com",
                FirstName = "Admin",
                LastName = "User",
                Document = "99999999",
                EmployeeId = 123,
                Address = "Mi direccion 1454",
                Location = "Montevideo",
                UserName = "administrator",
                RolId = 1,
                IsActive = true,
                IsDelete = false,
            };

            usuario.HashPassword = hasher.HashPassword(usuario, "MiPassword");

            usuarios.Add(usuario);


            var usuario2 = new ApplicationUser
            {
                Id = 2,
                Email = "user@localhost.com",
                FirstName = "User",
                LastName = "User",
                Document = "99999999",
                EmployeeId = 123,
                Address = "Mi direccion 1454",
                Location = "Montevideo",
                UserName = "usuario",
                RolId = 2,
                IsActive = true,
                IsDelete = false,
            };

            usuario2.HashPassword = hasher.HashPassword(usuario2, "MiPassword");

            usuarios.Add(usuario2);

            var usuario3 = new ApplicationUser
            {
                Id = 3,
                Email = "mainteiner@localhost.com",
                FirstName = "Mainteiner",
                LastName = "User",
                Document = "99999999",
                EmployeeId = 123,
                Address = "Mi direccion 1454",
                Location = "Montevideo",
                UserName = "mainteiner",
                RolId = 3,
                IsActive = true,
                IsDelete = false,
            };

            usuario3.HashPassword = hasher.HashPassword(usuario3, "MiPassword");

            usuarios.Add(usuario3);


            builder.HasData(usuarios);

        }
    }
}
