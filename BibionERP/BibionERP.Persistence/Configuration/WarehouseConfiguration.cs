﻿using BibionERP.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanArchitecture.Identity.Configurations
{
    internal class WarehouseConfiguration : IEntityTypeConfiguration<Warehouse>
    {
        public void Configure(EntityTypeBuilder<Warehouse> builder)
        {
            builder.HasData(
                new Warehouse
                {
                    Id = 1,
                    Name = "Sede Central",
                    Description = "Lo que se almacena en el local principal",
                    IsActive = true,
                    IsDelete = false,

                },
                new Warehouse
                {
                    Id = 2,
                    Name = "Deposito Central",
                    Description = "Lo que se almacena en el deposito",
                    IsActive = true,
                    IsDelete = false,
                }
                );
        }
    }
}
