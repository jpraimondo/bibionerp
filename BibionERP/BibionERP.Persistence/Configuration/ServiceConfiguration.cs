﻿using BibionERP.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Persistence.Configuration
{
    internal class ServiceConfiguration : IEntityTypeConfiguration<Service>
    {
        public void Configure(EntityTypeBuilder<Service> builder)
        {
            builder.HasData(
                new Service
                {
                    Id = 1,
                    Name = "Limpieza de oficinas",
                    Description = "Servicio de limpieza para oficinas y espacios de trabajo",
                    CostPrice = 50.00m,
                    TotalPrice = 100.00m,
                    IsActive = true,
                    IsDelete = false
                },
                new Service
                {
                    Id = 2,
                    Name = "Limpieza de hogares",
                    Description = "Servicio de limpieza para hogares y residencias",
                    CostPrice = 30.00m,
                    TotalPrice = 60.00m,
                    IsActive = true,
                    IsDelete = false
                },
                new Service
                {
                    Id = 3,
                    Name = "Limpieza de vidrios",
                    Description = "Servicio de limpieza de vidrios y superficies acristaladas",
                    CostPrice = 75.00m,
                    TotalPrice = 150.00m,
                    IsActive = true,
                    IsDelete = false
                },
                new Service
                {
                    Id = 4,
                    Name = "Limpieza de alfombras",
                    Description = "Servicio de limpieza de alfombras y tapices",
                    CostPrice = 80.00m,
                    TotalPrice = 160.00m,
                    IsActive = true,
                    IsDelete = false
                },
                new Service
                {
                    Id = 5,
                    Name = "Seguridad privada",
                    Description = "Servicio de seguridad privada para eventos y locales",
                    CostPrice = 120.00m,
                    TotalPrice = 240.00m,
                    IsActive = true,
                    IsDelete = false
                }
                );
        }
    }
}
