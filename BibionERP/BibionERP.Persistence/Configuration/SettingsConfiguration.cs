﻿using BibionERP.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Persistence.Configuration
{
    internal class SettingsConfiguration : IEntityTypeConfiguration<Settings>
    {
        public void Configure(EntityTypeBuilder<Settings> builder)
        {
            builder.HasData(
                new Settings
                {
                    Id = 1,
                    CompanyName = "Empresa de Servicios.",
                    CompanyType = "Servicios",
                    CompanyDescription = "Empresa de servicios de limpieza y seguridad",
                    CompanyPhoneNumber = "+598 2 555 5555",
                    CompanyEmail = "contacto@servicioslimpiezaseguridad.com",
                    CompanyDirection = "Av. 18 de Julio 1234",
                    CompanyLocation = "Montevideo, Uruguay",
                    CompanyImageLogo = "/img/logo_solo.png",
                    CompanyImageTitle = "Servicios de Limpieza y Seguridad",
                    CompanyWebTitle = "www.servicioslimpiezaseguridad.com",
                    CreateBy="System",
                    CreateTime=DateTime.Now
                });
        }
    }
}
