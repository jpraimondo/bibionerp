﻿using BibionERP.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BibionERP.Persistence.Configuration
{
    internal class ItemConfiguration : IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> builder)
        {
            builder.HasData(
                new Item
                {
                    Id = 1,
                    Name = "Blusa de mujer",
                    Description = "Blusa de algodón con cuello redondo y mangas cortas",
                    CostPrice = 10m,
                    TradePrice = 20m,
                    SupplierId = 3,
                    CategoryItemId = 3,
                    Code = "BL001",
                    ManufactureDate = DateTime.Now.AddDays(-763),
                    ExpirationDate = DateTime.Now.AddDays(763),
                    Note = "Colores disponibles: blanco, negro, rojo",
                    IsActive = true,
                    IsDelete = false,
                },
                new Item
                {
                    Id = 2,
                    Name = "Detergente en polvo",
                    Description = "Detergente en polvo para la limpieza de ropa blanca y de color",
                    CostPrice = 5.00m,
                    TradePrice = 7.50m,
                    SupplierId = 1,
                    CategoryItemId = 1,
                    Code = "CLEAN-001",
                    ManufactureDate = new DateTime(2022, 01, 01),
                    ExpirationDate = new DateTime(2024, 01, 01),
                    Note = "Producto de alta calidad",
                    IsActive = true,
                    IsDelete = false
                },
                new Item
                {
                    Id = 3,
                    Name = "Jabón líquido",
                    Description = "Jabón líquido para la limpieza de manos y cuerpo",
                    CostPrice = 2.50m,
                    TradePrice = 4.00m,
                    SupplierId = 1,
                    CategoryItemId = 1,
                    Code = "CLEAN-002",
                    ManufactureDate = new DateTime(2022, 02, 01),
                    ExpirationDate = new DateTime(2023, 02, 01),
                    Note = "Producto orgánico y ecológico",
                    IsActive = true,
                    IsDelete = false
                },

                new Item
                {
                    Id = 4,
                    Name = "Toallas de papel",
                    Description = "Toallas de papel absorbentes para la limpieza y secado",
                    CostPrice = 1.50m,
                    TradePrice = 2.50m,
                    SupplierId = 1,
                    CategoryItemId = 1,
                    Code = "CLEAN-003",
                    ManufactureDate = new DateTime(2021, 12, 01),
                    ExpirationDate = new DateTime(2023, 12, 01),
                    Note = "Producto de uso diario y alta calidad",
                    IsActive = true,
                    IsDelete = false
                },

                new Item
                {
                    Id = 5,
                    Name = "Desinfectante de superficies",
                    Description = "Desinfectante para la limpieza y desinfección de superficies",
                    CostPrice = 4.00m,
                    TradePrice = 6.50m,
                    SupplierId = 1,
                    CategoryItemId = 1,
                    Code = "CLEAN-004",
                    ManufactureDate = new DateTime(2022, 03, 01),
                    ExpirationDate = new DateTime(2024, 03, 01),
                    Note = "Producto con fragancia agradable",
                    IsActive = true,
                    IsDelete = false
                },

                new Item
                {
                    Id = 6,
                    Name = "Cepillo para limpieza",
                    Description = "Cepillo de cerdas duras para la limpieza de superficies resistentes",
                    CostPrice = 3.50m,
                    TradePrice = 5.00m,
                    SupplierId = 2,
                    CategoryItemId = 1,
                    Code = "CLEAN-005",
                    ManufactureDate = new DateTime(2022, 04, 01),
                    ExpirationDate = new DateTime(2025, 04, 01),
                    Note = "Cepillo de alta calidad",
                    IsActive = true,
                    IsDelete = false
                },
                new Item
                {
                    Id = 7,
                    Name = "Camisa Polo",
                    Description = "Camisa Polo de alta calidad para uniformes empresariales",
                    CostPrice = 25m,
                    TradePrice = 50m,
                    SupplierId = 3,
                    CategoryItemId = 3,
                    Code = "UNIF001",
                    ManufactureDate = new DateTime(2022, 1, 15),
                    ExpirationDate = new DateTime(2023, 1, 15),
                    Note = "Color: Blanco",
                    IsActive = true,
                    IsDelete = false,
                },
                new Item
                {
                    Id = 8,
                    Name = "Pantalón de Vestir",
                    Description = "Pantalón de Vestir para uniformes empresariales",
                    CostPrice = 40m,
                    TradePrice = 80m,
                    SupplierId = 3,
                    CategoryItemId = 3,
                    Code = "UNIF002",
                    ManufactureDate = new DateTime(2022, 2, 5),
                    ExpirationDate = new DateTime(2023, 2, 5),
                    Note = "Color: Negro",
                    IsActive = true,
                    IsDelete = false,
                },
                new Item
                {
                    Id = 9,
                    Name = "Chaqueta de Algodon",
                    Description = "Chaqueta de algodon para uniformes empresariales",
                    CostPrice = 80m,
                    TradePrice = 160m,
                    SupplierId = 3,
                    CategoryItemId = 3,
                    Code = "UNIF003",
                    ManufactureDate = new DateTime(2022, 3, 20),
                    ExpirationDate = new DateTime(2023, 3, 20),
                    Note = "Color: Marrón",
                    IsActive = true,
                    IsDelete = false,
                },
                new Item
                {
                    Id = 10,
                    Name = "Blusa",
                    Description = "Blusa de alta calidad para uniformes empresariales",
                    CostPrice = 20m,
                    TradePrice = 40m,
                    SupplierId = 3,
                    CategoryItemId = 3,
                    Code = "UNIF004",
                    ManufactureDate = new DateTime(2022, 4, 10),
                    ExpirationDate = new DateTime(2023, 4, 10),
                    Note = "Color: Azul",
                    IsActive = true,
                    IsDelete = false,
                },
                new Item
                {
                    Id = 11,
                    Name = "Falda",
                    Description = "Falda para uniformes empresariales",
                    CostPrice = 30m,
                    TradePrice = 60m,
                    SupplierId = 3,
                    CategoryItemId = 3,
                    Code = "UNIF005",
                    ManufactureDate = new DateTime(2022, 5, 12),
                    ExpirationDate = new DateTime(2023, 5, 12),
                    Note = "Color: Gris",
                    IsActive = true,
                    IsDelete = false,
                }

            );
        }
    }
}
