﻿using BibionERP.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Persistence.Configuration
{
    internal class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasData(
                new Customer
                {
                    Id = 1,
                    FirstName = "Juan",
                    LastName = "Pérez",
                    CustomerType = "Particular",
                    Phone = "0987654321",
                    Email = "juanperez@mail.com",
                    Address = "Av. Siempreviva 123",
                    Notes = "Cliente fiel desde hace 5 años",
                    IsDelete = false,
                    IsActive = true
                },
new Customer
{
    Id = 2,
    FirstName = "María",
    LastName = "González",
    CustomerType = "Empresa",
    Phone = "0998765432",
    Email = "mariagonzalez@mail.com",
    Address = "Calle Falsa 123",
    Notes = "Cliente nuevo",
    IsDelete = false,
    IsActive = true
},
new Customer
{
    Id = 3,
    FirstName = "Pedro",
    LastName = "Ramírez",
    CustomerType = "Particular",
    Phone = "0976543210",
    Email = "pedroramirez@mail.com",
    Address = "Av. Primavera 456",
    Notes = "Cliente frecuente",
    IsDelete = false,
    IsActive = true
},
new Customer
{
    Id = 4,
    FirstName = "Ana",
    LastName = "Martínez",
    CustomerType = "Empresa",
    Phone = "0912345678",
    Email = "anamartinez@mail.com",
    Address = "Calle Ficticia 789",
    Notes = "Cliente potencial",
    IsDelete = false,
    IsActive = true
},
new Customer
{
    Id = 5,
    FirstName = "José",
    LastName = "Sánchez",
    CustomerType = "Particular",
    Phone = "0923456789",
    Email = "josesanchez@mail.com",
    Address = "Av. Real 789",
    Notes = "",
    IsDelete = false,
    IsActive = true
}
                );
        }
    }
}
