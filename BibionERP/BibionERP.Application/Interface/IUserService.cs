﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface IUserService
    {
        Task<IEnumerable<ViewUserDto>> GetAll();
        Task<IEnumerable<ViewUserDto>> GetAllAndDelete();
        Task<ViewDetailUserDto> GetById(int id);
        Task<IEnumerable<ViewUserDto>> Find(Expression<Func<ApplicationUser, bool>> predicate);
        public Task<ViewDetailUserDto> Insert(CreateUserDto userDto, string userName = "Not User");
        public Task<ViewDetailUserDto> Update(CreateUserDto userDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
        Task<bool> EnableDisableUser(int id, string userName);
        Task<ViewDetailUserDto> ResetPassword(CreateUserDto userDto, int id, string userName = "Not User");
    }
}
