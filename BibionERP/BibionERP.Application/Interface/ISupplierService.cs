﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface ISupplierService
    {
        Task<IEnumerable<ViewSupplierDto>> GetAll();
        Task<IEnumerable<ViewSupplierDto>> GetAllAndDelete();
        Task<ViewDetailSupplierDto> GetById(int id);
        Task<IEnumerable<ViewSupplierDto>> Find(Expression<Func<Supplier, bool>> predicate);
        public Task<ViewDetailSupplierDto> Insert(CreateSupplierDto supplierDto, string userName = "Not User");
        public Task<ViewDetailSupplierDto> Update(CreateSupplierDto supplierDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
