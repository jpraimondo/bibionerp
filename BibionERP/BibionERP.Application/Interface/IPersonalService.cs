﻿using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Helpers;

namespace BibionERP.Application.Interface
{
    public interface IPersonalService
    {

        Task<IEnumerable<ViewDetailPersonalDto>> GetAll();

        Task<bool> Delete(int id);
        Task<PersonalData> GetPersonalData();
    }
}