﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface IRolService
    {
        Task<IEnumerable<ViewDetailRolDto>> GetAll();
        Task<IEnumerable<ViewDetailRolDto>> GetAllAndDelete();
        Task<ViewDetailRolDto> GetById(int id);
        Task<IEnumerable<ViewDetailRolDto>> Find(Expression<Func<ApplicationRol, bool>> predicate);
        public Task<ViewDetailRolDto> Insert(CreateRolDto rolDto, string userName = "Not User");
        public Task<ViewDetailRolDto> Update(CreateRolDto rolDto, int id, string userName = "Not User");
    }
}
