﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface ICategoryDeliveryNoteService
    {
        Task<IEnumerable<ViewDetailCategoryDeliveryNote>> GetAll();
        Task<ViewDetailCategoryDeliveryNote> GetById(int id);
        Task<IEnumerable<ViewDetailCategoryDeliveryNote>> Find(Expression<Func<CategoryDeliveryNote, bool>> predicate);
        public Task<ViewDetailCategoryDeliveryNote> Insert(CreateCategoryDeliveryNote categoryDetailDto, string userName = "Not User");
        public Task<ViewDetailCategoryDeliveryNote> Update(CreateCategoryDeliveryNote categoryDetailDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
