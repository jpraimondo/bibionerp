﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;

namespace BibionERP.Application.Interface
{
    public interface IAuthService
    {

        Task<ViewAuthDto> Login(CreateAuthDto request);

        //Task<ViewAuthDto> Register(RegisterAuthDto request);

        Task<ViewAuthDto> ChangePassword(RegisterAuthDto request);


    }
}
