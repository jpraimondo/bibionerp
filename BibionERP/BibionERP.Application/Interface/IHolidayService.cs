﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface IHolidayService
    {
        Task<IEnumerable<ViewHolidayDto>> GetAll();
        Task<IEnumerable<ViewHolidayDto>> GetAllAndDelete();
        Task<ViewDetailHolidayDto> GetById(int id);
        Task<IEnumerable<ViewHolidayDto>> Find(Expression<Func<Holiday, bool>> predicate);
        public Task<ViewDetailHolidayDto> Insert(CreateHolidayDto holidayDto, string userName = "Not User");
        public Task<ViewDetailHolidayDto> Update(CreateHolidayDto holidayDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
