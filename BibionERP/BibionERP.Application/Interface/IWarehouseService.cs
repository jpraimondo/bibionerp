﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface IWarehouseService
    {
        Task<IEnumerable<ViewWarehouseDto>> GetAll();
        Task<IEnumerable<ViewWarehouseDto>> GetAllAndDelete();
        Task<ViewDetailWarehouseDto> GetById(int id);
        Task<IEnumerable<ViewWarehouseDto>> Find(Expression<Func<Warehouse, bool>> predicate);
        public Task<ViewDetailWarehouseDto> Insert(CreateWarehouseDto warehouseDto, string userName = "Not User");
        public Task<ViewDetailWarehouseDto> Update(CreateWarehouseDto warehouseDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
