﻿using BibionERP.Application.DTOs;
using BibionERP.Application.DTOs.Create;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface IResponseService
    {
        Task<IEnumerable<ResponseMessage>> GetAll();
        Task<IEnumerable<ResponseMessage>> GetAllAndDelete();
        Task<ResponseMessage> GetById(int id);
        Task<IEnumerable<ResponseMessage>> Find(Expression<Func<Employee, bool>> predicate);
        public Task<ResponseMessage> Insert(CreateCompanyDto companyDto, string userName = "Not User");
        public Task<ResponseMessage> Update(CreateCompanyDto companyDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
