﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface ICompanyService
    {
        Task<IEnumerable<ViewCompanyDto>> GetAll();
        Task<IEnumerable<ViewCompanyDto>> GetAllAndDelete();
        Task<ViewDetailCompanyDto> GetById(int id);
        Task<IEnumerable<ViewCompanyDto>> Find(Expression<Func<Company, bool>> predicate);
        public Task<ViewDetailCompanyDto> Insert(CreateCompanyDto companyDto, string userName = "Not User");
        public Task<ViewDetailCompanyDto> Update(CreateCompanyDto companyDto, int id, string userName = "Not User");
        public Task<ViewDetailCompanyDto> UpdateServices(int id, List<CreateCompanyServiceDto> listServicesDto, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
