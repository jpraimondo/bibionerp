﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface IEmployeeService
    {

        Task<IEnumerable<ViewEmployeeDto>> GetAll();
        Task<IEnumerable<ViewEmployeeDto>> GetAllAndDelete();
        Task<ViewDetailEmployeeDto> GetById(int id);
        Task<IEnumerable<ViewEmployeeDto>> Find(Expression<Func<Employee, bool>> predicate);
        public Task<ViewDetailEmployeeDto> Insert(CreateEmployeeDto employeeDto, string userName = "Not User");
        public Task<ViewDetailEmployeeDto> Update(CreateEmployeeDto employeeDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");

    }
}
