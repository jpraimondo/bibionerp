﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface IHolidayEmployeeService
    {
        Task<IEnumerable<ViewHolidayEmployeeDto>> GetAll();
        Task<IEnumerable<ViewHolidayEmployeeDto>> GetAllAndDelete();
        Task<ViewHolidayEmployeeDto> GetById(int id);
        Task<IEnumerable<ViewHolidayEmployeeDto>> Find(Expression<Func<HolidayEmployee, bool>> predicate);
        public Task<ViewHolidayEmployeeDto> Insert(CreateHolidayEmployeeDto holidayEmployeeDto, string userName = "Not User");
        public Task<ViewHolidayEmployeeDto> Update(CreateHolidayEmployeeDto holidayEmployeeDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
