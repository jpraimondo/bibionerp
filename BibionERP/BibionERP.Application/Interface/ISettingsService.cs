﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.ViewDetail;

namespace BibionERP.Application.Interface
{
    public interface ISettingsService
    {
        Task<ViewDetailSettingsDto> GetSettings();
        Task<ViewDetailSettingsDto> UpdateSettings(CreateSettingsDto settingsDto, string userName = "Not User");
        Task<ViewDetailSettingsDto> GetSettingsNow();
        
    }
}
