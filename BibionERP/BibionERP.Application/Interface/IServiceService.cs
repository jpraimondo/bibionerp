﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface IServiceService
    {
        Task<IEnumerable<ViewServiceDto>> GetAll();
        Task<IEnumerable<ViewServiceDto>> GetAllAndDelete();
        Task<ViewDetailServiceDto> GetById(int id);
        Task<IEnumerable<ViewServiceDto>> Find(Expression<Func<Service, bool>> predicate);
        public Task<ViewDetailServiceDto> Insert(CreateServiceDto serviceDto, string userName = "Not User");
        public Task<ViewDetailServiceDto> Update(CreateServiceDto serviceDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
