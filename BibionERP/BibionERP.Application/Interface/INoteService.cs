﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface INoteService
    {
        Task<IEnumerable<ViewNoteDto>> GetAll();
        Task<IEnumerable<ViewNoteDto>> GetAllAndDelete();
        Task<ViewNoteDto> GetById(int id);
        Task<IEnumerable<ViewNoteDto>> Find(Expression<Func<Note, bool>> predicate);
        public Task<ViewNoteDto> Insert(CreateNoteDto noteDto, string userName = "Not User");
        public Task<ViewNoteDto> Update(CreateNoteDto noteDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
