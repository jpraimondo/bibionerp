﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface IDeliveryNoteService
    {
        Task<IEnumerable<ViewDeliveryNoteDto>> GetAll();
        Task<IEnumerable<ViewDeliveryNoteDto>> GetAllAndDelete();
        Task<ViewDetailDeliveryNoteDto> GetById(int id);
        Task<IEnumerable<ViewDeliveryNoteDto>> Find(Expression<Func<DeliveryNote, bool>> predicate);
        public Task<ViewDetailDeliveryNoteDto> Insert(CreateDeliveryNoteDto deliverNoteDto,List<CreateDeliveryNoteDetailDto> deliveryNoteDetailsDto, string userName = "Not User");
        public Task<ViewDetailDeliveryNoteDto> Update(CreateDeliveryNoteDto deliverNoteDto, int id, List<CreateDeliveryNoteDetailDto> deliveryNoteDetailsDto, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
