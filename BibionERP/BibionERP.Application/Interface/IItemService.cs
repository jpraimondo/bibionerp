﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface IItemService
    {
        Task<IEnumerable<ViewItemDto>> GetAll();
        Task<IEnumerable<ViewItemDto>> GetAllAndDelete();
        Task<ViewDetailItemDto> GetById(int id);
        Task<IEnumerable<ViewItemDto>> Find(Expression<Func<Item, bool>> predicate,int id = 0);
        public Task<ViewDetailItemDto> Insert(CreateItemDto itemDto, string userName = "Not User");
        public Task<ViewDetailItemDto> Update(CreateItemDto itemDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");

    }
}
