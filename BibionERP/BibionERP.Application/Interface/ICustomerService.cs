﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface ICustomerService
    {
        Task<IEnumerable<ViewCustomerDto>> GetAll();
        Task<IEnumerable<ViewCustomerDto>> GetAllAndDelete();
        Task<ViewDetailCustomerDto> GetById(int id);
        Task<IEnumerable<ViewCustomerDto>> Find(Expression<Func<Customer, bool>> predicate);
        public Task<ViewDetailCustomerDto> Insert(CreateCustomerDto customerDto, string userName = "Not User");
        public Task<ViewDetailCustomerDto> Update(CreateCustomerDto customerDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
