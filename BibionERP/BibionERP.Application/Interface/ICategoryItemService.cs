﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface ICategoryItemService
    {

        Task<IEnumerable<ViewCategoryItemDto>> GetAll();
        Task<ViewDetailCategoryItemDto> GetById(int id);
        Task<IEnumerable<ViewCategoryItemDto>> Find(Expression<Func<CategoryItem, bool>> predicate);
        public Task<ViewCategoryItemDto> Insert(CreateCategoryItemDto categoryItemDto, string userName = "Not User");
        public Task<ViewCategoryItemDto> Update(CreateCategoryItemDto categoryItemDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");

    }
}
