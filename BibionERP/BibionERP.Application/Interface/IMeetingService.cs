﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Interface
{
    public interface IMeetingService
    {
        Task<IEnumerable<ViewMeetingDto>> GetAll();
        Task<IEnumerable<ViewMeetingDto>> GetAllAndDelete();
        Task<ViewDetaiLeaveDto> GetById(int id);
        Task<IEnumerable<ViewMeetingDto>> Find(Expression<Func<Meeting, bool>> predicate);
        public Task<ViewDetaiLeaveDto> Insert(CreateMeetingDto meetingDto, string userName = "Not User");
        public Task<ViewDetaiLeaveDto> Update(CreateMeetingDto meetingDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
