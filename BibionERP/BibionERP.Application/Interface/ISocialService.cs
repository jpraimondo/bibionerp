﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;

namespace BibionERP.Application.Interface
{
    public interface ISocialService
    {
        Task<IEnumerable<ViewSocialDto>> GetAll();
        Task<IEnumerable<ViewSocialDto>> GetAllAndDelete();
        Task<ViewSocialDto> GetById(int id);
        public Task<ViewSocialDto> Insert(CreateSocialDto socialDto, string userName = "Not User");
        public Task<ViewSocialDto> Update(CreateSocialDto socialDto, int id, string userName = "Not User");
        Task<bool> Delete(int id, string userName = "Not User");
    }
}
