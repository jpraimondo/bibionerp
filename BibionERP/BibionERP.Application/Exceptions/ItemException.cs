﻿using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace BibionERP.Application.Exceptions
{
    public class ItemException:Exception
    {
        

        public ItemException(string message,ILogger logger):base(message)
        {
            
            logger.LogError(message);
        }
    }
}
