﻿using BibionERP.Domain.Email;

namespace BibionERP.Application.Infraestructure.Interface
{
    public interface IEmailService
    {
        Task<bool> SendEmail(Email email);
    }
}
