﻿using BibionERP.Domain.Entities;
using BibionERP.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateHolidayEmployeeDto
    {

        public string HolidayName { get; set; } = string.Empty;

        public int EmployeeId { get; set; }
       
        public DateTime? HolidayExpiration { get; set; } = DateTime.MaxValue;

        public int HolidayTypeId { get; set; }

        public int HolidayDays { get; set; }

        public bool RequiredDocumentation { get; set; } = false;

    }
}
