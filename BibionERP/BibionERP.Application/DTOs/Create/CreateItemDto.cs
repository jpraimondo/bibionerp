﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateItemDto
    {
        [Required]
        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public decimal CostPrice { get; set; } = decimal.Zero;

        public decimal TradePrice { get; set; } = decimal.Zero;

        public int CategoryId { get; set; }

        public int SupplierId { get; set; }

        public List<CreateWarehouseItemDto>? CreateWarehouseItems { get; set; }

        public string Code { get; set; } = string.Empty;

        public DateOnly? ManufactureDate { get; set; }

        public DateOnly? ExpirationDate { get; set; }

        public string Note { get; set; } = string.Empty;
    }

    public class CreateWarehouseItemDto
    {
        public int WarehouseId { get; set; }

        public int Quantity { get; set; }
    }
}
