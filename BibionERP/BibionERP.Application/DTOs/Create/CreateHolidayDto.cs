﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateHolidayDto
    {

        [Required]
        public int EmployeeId { get; set; }

        [Required]
        public int HolidayTypeId { get; set; }

        [Required]
        public DateTime StartLeave { get; set; } = DateTime.MinValue;

        public DateTime EndLeave { get; set; } = DateTime.MinValue;

        [Required]
        public int ApproverId { get; set; }

        public int HolidayStatusId { get; set; }

        public string Details { get; set; } = string.Empty;

    }
}
