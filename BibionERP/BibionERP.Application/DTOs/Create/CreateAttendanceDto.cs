﻿namespace BibionERP.Application.DTOs.Create
{
    public class CreateAttendanceDto
    {

        public int AttendanceId { get; set; }

        public string AttendanceType { get; set; } = string.Empty;

    }
}
