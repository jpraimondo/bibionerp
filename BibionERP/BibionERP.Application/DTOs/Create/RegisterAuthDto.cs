﻿namespace BibionERP.Application.DTOs.Create
{
    public class RegisterAuthDto
    {
        public string Nombre { get; set; } = string.Empty;

        public string Apellido { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public int EmployeeId { get; set; }

        public string Password { get; set; } = string.Empty;

    }
}
