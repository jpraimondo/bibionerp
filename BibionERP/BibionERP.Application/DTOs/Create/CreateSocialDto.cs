﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateSocialDto
    {
        [MaxLength(50)]
        public string Name { get; set; } = string.Empty;

        [MaxLength(255)]
        public string URL { get; set; } = string.Empty;

        [MaxLength(50)]
        public string Icons { get; set; } = string.Empty;

        public bool Visible { get; set; } = false;
    }
}
