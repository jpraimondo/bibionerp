﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateSupplierDto
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        [Required]
        public string Document { get; set; } = string.Empty;

        public string ContactPerson { get; set; } = string.Empty;
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; } = string.Empty;

        public string Phone { get; set; } = string.Empty;

        public string Address { get; set; } = string.Empty;

        public string Location { get; set; } = string.Empty;
    }
}
