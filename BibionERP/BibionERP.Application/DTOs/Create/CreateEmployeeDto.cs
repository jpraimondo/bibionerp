﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateEmployeeDto
    {
        [Required]
        public string FirstName { get; set; } = string.Empty;

        [Required]
        public string LastName { get; set; } = string.Empty;

        [Required]
        public string Document { get; set; } = string.Empty;

        public DateOnly? Birthday { get; set; } = DateOnly.MinValue;

        public string Designature { get; set; } = string.Empty;

        public string Section { get; set; } = string.Empty;

        public DateOnly? JoiningDate { get; set; } = DateOnly.MinValue;

        public DateOnly? LeavingDate { get; set; } = DateOnly.MinValue;
        [Required]
        public string Phone { get; set; } = string.Empty;
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; } = string.Empty;

        public string Address { get; set; } = string.Empty;

        public string Location { get; set; } = string.Empty;
    }
}
