﻿namespace BibionERP.Application.DTOs.Create
{
    public class CreateCategoryDeliveryNote
    {
        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public bool addItem { get; set; } = false;
    }
}
