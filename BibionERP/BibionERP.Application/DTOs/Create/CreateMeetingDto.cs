﻿using BibionERP.Application.DTOs.View;
using System.ComponentModel.DataAnnotations;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateMeetingDto
    {

        public ICollection<ViewAttendanceDto>? AttendanceList;

        public DateTime? Date { get; set; } = DateTime.Now;

        public string Location { get; set; } = string.Empty;

        public string Notes { get; set; } = string.Empty;

        public string Task { get; set; } = string.Empty;

    }
}
