﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateSettingsDto
    {

        [Required]
        public string CompanyName { get; set; } = string.Empty;

        public string CompanyType { get; set; } = string.Empty;

        public string CompanyDescription { get; set; } = string.Empty;
        [Required]
        public string CompanyPhoneNumber { get; set; } = string.Empty;
        [Required]
        public string CompanyEmail { get; set; } = string.Empty;
        [Required]
        public string CompanyDirection { get; set; } = string.Empty;

        public string CompanyLocation { get; set; } = string.Empty;

        public string CompanyImageLogo { get; set; } = string.Empty;

        public string CompanyImageTitle { get; set; } = string.Empty;
        [Required]
        public string CompanyWebTitle { get; set; } = string.Empty;

    }
}
