﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateDeliveryNoteDto
    {
        [Required]
        public int EmployeeId { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        public int WarehouseId { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

        public DateOnly? StartDate { get; set; }

        public DateOnly? EndDate { get; set; }

        public string Location { get; set; } = string.Empty;

        public string Note { get; set; } = string.Empty;

        
       
    }
}
