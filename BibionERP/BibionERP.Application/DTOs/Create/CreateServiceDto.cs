﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateServiceDto
    {
        [Required]
        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public decimal CostPrice { get; set; } = decimal.Zero;
        [Required]
        public decimal TotalPrice { get; set; } = decimal.Zero;
    }
}
