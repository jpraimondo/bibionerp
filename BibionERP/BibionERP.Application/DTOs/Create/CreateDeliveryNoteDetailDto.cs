﻿using System.ComponentModel.DataAnnotations;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateDeliveryNoteDetailDto
    {
        [Required]
        public int DeliveryNoteId { get; set; }
        [Required]
        public int ItemId { get; set; }

        public string ItemName { get; set; } = string.Empty;

        public int Quantity { get; set; } = 1;
    }
}
