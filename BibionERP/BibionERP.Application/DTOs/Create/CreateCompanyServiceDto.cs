﻿using BibionERP.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateCompanyServiceDto
    {

        [Required]
        public int CompanyId { get; set; }

        [Required]
        public int ServiceId { get; set; }

        public string ServiceName { get; set; } = string.Empty;

        public DateTime? StartDate { get; set; } = DateTime.Now;

        public DateTime? EndDate { get; set; } = DateTime.Now;

        public int HoursWeek { get; set; } = 1;

        public string RepeatDays { get; set; } = string.Empty;

        public decimal TotalCost { get; set; } = decimal.Zero;

        public decimal DiscountPercentage { get; set; } = decimal.Zero;

        public decimal FinalPrice { get; set; } = decimal.Zero;

    }
}
