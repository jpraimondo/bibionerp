﻿using BibionERP.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace BibionERP.Application.DTOs.Create
{
    public class CreateNoteDto
    {
        [Required]
        public int UserSenderId { get; set; }
        [Required]
        public int UserReceiverId { get; set; }

        [Required(ErrorMessage = "El titulo es requerido.")]
        [MaxLength(250,ErrorMessage ="El titulo de la nota no puede superar los 250 caracteres.")]
        public string Title { get; set; } = string.Empty;
        [Required(ErrorMessage = "El mensaje es requerido.")]
        public string Message { get; set; } = string.Empty;

        public bool IsTask { get; set; } = false;

        public bool IsCompleted { get; set; } = false;

        public ICollection<string>? Tags { get; set; }

       

    }
}
