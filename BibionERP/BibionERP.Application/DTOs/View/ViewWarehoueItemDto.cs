﻿namespace BibionERP.Application.DTOs.View
{

    public class ViewWarehoueItemDto
    {
        public int WarehouseId { get; set; }

        public string WarehouseName { get; set; } = string.Empty;

        public int Quantity { get; set; }
    }

}
