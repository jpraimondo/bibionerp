﻿namespace BibionERP.Application.DTOs.View
{
    public class ViewNoteDto
    {
        public int Id { get; set; }
        public int UserSenderId { get; set; }
        
        public string UserSenderName { get; set; } = string.Empty;

        public int UserReceiverId { get; set; }

        public string UserReceiverName { get; set; } = string.Empty;

        public DateTime Date { get; set; }

        public string Title { get; set; } = string.Empty;

        public string Message { get; set; } = string.Empty;

        public bool IsTask { get; set; } = false;

        public bool IsCompleted { get; set; } = false;

        public ICollection<string>? Tags { get; set; }

    }
}
