﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Application.DTOs.View
{
    public class ViewCompanyServiceDto
    {
        public int CompanyId { get; set; }

        public int ServiceId { get; set; }

        public string ServiceName { get; set; } = string.Empty;

        public DateTime? StartDate { get; set; } = DateTime.Now;

        public DateTime? EndDate { get; set; } = DateTime.Now;

        public int HoursWeek { get; set; }

        public string RepeatDays { get; set; } = string.Empty;
        
        public decimal TotalCost { get; set; } = decimal.Zero;

        public decimal DiscountPercentage { get; set; } = decimal.Zero;

        public decimal FinalPrice { get; set; } = decimal.Zero;

    }
}
