﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.View
{
    public class ViewServiceDto : IResponseDto, IReportingPdf
    {
        public int Id { get; set; }
  
        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public decimal TotalPrice { get; set; } = decimal.Zero;
    }
}
