﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.View
{
    public class ViewHolidayDto: IResponseDto, IReportingPdf
    {
        public int Id { get; set; }

        public string EmployeeName { get; set; } = string.Empty;

        public string HolidayType { get; set; } = string.Empty;

        public DateTime? StartHoliday { get; set; }
        public DateTime? EndHoliday { get; set; }

        public string ApproverName { get; set; } = string.Empty;

        public string HolidayStatus { get; set; } = string.Empty;

    }
}
