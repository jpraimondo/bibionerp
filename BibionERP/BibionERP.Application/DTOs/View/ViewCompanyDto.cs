﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.View
{
    public class ViewCompanyDto: IResponseDto,IReportingPdf
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Document { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

    }
}
