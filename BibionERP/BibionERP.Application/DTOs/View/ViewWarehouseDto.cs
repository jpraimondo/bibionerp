﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.View
{
    public class ViewWarehouseDto : IResponseDto
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

    }
}
