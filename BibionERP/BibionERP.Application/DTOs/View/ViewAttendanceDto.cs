﻿namespace BibionERP.Application.DTOs.View
{
    public class ViewAttendanceDto
    {
        public int AttendanceId { get; set; }

        public string AttendanceName { get; set;} = string.Empty;

        public string AttendanceType { get; set; } = string.Empty;

    }
}
