﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Application.DTOs.View
{
    public class ViewHolidayEmployeeDto
    {

        public int Id { get; set; }

        public string HolidayName { get; set; } = string.Empty;

        public string EmployeeName { get; set; } = string.Empty;

        public string HolidayType { get; set; } = string.Empty;

        public DateTime? HolidayExpiration { get; set; }

        public int HolidayDays { get; set; }

        public bool RequiredDocumentation { get; set; } = false;

        

    }
}
