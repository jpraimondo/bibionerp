﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.View
{
    public class ViewUserDto : IResponseDto, IReportingPdf
    {
        public int Id { get; set; }

        public string UserName { get; set; } = string.Empty;

        public string FullName { get; set; } = string.Empty;

        public string Document { get; set; } = string.Empty;
        
        public string Email { get; set; } = string.Empty;

        public string RolName { get; set; } = string.Empty;

        public DateTime LastLogin { get; set; } = DateTime.MinValue;

        public bool IsActive { get; set; }
    }
}
