﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.View
{
    public class ViewDeliveryNoteDto : IResponseDto
    {
        public int Id { get; set; }

        public string EmployeeName { get; set; } = string.Empty;

        public string CategoryName { get; set; } = string.Empty;

        public DateTime Date { get; set; } = DateTime.Now;

        public string Location { get; set; } = string.Empty;

    }
}
