﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.View
{
    public class ViewMeetingDto : IResponseDto
    {
        public int Id { get; set; }

        public ICollection<ViewAttendanceDto>? Attendances { get; set; } 

        public DateTime? Date { get; set; } = DateTime.Now;
    }
}
