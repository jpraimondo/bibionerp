﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.View
{
    public class ViewItemDto : IResponseDto, IReportingPdf
    {

        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Code { get; set; } = string.Empty;

        public string SupplierName { get; set; } = string.Empty;

        public decimal TradePrice { get; set; } = decimal.Zero;

        public int Quantity { get; set; }


    }
}
