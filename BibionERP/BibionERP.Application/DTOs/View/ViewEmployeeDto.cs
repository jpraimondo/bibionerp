﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.View
{
    public class ViewEmployeeDto : IResponseDto,IReportingPdf
    {
        public int Id { get; set; }

        public string FirstName { get; set; } = string.Empty;

        public string LastName { get; set; } = string.Empty;

        public string Document { get; set; } = string.Empty;

        public string Phone { get; set; } = string.Empty;

        public string FullName { get { return $"{FirstName} {LastName}"; } }

        public string Email { get; set; } = string.Empty;
    }
}
