﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.View
{
    public class ViewSupplierDto : IResponseDto
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string ContactPerson { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

    }
}
