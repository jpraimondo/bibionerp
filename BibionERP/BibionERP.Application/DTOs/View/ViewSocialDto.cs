﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Application.DTOs.View
{
    public class ViewSocialDto
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string URL { get; set; } = string.Empty;

        public string Icons { get; set; } = string.Empty;

        public bool Visible { get; set; } = false;
    }
}
