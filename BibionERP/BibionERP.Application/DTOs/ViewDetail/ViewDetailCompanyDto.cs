﻿using BibionERP.Application.DTOs.Interface;
using BibionERP.Application.DTOs.View;

namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetailCompanyDto : IResponseDto
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public string Document { get; set; } = string.Empty;

        public string Address { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public string Phone { get; set; } = string.Empty;

        public string CustomerName { get; set; } = string.Empty;

        public ICollection<ViewCompanyServiceDto>? CompanyServices { get; set; }
    }
}
