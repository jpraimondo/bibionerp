﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetailCategoryItemDto : IResponseDto
    {
        public int Id { get; set;}

        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;


    }
}
