﻿namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetailCategoryDeliveryNote
    {
        public int Id { get;set; }

        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public bool addItem { get; set; } = false;
    }
}
