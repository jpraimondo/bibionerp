﻿namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetailSettingsDto
    {
        public int Id { get; set; }

        public string CompanyName { get; set; } = string.Empty;

        public string CompanyType { get; set; } = string.Empty;

        public string CompanyDescription { get; set; } = string.Empty;

        public string CompanyPhoneNumber { get; set; } = string.Empty;

        public string CompanyEmail { get; set; } = string.Empty;

        public string CompanyDirection { get; set; } = string.Empty;

        public string CompanyLocation { get; set; } = string.Empty;

        public string CompanyImageLogo { get; set; } = string.Empty;

        public string CompanyImageTitle { get; set; } = string.Empty;

        public string CompanyWebTitle { get; set; } = string.Empty;
    }
}
