﻿namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetailPersonalDto
    {

        public int Id { get; set; }

        public string Nombre { get; set; } = string.Empty;

        public string Apellido { get; set; } = string.Empty;

        public string Localidad { get; set; } = string.Empty;

        public string Direccion { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public string Experiencia { get; set; } = string.Empty;

        public string Masinfo { get; set; } = string.Empty;

        public string Telefono { get; set; } = string.Empty;

        public string Pathfile { get; set; } = string.Empty;


    }
}
