﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetailDeliveryNoteDetailDto : IResponseDto
    {
        public int Id { get; set; }

        public int ItemId { get; set; }

        public string ItemName { get; set; } = string.Empty;

        public int Quantity { get; set; }
    }
}
