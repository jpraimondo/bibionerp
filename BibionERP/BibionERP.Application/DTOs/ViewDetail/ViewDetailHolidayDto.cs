﻿namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetailHolidayDto
    {

        public int Id { get; set; }

        public string EmployeeName { get; set; } = string.Empty;

        public string HolidayType { get; set; } = string.Empty;

        public DateTime? StartHoliday { get; set; }
        public DateTime? EndHoliday { get; set; }

        public int ApproverId { get; set; }
        public string ApproverName { get; set; } = string.Empty;

        public DateTime? ApprovalDate { get; set; }

        public string HolidayStatus { get; set; } = string.Empty;

        public string Details { get; set; } = string.Empty;

    }
}
