﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetailUserDto : IResponseDto
    {

        public int Id { get; set; }

        public string UserName { get; set; } = string.Empty;

        public string FirstName { get; set; } = string.Empty;

        public string LastName { get; set; } = string.Empty;

        public string Document { get; set; } = string.Empty;

        public DateTime? Birthday { get; set; }

        public int EmployeeId { get; set; }

        public string Email { get; set; } = string.Empty;

        public string PhoneNumber { get; set; } = string.Empty;

        public string Address { get; set; } = string.Empty;

        public string Location { get; set; } = string.Empty;

        public string RolName { get; set; } = string.Empty;

        public DateTime LastLogin { get; set; } = DateTime.MinValue;

        public bool IsActive { get; set; }

    }
}
