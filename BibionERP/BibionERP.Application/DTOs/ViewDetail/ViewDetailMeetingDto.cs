﻿using BibionERP.Application.DTOs.Interface;
using BibionERP.Application.DTOs.View;

namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetaiLeaveDto : IResponseDto
    {
        public int Id { get; set; }

        public ICollection<ViewAttendanceDto>? AttendanceList { get; set; }

        public DateTime? Date { get; set; }

        public string Location { get; set; } = string.Empty;

        public string Notes { get; set; } = string.Empty;

        public string Task { get; set; } = string.Empty;
    }
}
