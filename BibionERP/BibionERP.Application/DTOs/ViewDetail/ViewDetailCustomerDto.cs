﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetailCustomerDto : IResponseDto
    {
        public int Id { get; set; }

        public string FirstName { get; set; } = string.Empty;

        public string LastName { get; set; } = string.Empty;

        public string FullName { get { return $"{FirstName} {LastName}"; } }

        public string CustomerType { get; set; } = string.Empty;

        public string Phone { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public string Address { get; set; } = string.Empty;

        public string Notes { get; set; } = string.Empty;
    }
}
