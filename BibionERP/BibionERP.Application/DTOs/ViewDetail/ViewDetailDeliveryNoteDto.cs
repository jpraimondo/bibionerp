﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetailDeliveryNoteDto : IResponseDto
    {
        public int Id { get; set; }

        public string EmployeeName { get; set; } = string.Empty;

        public string CategoryName { get; set; } = string.Empty;

        public DateTime? Date { get; set; }

        public DateOnly? StartDate { get; set; }

        public DateOnly? EndDate { get; set; }

        public string Location { get; set; } = string.Empty;

        public string Note { get; set; } = string.Empty;

        public virtual ICollection<ViewDetailDeliveryNoteDetailDto>? Details { get; set; }

    }
}
