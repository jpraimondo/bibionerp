﻿using BibionERP.Application.DTOs.Interface;

namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetailEmployeeDto : IResponseDto
    {
        public int Id { get; set;}

        public string FirstName { get; set; } = string.Empty;

        public string LastName { get; set; } = string.Empty;

        public string Document { get; set; } = string.Empty;

        public string FullName { get { return $"{FirstName} {LastName}"; } }

        public DateOnly? Birthday { get; set; } = DateOnly.MinValue;

        public string Designature { get; set; } = string.Empty;

        public string Section { get; set; } = string.Empty;

        public DateOnly? JoiningDate { get; set; } = DateOnly.MinValue;

        public DateOnly? LeavingDate { get; set; } = DateOnly.MinValue;

        public string Phone { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public string Address { get; set; } = string.Empty;

        public string Location { get; set; } = string.Empty;
    }
}
