﻿using BibionERP.Application.DTOs.Interface;
using BibionERP.Application.DTOs.View;

namespace BibionERP.Application.DTOs.ViewDetail
{
    public class ViewDetailItemDto : IResponseDto
    { 
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public decimal CostPrice { get; set; } = decimal.Zero;

        public decimal TradePrice { get; set; } = decimal.Zero;

        public string CategoryName { get; set; } = string.Empty;

        public string SupplierName { get; set; } = string.Empty;

        public string Code { get; set; } = string.Empty;

        public List<ViewWarehoueItemDto>? ViewWarehouseItemsDto { get; set; }

        public DateOnly? ManufactureDate { get; set; }

        public DateOnly? ExpirationDate { get; set; }

        public string Note { get; set; } = string.Empty;

    }

    
}
