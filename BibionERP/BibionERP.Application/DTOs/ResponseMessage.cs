﻿using BibionERP.Application.DTOs.Interface;
using BibionERP.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Application.DTOs
{
    public class ResponseMessage
    {
        public int Error { get; set; }
        public string Message { get; set; } = string.Empty;
        public IResponseDto? responseDto { get; set; }

       
    }
}
