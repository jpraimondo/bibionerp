﻿using BibionERP.Application.Interface;
using BibionERP.Application.Services;
using Microsoft.Extensions.DependencyInjection;

namespace BibionERP.Application
{
    public static class ApplicationServicesRegistration
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            
            services.AddScoped<ICategoryDeliveryNoteService, CategoryDeliveryNoteService>();
            services.AddScoped<ICompanyService,CompanyService>();
            services.AddScoped<ICustomerService,CustomerService>();
            services.AddScoped<ICategoryItemService, CategoryItemService>();
            services.AddScoped<IDeliveryNoteService,DeliveryNoteService>();
            services.AddScoped<IEmployeeService,EmployeeService>();
            services.AddScoped<IItemService,ItemService>();
            services.AddScoped<IServiceService,ServiceService>();
            services.AddScoped<ISupplierService,SupplierService>();
            services.AddScoped<IWarehouseService,WarehouseService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRolService, RolService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IMeetingService,MeetingService>();
            services.AddScoped<ISettingsService, SettingsService>();
            services.AddScoped<IHolidayService, HolidayService>();
            services.AddScoped<IHolidayEmployeeService, HolidayEmployeeService>();
            services.AddScoped<INoteService, NoteService>();
            services.AddSingleton<IPersonalService, PersonalService>();
            


            return services;
        }

    }
}
