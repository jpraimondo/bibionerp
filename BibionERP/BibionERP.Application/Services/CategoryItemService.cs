﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;



namespace BibionERP.Application.Services
{

    /// <summary>
    /// Servicio que maneja la categoria de articulos
    /// </summary>
    public class CategoryItemService : ICategoryItemService
    {

        private readonly IUnitOfWork _unitOfWork;
        private ILogger<CategoryItemService> _logger;

        public CategoryItemService(IUnitOfWork unitOfWork,
            ILogger<CategoryItemService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        /// <summary>
        /// Funcion que permite eliminar una categoria de articulo por un Id dado
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna un bool con el resultado de la consulta</returns>
        public async Task<bool> Delete(int id,string userName = "Not User")
        {

            try
            {

            CategoryItem category = await _unitOfWork.CategoryItemRepository.GetById(id);
            
                if(category is not null) { 
           
                    category.DeleteBy = userName;

                    await _unitOfWork.CategoryItemRepository.Delete(category);
                    await _unitOfWork.Save();

                    _logger.LogInformation($"Se elimino la categoria Id {id} {category.Name}");

                    return true;
                }

                _logger.LogInformation($"no se encontro la categoria Id {id}");
                return false;
            }
            catch (Exception ex)
            {

                _logger.LogError($"Error al eliminar la categoria {ex.Message}");
                throw;
            }

            

            
        }

        /// <summary>
        /// Funcion para buscar categoria por expression
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Retorna IEnumerable<ViewCategoryItemDto></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<IEnumerable<ViewCategoryItemDto>> Find(Expression<Func<CategoryItem, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Funcion que devuelve todas las categorias de items
        /// </summary>
        /// <returns>Retorna IEnumerable<ViewCategoryItemDto></returns>
        public async Task<IEnumerable<ViewCategoryItemDto>> GetAll()
        {
            try
            {

                List<CategoryItem> listCategoryItem = (await _unitOfWork.CategoryItemRepository.GetAll()).ToList();

                List<ViewCategoryItemDto> listCategoryItemDto = new List<ViewCategoryItemDto>();

                foreach (var category in listCategoryItem)
                {
                    listCategoryItemDto.Add(CategoryItemMapper.ToViewCategoryItemDto(category));
                }

                _logger.LogInformation($"Se listaron las categorias cantidad {listCategoryItemDto.Count}");
                return listCategoryItemDto;

            }
            catch (Exception ex)
            {
                _logger.LogError($"Error al listar las categoria {ex.Message}");
                return new List<ViewCategoryItemDto>();
            }
        }

        /// <summary>
        /// Devuelve una categoria por un Id dado
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorna un ViewDetailCategoryItemDto</returns>

        public async Task<ViewDetailCategoryItemDto> GetById(int id)
        {
            try
            {
                CategoryItem category = await _unitOfWork.CategoryItemRepository.GetById(id);

                _logger.LogInformation($"Se busco la categoria con el Id {id} {category.Name}");

                return CategoryItemMapper.ToViewDetailCategoryItemDto(category);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Error al buscar categoria Id {id} {ex.Message}");
                return new ViewDetailCategoryItemDto();
            }
        }


        /// <summary>
        /// Funcion que inserta una nueva categoria para los items
        /// </summary>
        /// <param name="categoryItemDto"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna una ViewCategoryItemDto </returns>

        public async Task<ViewCategoryItemDto> Insert(CreateCategoryItemDto categoryItemDto, string userName = "Not User")
        {
            try
            {

                CategoryItem category = CategoryItemMapper.ToCategoryItem(categoryItemDto);

                category.CreateBy = userName;

                await _unitOfWork.CategoryItemRepository.Insert(category);

                await _unitOfWork.Save();

                return CategoryItemMapper.ToViewCategoryItemDto(category);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Error al insertar una nueva categoria item {ex.Message}");
                return new ViewCategoryItemDto();
                
            }
        }

        /// <summary>
        /// Funcion que actualiza una categoria para los items
        /// </summary>
        /// <param name="categoryItemDto"></param>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna un ViewCategoryItemDto</returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ViewCategoryItemDto> Update(CreateCategoryItemDto categoryItemDto, int id, string userName = "Not User")
        {
            try
            {

                CategoryItem category = CategoryItemMapper.ToCategoryItem(categoryItemDto);

                CategoryItem categoryEdit = await _unitOfWork.CategoryItemRepository.GetById(id);

                categoryEdit.Name = category.Name;
                categoryEdit.Description = category.Description;
                categoryEdit.EditBy = userName;

                await _unitOfWork.CategoryItemRepository.Update(categoryEdit);

                await _unitOfWork.Save();

                _logger.LogInformation($"Se actualizo la categoria item {categoryEdit.Id} {categoryEdit.Name}");

                return CategoryItemMapper.ToViewCategoryItemDto(category);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Error al editar una nueva categoria item {ex.Message}");
                return new ViewCategoryItemDto();

            }
            throw new NotImplementedException();
        }
    }
}
