﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Application.Services
{
    public class SocialService : ISocialService
    {

        private readonly IUnitOfWork _unitOfWork;
        private ILogger<SocialService> _logger;

        public SocialService(IUnitOfWork unitOfWork, ILogger<SocialService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<bool> Delete(int id, string userName = "Not User")
        {
            try
            {
                Social social = await _unitOfWork.SocialRepository.GetById(id);

                if (social is not null)
                {
                    if (social.IsDelete.Equals(false))
                    {

                        social.DeleteBy = userName;

                        await _unitOfWork.SocialRepository.Delete(social);
                        await _unitOfWork.Save();
                        
                        _logger.LogInformation($"Se elimina la red social {social.Id} {social.Name}");

                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<IEnumerable<ViewSocialDto>> GetAll()
        {
            try
            {

                List<Social> listSocial = (await _unitOfWork.SocialRepository.GetAll()).ToList();

                List<ViewSocialDto> listSocialDto = new List<ViewSocialDto>();

                foreach (var social in listSocial)
                {
                    listSocialDto.Add(SocialMapper.ToViewSocialDto(social));
                }

                return listSocialDto;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<IEnumerable<ViewSocialDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public async Task<ViewSocialDto> GetById(int id)
        {
            try
            {
                var social = await _unitOfWork.SocialRepository.GetById(id);

                return SocialMapper.ToViewSocialDto(social);
            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<ViewSocialDto> Insert(CreateSocialDto socialDto, string userName = "Not User")
        {
            try
            {
                Social social = SocialMapper.ToSocial(socialDto);

                social.CreateBy = userName;

                await _unitOfWork.SocialRepository.Insert(social);

                await _unitOfWork.Save();

                return SocialMapper.ToViewSocialDto(social);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<ViewSocialDto> Update(CreateSocialDto socialDto, int id, string userName = "Not User")
        {
            try
            {
                Social social = await _unitOfWork.SocialRepository.GetById(id);

                social.Name = socialDto.Name;
                social.URL = socialDto.URL;
                social.Icons = socialDto.Icons;
                social.Visible = socialDto.Visible;

                social.EditBy = userName;

                await _unitOfWork.SocialRepository.Update(social);

                await _unitOfWork.Save();

                return SocialMapper.ToViewSocialDto(social);
            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message);
                throw;
            }
        }
   
    
    }
}
