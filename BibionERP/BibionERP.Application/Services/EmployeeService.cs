﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;

namespace BibionERP.Application.Services
{

    public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private ILogger<EmployeeService> _logger;

        public EmployeeService(IUnitOfWork unitOfWork, ILogger<EmployeeService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<bool> Delete(int id, string userName = "Not User")
        {
            try
            {

            Employee employee = await _unitOfWork.EmployeeRepository.GetById(id);

            if (employee is not null)
            {
                if (employee.IsDelete.Equals(false))
                {

                    employee.DeleteBy = userName;

                    await _unitOfWork.EmployeeRepository.Delete(employee);
                    await _unitOfWork.Save();

                    
                    return true;
                }
            }
            
            return false;
            }catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public Task<IEnumerable<ViewEmployeeDto>> Find(Expression<Func<Employee, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewEmployeeDto>> GetAll()
        {
            try
            {

                var listEmployee = (await _unitOfWork.EmployeeRepository.GetAll()).ToList();
                                                                                  

                List<ViewEmployeeDto> listEmployeeDto = new List<ViewEmployeeDto>();

                foreach (var employee in listEmployee)
                {
                    listEmployeeDto.Add(EmployeeMapper.ToViewEmployeeDto(employee));
                }
                
                return listEmployeeDto;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public Task<IEnumerable<ViewEmployeeDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public async Task<ViewDetailEmployeeDto> GetById(int id)
        {
            try
            {
                Employee employee = await _unitOfWork.EmployeeRepository.GetById(id);

                
                return EmployeeMapper.ToViewDetailEmployeeDto(employee);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new Exception (ex.Message);
            }
        }

        public async Task<ViewDetailEmployeeDto> Insert(CreateEmployeeDto employeeDto, string userName = "Not User")
        {
            try
            {

                Employee employee = EmployeeMapper.ToEmployee(employeeDto);

                employee.JoiningDate = DateTime.Now;

                employee.CreateBy=userName;

                await _unitOfWork.EmployeeRepository.Insert(employee);

                await _unitOfWork.Save();

                
                return EmployeeMapper.ToViewDetailEmployeeDto(employee);

            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<ViewDetailEmployeeDto> Update(CreateEmployeeDto employeeDto, int id, string userName = "Not User")
        {
            try
            {

                Employee employee = await _unitOfWork.EmployeeRepository.GetById(id);

                employee.FirstName = employeeDto.FirstName;
                employee.LastName = employeeDto.LastName;
                employee.Email = employeeDto.Email;
                employee.Phone = employeeDto.Phone;
                employee.Location = employeeDto.Location;
                employee.Address = employeeDto.Address;
                employee.Document = employeeDto.Document;
                employee.Birthday = employeeDto.Birthday.Value.ToDateTime(TimeOnly.MinValue);
                employee.JoiningDate = employeeDto.JoiningDate.Value.ToDateTime(TimeOnly.MinValue);
                employee.LeavingDate = employeeDto.LeavingDate.Value.ToDateTime(TimeOnly.MinValue);

                employee.EditBy = userName;

                await _unitOfWork.EmployeeRepository.Update(employee);

                await _unitOfWork.Save();

                
                return EmployeeMapper.ToViewDetailEmployeeDto(employee);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new Exception(ex.Message);
            }
        }
    }
}
