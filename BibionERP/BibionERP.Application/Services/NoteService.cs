﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;

namespace BibionERP.Application.Services
{
    internal class NoteService : INoteService
    {

        private readonly IUnitOfWork _unitOfWork;
        private ILogger<NoteService> _logger;

        /// <summary>
        /// Servicio que maneja las notas creadas por usuarios. 
        /// </summary>
        public NoteService(IUnitOfWork unitOfWork, ILogger<NoteService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        /// <summary>
        /// Funcion que elimina una nota por un Id dado
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna un bool con el resultado de la operación</returns>
        public async Task<bool> Delete(int id, string userName = "Not User")
        {
            try
            {
                Note note = await _unitOfWork.NoteRepository.GetById(id);

                if (note is not null)
                {
                    if (note.IsDelete.Equals(false))
                    {

                        note.DeleteBy = userName;

                        await _unitOfWork.NoteRepository.Delete(note);
                        await _unitOfWork.Save();

                        _logger.LogInformation(message: $"Se elimino la nota Id {id} {note.Title} | {userName}");
                        return true;
                    }

                }
                _logger.LogInformation(message: $"No se encontro la nota Id {id}");
                return false;
            }
            catch (Exception ex)
            {

                _logger.LogError(message: $"Error al eliminar la nota Id {id} {ex.Message}");
                return false;
            }
        }

        /// <summary>
        /// Función que devuelve un IEnumerable de ViewNoteDto por una consulta dada
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Devuelve un IEnumerable<ViewNoteDto></returns>
        public async Task<IEnumerable<ViewNoteDto>> Find(Expression<Func<Note, bool>> predicate)
        {
            try
            {

                List<Note> listNotes = (await _unitOfWork.NoteRepository.Find(predicate,null,"UserSender,UserReceiver",true)).ToList();

                List<ViewNoteDto> noteDtoList = new List<ViewNoteDto>();

                foreach (var note in listNotes)
                {
                    noteDtoList.Add(NoteMapper.ToViewNoteDto(note));
                }

                _logger.LogInformation(message: $"Se genero una lista de notas para un UserReceiver");

                return noteDtoList;

            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al listar las notas {ex.Message}");
                return new List<ViewNoteDto>();
            }
        }

        /// <summary>
        /// Devuelve toda la lista de notas registradas con isDelete = false
        /// </summary>
        /// <returns>Devuelve un IEnumerable<ViewNoteDto>returns>
        public async Task<IEnumerable<ViewNoteDto>> GetAll()
        {
            try
            {

                List<Note> listNotes = (await _unitOfWork.NoteRepository.GetAll("UserSender,UserReceiver")).ToList();

                List<ViewNoteDto> noteDtoList = new List<ViewNoteDto>();

                foreach (var note in listNotes)
                {
                    noteDtoList.Add(NoteMapper.ToViewNoteDto(note));
                }

                _logger.LogInformation(message: $"Se genero una lista de notas registros {noteDtoList.Count}");
                return noteDtoList;

            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al listar las notas {ex.Message}");
                return new List<ViewNoteDto>();
            }
        }

        /// <summary>
        /// Devuelve toda la lista de notas registradas, incluidos los isDelete = true
        /// </summary>
        /// <returns>Devuelve un IEnumerable<ViewNoteDto>returns>
        public Task<IEnumerable<ViewNoteDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Devuelve un ViewNoteDto por un id dado
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ViewNoteDto</returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ViewNoteDto> GetById(int id)
        {
            try
            {

                Note note = await _unitOfWork.NoteRepository.GetById(id, "UserSender,UserReceiver");


                _logger.LogInformation(message: $"Se obtiene la nota Id {id}");
                return NoteMapper.ToViewNoteDto(note);

            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al obtener la nota id {id} {ex.Message}");
                return new ViewNoteDto();
            }
        }

        /// <summary>
        /// Inserta una nueva nota y devuelve un ViewNoteDto 
        /// </summary>
        /// <param name="noteDto"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task<ViewNoteDto> Insert(CreateNoteDto noteDto, string userName = "Not User")
        {
            try
            {
                Note note = NoteMapper.ToNote(noteDto);

                note.UserReceiver = await _unitOfWork.UsersRepository.GetById(noteDto.UserReceiverId);

                note.UserSender = await _unitOfWork.UsersRepository.GetById(noteDto.UserSenderId);

                await _unitOfWork.NoteRepository.Insert(note);

                await _unitOfWork.Save();

                _logger.LogInformation(message: $"Se inserta la nota Id {note.Id}");
                return NoteMapper.ToViewNoteDto(note);

            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al insertar la nota {ex.Message}");
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Actualiza una nota ya existente y devuelve un ViewNoteDto 
        /// </summary>
        /// <param name="noteDto"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
 
        public async Task<ViewNoteDto> Update(CreateNoteDto noteDto, int id, string userName = "Not User")
        {
            try
            {
                Note note = NoteMapper.ToNote(noteDto);

                Note noteEdit = await _unitOfWork.NoteRepository.GetById(id, "UserReceiver,UserSender");

                if(!noteEdit.UserReceiverId.Equals(noteDto.UserReceiverId))
                {
                    noteEdit.UserReceiver = await _unitOfWork.UsersRepository.GetById(noteDto.UserReceiverId);
                }

                noteEdit.Title = note.Title;
                noteEdit.Message = note.Message;
                noteEdit.Tags = note.Tags;
                noteEdit.IsTask = note.IsTask;
                noteEdit.IsCompleted = note.IsCompleted;

                await _unitOfWork.NoteRepository.Update(noteEdit);

                await _unitOfWork.Save();

                _logger.LogInformation(message: $"Se actualiza la nota Id {note.Id}");
                return NoteMapper.ToViewNoteDto(noteEdit);

            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al insertar la nota {ex.Message}");
                throw new Exception(ex.Message);
            }
        }
    }
}
