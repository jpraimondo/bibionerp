﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Services
{
    public class HolidayService : IHolidayService
    {

        private readonly IUnitOfWork _unitOfWork;

        public HolidayService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public Task<bool> Delete(int id, string userName = "Not User")
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ViewHolidayDto>> Find(Expression<Func<Holiday, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewHolidayDto>> GetAll()
        {
            try
            {

                List<Holiday> listHoliday = (await _unitOfWork.HolidayRepository.GetAll("Employee,Approver")).ToList();

                List<ViewHolidayDto> listHolidayDto = new List<ViewHolidayDto>();

                foreach (var item in listHoliday)
                {
                    listHolidayDto.Add(HolidayMapper.ToViewHolidayDto(item));
                }

                return listHolidayDto;

            }
            catch 
            {
                throw;
            }
        }

        public Task<IEnumerable<ViewHolidayDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public async Task<ViewDetailHolidayDto> GetById(int id)
        {
            try
            {
                Holiday holiday = await _unitOfWork.HolidayRepository.GetById(id);

                holiday.Approver = await _unitOfWork.UsersRepository.GetById(holiday.ApproverId);

                holiday.Employee = await _unitOfWork.EmployeeRepository.GetById(holiday.EmployeeId);

                return HolidayMapper.ToViewDetailHolidayDto(holiday);
            }
            catch
            {
                throw;
            }
        }

        public async Task<ViewDetailHolidayDto> Insert(CreateHolidayDto holidayDto, string userName = "Not User")
        {
            try
            {

                Holiday holiday = HolidayMapper.ToHoliday(holidayDto);

                holiday.Employee = await _unitOfWork.EmployeeRepository.GetById(holidayDto.EmployeeId);
                holiday.Approver = await _unitOfWork.UsersRepository.GetById(holidayDto.ApproverId);

                if (holiday.StartHoliday > holiday.EndHoliday)
                    throw new Exception("La fecha finaL no puede ser menor a la fecha de inicio.");

                int diffDays = ((holiday.EndHoliday.Value - holiday.StartHoliday.Value).Days)+1;


                var holidayAvailable = (await _unitOfWork.HolidayEmployeeRepository.Find(x => x.EmployeeId.Equals(holidayDto.EmployeeId) &&
                                                                                              x.HolidayType.Equals(holiday.HolidayType)  &&
                                                                                              x.HolidayDays >= diffDays &&
                                                                                             x.IsDelete.Equals(false))).FirstOrDefault();

                if (holidayAvailable == null)
                    throw new Exception("El funcionario no cuenta con licencia disponible.");
                
                holidayAvailable.HolidayDays = holidayAvailable.HolidayDays - diffDays;

                holidayAvailable.EditBy = userName;

                holiday.CreateBy = userName;

                await  _unitOfWork.HolidayRepository.Insert(holiday);

                await _unitOfWork.HolidayEmployeeRepository.Update(holidayAvailable);

                await _unitOfWork.Save();

                return HolidayMapper.ToViewDetailHolidayDto(holiday);

            }
            catch
            {
                throw;
            }
        }

        public Task<ViewDetailHolidayDto> Update(CreateHolidayDto laveDto, int id, string userName = "Not User")
        {
            throw new NotImplementedException();
        }
    }
}
