﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Exceptions;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;


namespace BibionERP.Application.Services
{
    /// <summary>
    /// Servicio que maneja las Notas de Entrega
    /// </summary>
    public class DeliveryNoteService : IDeliveryNoteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private ILogger<DeliveryNoteService> _logger;

        public DeliveryNoteService(IUnitOfWork unitOfWork,ILogger<DeliveryNoteService>logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }
        /// <summary>
        /// Función que elimina una boleta de entrega de productos
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna un bool con el resultado de la función</returns>
        public async Task<bool> Delete(int id, string userName = "Not User")
        {
            try
            {

            DeliveryNote deliveryNote = (await _unitOfWork.DeliveryNoteRepository.Find(x => x.Id.Equals(id),null, "Warehouse,CategoryDeliveryNote",false)).FirstOrDefault();

            if (deliveryNote is not null)
            {
                if (deliveryNote.IsDelete.Equals(false))
                {

                    deliveryNote.DeleteBy = userName;

                    await _unitOfWork.DeliveryNoteRepository.Delete(deliveryNote);
                    
                    List<DeliveryNoteDetail> deliveryNoteDetails = (await _unitOfWork.DeliveryNoteDetailRepository.Find(d => d.DeliveryNoteId.Equals(id), null, "Item")).ToList();

                    await UpdateStock(deliveryNoteDetails, !deliveryNote.CategoryDeliveryNote.AddItem , deliveryNote.WarehouseId);

                    await _unitOfWork.Save();

                    _logger.LogInformation(message: $"Se elimino la boleta Id {id} | {userName}");
                    return true;
                }
            }
            _logger.LogInformation(message: $"Error al eliminar la boleta Id {id}");
            return false;
            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al eliminar la boleta de entrega, {ex.Message}", ex);
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<IEnumerable<ViewDeliveryNoteDto>> Find(Expression<Func<DeliveryNote, bool>> predicate)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Función que devuelve una lista de Boletas de entrega
        /// </summary>
        /// <returns>Retorna una IEnumerable<ViewDeliveryNoteDto></returns>
        public async Task<IEnumerable<ViewDeliveryNoteDto>> GetAll()
        {
            try
            {
                List<ViewDeliveryNoteDto> deliveryNoteDto = new();

                List<DeliveryNote> deliveryNote =  (await _unitOfWork.DeliveryNoteRepository.GetAll("Employee,CategoryDeliveryNote")).ToList();

                foreach(var note in deliveryNote)
                {
                    deliveryNoteDto.Add(DeliveryNoteMapper.ToViewDeliveryNote(note));
                }

                _logger.LogInformation($"Se listaron las boletas cantidad {deliveryNoteDto.Count}");
                return deliveryNoteDto;
            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al listar las boletas {ex.Message}");
                return new List<ViewDeliveryNoteDto>();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<IEnumerable<ViewDeliveryNoteDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Función que devuelve una boleta por un Id dado
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorna un ViewDetailDeliveryNoteDto</returns>
        public async Task<ViewDetailDeliveryNoteDto> GetById(int id)
        {
            try
            {
                var deliveryNote = await _unitOfWork.DeliveryNoteRepository.GetById(id);

                deliveryNote.Employee = await _unitOfWork.EmployeeRepository.GetById(deliveryNote.EmployeeId);

                deliveryNote.CategoryDeliveryNote = await _unitOfWork.CategoryDeliveryNoteRepository.GetById(deliveryNote.CategoryDeliveryNoteId);

                var deliveryNoteDetails = await _unitOfWork.DeliveryNoteDetailRepository.Find(d => d.DeliveryNoteId.Equals(id),null,"Item");



                List<ViewDetailDeliveryNoteDetailDto> viewDetailDeliveryNoteDetailDtos= new();

                foreach(var item in deliveryNoteDetails)
                {
                    viewDetailDeliveryNoteDetailDtos.Add(DeliveryNoteDetailMapper.ToViewDeliveryNoteDetail(item));
                }

                
                var deliveryNoteDto = DeliveryNoteMapper.ToViewDetailDeliveryNote(deliveryNote);

                deliveryNoteDto.Details = viewDetailDeliveryNoteDetailDtos;



                return deliveryNoteDto;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Función que inserta una boleta nueva
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorna un ViewDetailDeliveryNoteDto</returns>
        public async Task<ViewDetailDeliveryNoteDto> Insert(CreateDeliveryNoteDto deliverNoteDto, List<CreateDeliveryNoteDetailDto> deliveryNoteDetailsDto, string userName = "Not User")
        {
            try
            {
                var deliveryNote = DeliveryNoteMapper.ToDeliveryNote(deliverNoteDto);

                var employee = await _unitOfWork.EmployeeRepository.GetById(deliveryNote.EmployeeId);

                deliveryNote.Employee = employee;

                CategoryDeliveryNote category = await _unitOfWork.CategoryDeliveryNoteRepository.GetById(deliverNoteDto.CategoryId);

                deliveryNote.CategoryDeliveryNote = category;

                //Warehouse warehouse = await _unitOfWork.WarehouseRepository.

                List<DeliveryNoteDetail> details = new List<DeliveryNoteDetail>();

                foreach (var itemDto in deliveryNoteDetailsDto)
                {
                    var item = DeliveryNoteDetailMapper.ToDeliveryNote(itemDto);
                    item.Item = await _unitOfWork.ItemRepository.GetById(itemDto.ItemId);
                    details.Add(item);
                }
                deliveryNote.Details = details;

                await UpdateStock(deliveryNote.Details, deliveryNote.CategoryDeliveryNote.AddItem, deliverNoteDto.WarehouseId);

                await _unitOfWork.DeliveryNoteRepository.Insert(deliveryNote);

                await _unitOfWork.Save();

                _logger.LogInformation(message: $"Se creo la boleta Id {deliveryNote.Id}", deliveryNote);

                return DeliveryNoteMapper.ToViewDetailDeliveryNote(deliveryNote);

            }
            catch (ItemException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al crear la boleta de pedido {ex.Message}");
                return new ViewDetailDeliveryNoteDto();
            }
            
        }
      
        /// <summary>
        /// Función que actualiza una boleta por un Id dado
        /// </summary>
        /// <param name="deliverNoteDto"></param>
        /// <param name="id"></param>
        /// <param name="deliveryNoteDetailsDto"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<ViewDetailDeliveryNoteDto> Update(CreateDeliveryNoteDto deliverNoteDto, int id, List<CreateDeliveryNoteDetailDto> deliveryNoteDetailsDto, string userName = "Not User")
        {
            throw new NotImplementedException();
        }

        private async Task UpdateStock(ICollection<DeliveryNoteDetail> deliveryNoteDetails, bool addItems, int warehouseId)
        {
 
            var warehouse = (await _unitOfWork.WarehouseRepository.Find(w => w.Id.Equals(warehouseId),null, "WarehouseItem.Item",false)).FirstOrDefault();
                        
            if (warehouse is not null || warehouse.WarehouseItem is not null )
            {

                List<WarehouseItem> warehouseItem = warehouse.WarehouseItem.ToList();

                if (!addItems)
                {
                    foreach (var detail in deliveryNoteDetails)
                    {
                        var itemEdit = warehouseItem.Where(wi => wi.ItemId.Equals(detail.ItemId)).FirstOrDefault();

                        if (itemEdit is null || itemEdit?.Quantity < detail.Quantity)
                        {
                            throw new ItemException($"Hay items sin stock disponible en el Almacén: Item Id {detail.ItemId}", _logger);
                        }
                    }
                }

                foreach (DeliveryNoteDetail deliveryNoteDetail in deliveryNoteDetails)
                {

                        var itemEdit = warehouseItem.Where(wi => wi.ItemId.Equals(deliveryNoteDetail.ItemId)).FirstOrDefault();

                        if (itemEdit is null)
                        {
                            if (addItems)
                            {
                                var item = await _unitOfWork.ItemRepository.GetById(deliveryNoteDetail.ItemId);

                                warehouseItem.Add(new WarehouseItem()
                                {
                                    WarehouseId = warehouseId,
                                    Warehouse = warehouse,
                                    ItemId = deliveryNoteDetail.ItemId,
                                    Item = item,
                                    Quantity = deliveryNoteDetail.Quantity,
                                });
                            }
                            else
                            {
                                throw new ItemException("Hay items no disponibles en el Almacén", _logger);
                            }


                        }
                        else
                        {

                            itemEdit.Quantity = addItems ? itemEdit.Quantity + deliveryNoteDetail.Quantity : itemEdit.Quantity - deliveryNoteDetail.Quantity;
                        }

                }

                warehouse.WarehouseItem = warehouseItem;

                await _unitOfWork.WarehouseRepository.Update(warehouse);
     
                return;

            }
        }
        
    }
}
