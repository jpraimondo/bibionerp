﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using System.Linq.Expressions;

namespace BibionERP.Application.Services
{
    public class RolService : IRolService
    {
        private readonly IUnitOfWork _unitOfWork;

        public RolService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public Task<IEnumerable<ViewDetailRolDto>> Find(Expression<Func<ApplicationRol, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewDetailRolDto>> GetAll()
        {
            try
            {
                var rols = (await _unitOfWork.RolesRepository.GetAll()).ToList();


                List<ViewDetailRolDto> rolsDto = new List<ViewDetailRolDto>();

                foreach(var rol in rols)
                {
                    rolsDto.Add( 
                        new ViewDetailRolDto(){
                            Id = rol.Id,
                            Name = rol.Name,
                            NormalizedName = rol.NormalizedName,
                            Description = rol.Description
                        });
                }


                return rolsDto;


            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public Task<IEnumerable<ViewDetailRolDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public Task<ViewDetailRolDto> GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Task<ViewDetailRolDto> Insert(CreateRolDto rolDto, string userName = "Not User")
        {
            throw new NotImplementedException();
        }

        public Task<ViewDetailRolDto> Update(CreateRolDto rolDto, int id, string userName = "Not User")
        {
            throw new NotImplementedException();
        }
    }
}
