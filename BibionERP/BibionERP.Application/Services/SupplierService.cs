﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;

namespace BibionERP.Application.Services
{
    public class SupplierService : ISupplierService
    {

        private readonly IUnitOfWork _unitOfWork;
        private ILogger<SupplierService> _logger;

        public SupplierService(IUnitOfWork unitOfWork, ILogger<SupplierService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }


        public Task<bool> Delete(int id, string userName = "Not User")
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ViewSupplierDto>> Find(Expression<Func<Supplier, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewSupplierDto>> GetAll()
        {
            try
            {

                List<Supplier> listSupplier = (await _unitOfWork.SupplierRepository.GetAll()).ToList();

                List<ViewSupplierDto> listSupplierDto = new List<ViewSupplierDto>();

                foreach (var supplier in listSupplier)
                {
                    listSupplierDto.Add(SupplierMapper.ToViewSupplierDto(supplier));
                }

                return listSupplierDto;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<IEnumerable<ViewSupplierDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public async Task<ViewDetailSupplierDto> GetById(int id)
        {
            try
            {
                var supplier = await _unitOfWork.SupplierRepository.GetById(id);

                return SupplierMapper.ToViewDetailSupplierDto(supplier); 
            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<ViewDetailSupplierDto> Insert(CreateSupplierDto supplierDto, string userName = "Not User")
        {
            try
            {
                Supplier supplier = SupplierMapper.ToSupplier(supplierDto);

                supplier.CreateBy = userName;

                await _unitOfWork.SupplierRepository.Insert(supplier);

                await _unitOfWork.Save();

                return SupplierMapper.ToViewDetailSupplierDto(supplier);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<ViewDetailSupplierDto> Update(CreateSupplierDto supplierDto, int id, string userName = "Not User")
        {
            try
            {
                var supplier = await _unitOfWork.SupplierRepository.GetById(id);

                supplier.Name = supplierDto.Name;
                supplier.Email = supplierDto.Email;
                supplier.Document = supplierDto.Document;
                supplier.ContactPerson = supplierDto.ContactPerson;
                supplier.Phone = supplierDto.Phone;
                supplier.Address = supplierDto.Address;
                supplier.Location = supplierDto.Location;

                supplier.EditBy= userName;
                
                await _unitOfWork.SupplierRepository.Update(supplier);
                await _unitOfWork.Save();

                return SupplierMapper.ToViewDetailSupplierDto(supplier);
            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message);
                throw;
            }
        }
    }
}
