﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;


namespace BibionERP.Application.Services
{
    /// <summary>
    /// Servicio que maneja a los clientes de la empresa
    /// </summary>
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private ILogger<CustomerService> _logger;

        public CustomerService(IUnitOfWork unitOfWork,ILogger<CustomerService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        /// <summary>
        /// Funcion que elimina un cliente de la aplicacion por un Id dado
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna un bool con el resultado de la funcion</returns>
        public async Task<bool> Delete(int id, string userName = "Not User")
        {
            try
            {

            Customer customer = await _unitOfWork.CustomerRepository.GetById(id);

            if (customer is not null)
            {
                if (customer.IsDelete.Equals(false))
                {

                    customer.DeleteBy = userName;

                    await _unitOfWork.CustomerRepository.Delete(customer);
                        await _unitOfWork.Save();

                    _logger.LogInformation(message: $"Se elimino al cliente Id {id} {customer.FirstName} {customer.LastName} | {userName}", customer);
                    return true;
                }
            }

                _logger.LogInformation(message: $"No se encontro al cliente apra borrar Id {id}");
                return false;


            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Ocurrio un error al eliminar al cliente Id {id} {ex.Message}",ex);
                return false;
            }
        }

        /// <summary>
        /// Función que busca una lista de clientes por una expression dada
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Retorna IEnumerable<ViewCustomerDto></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<IEnumerable<ViewCustomerDto>> Find(Expression<Func<Customer, bool>> predicate)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Función que retorna una lista de clientes no borrados
        /// </summary>
        /// <returns>Retorna un IEnumerable<ViewCustomerDto></returns>
        public async Task<IEnumerable<ViewCustomerDto>> GetAll()
        {
            try
            {

                List<Customer> listCustomers = (await _unitOfWork.CustomerRepository.GetAll()).ToList();

                List<ViewCustomerDto> listCustomersDto = new List<ViewCustomerDto>();

                foreach (var customer in listCustomers)
                {
                    listCustomersDto.Add(CustomerMapper.ToViewCustomerDto(customer));
                }

                _logger.LogInformation(message: $"Se listan los clientes cantidad {listCustomersDto.Count}",listCustomersDto);
                return listCustomersDto;

            }
            catch (Exception ex)
            {
                _logger.LogError($"Error al listar los clientes {ex.Message}", ex);
                return new List<ViewCustomerDto>();
            }
        }
        /// <summary>
        /// Función que retorna una lista de clientes inclusive los borrados
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<IEnumerable<ViewCustomerDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Función que retorna un cliente por un Id dado
        /// </summary>
        /// <param name="id"></param>
        /// <returns>retorna un ViewDetailCustomerDto</returns>
        public async Task<ViewDetailCustomerDto> GetById(int id)
        {
            try
            {
                Customer customer = await _unitOfWork.CustomerRepository.GetById(id);


                _logger.LogInformation(message: $"Se busca el cliente Id {id} {customer.FirstName} {customer.LastName}", customer);
                return CustomerMapper.ToViewDetailCustomerDto(customer);
            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al buscar el cliente {id} {ex.Message}", ex);
                return new ViewDetailCustomerDto();
            }
        }

        /// <summary>
        /// Funcion que crea un nuevo cliente
        /// </summary>
        /// <param name="customerDto"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna una nueva ViewDetailCustomerDto</returns>
        public async Task<ViewDetailCustomerDto> Insert(CreateCustomerDto customerDto, string userName = "Not User")
        {
            try
            {

                Customer customer = CustomerMapper.ToCustomer(customerDto);

                customer.CreateBy=userName;

                await _unitOfWork.CustomerRepository.Insert(customer);

                await _unitOfWork.Save();

                _logger.LogInformation(message: $"Se agrega el cliente Id {customer.Id} {customer.FirstName} {customer.LastName}", customer);

                return CustomerMapper.ToViewDetailCustomerDto(customer);

            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al agregar el Cliente {ex.Message}");
                return new ViewDetailCustomerDto();
            }
        }

        /// <summary>
        /// Función que edita al cliente ya existente
        /// </summary>
        /// <param name="customerDto"></param>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna una nueva ViewDetailCustomerDto</returns>
        public async Task<ViewDetailCustomerDto> Update(CreateCustomerDto customerDto, int id, string userName = "Not User")
        {
            try
            {
                Customer customer = await _unitOfWork.CustomerRepository.GetById(id);

                customer.FirstName= customerDto.FirstName;
                customer.LastName= customerDto.LastName;
                customer.Email= customerDto.Email;
                customer.Address = customerDto.Address; 
                customer.Phone = customerDto.Phone;
                customer.Notes = customerDto.Notes;
                customer.EditBy=userName;
                               
                await _unitOfWork.CustomerRepository.Update(customer);

                await _unitOfWork.Save();

                _logger.LogInformation(message: $"Se edito el cliente Id {customer.Id} {customer.FirstName} {customer.LastName}", customer);
                return CustomerMapper.ToViewDetailCustomerDto(customer);
            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al editar el Cliente {ex.Message}");
                return new ViewDetailCustomerDto();
            }
        }
    }
}
