﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Services
{
    public class SettingsService : ISettingsService
    {

        private ViewDetailSettingsDto _viewDetailSettingsDto;

        private DateTime _lastChange = DateTime.MinValue;

        private readonly IUnitOfWork _unitOfWork;

        public SettingsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ViewDetailSettingsDto> GetSettings()
        {
            try
            {
                if(_lastChange < DateTime.Now.AddHours(-1))
                {

                

                Settings setting = await _unitOfWork.SettingsRepository.GetById(1);

                if (setting is not null)
                    {
                        _viewDetailSettingsDto = SettingsMapper.ToViewDetail(setting);
                        _lastChange = DateTime.Now;
                    }
                }

                return _viewDetailSettingsDto;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<ViewDetailSettingsDto> UpdateSettings(CreateSettingsDto settingsDto, string userName = "Not User")
        {
            try
            {
                Settings setting = await _unitOfWork.SettingsRepository.GetById(1);

                setting.CompanyDirection = settingsDto.CompanyDirection;
                setting.CompanyName = settingsDto.CompanyName;
                setting.CompanyDescription = settingsDto.CompanyDescription;
                setting.CompanyType= settingsDto.CompanyType;
                setting.CompanyWebTitle = settingsDto.CompanyWebTitle;
                setting.CompanyPhoneNumber = settingsDto.CompanyPhoneNumber;
                setting.CompanyLocation = settingsDto.CompanyLocation;
                setting.CompanyEmail = settingsDto.CompanyEmail;
                setting.CompanyPhoneNumber = settingsDto.CompanyPhoneNumber;
                setting.CompanyImageLogo = settingsDto.CompanyImageLogo;

                setting.EditBy = userName;
                setting.EditTime= DateTime.Now;

                await _unitOfWork.SettingsRepository.Update(setting);

                await _unitOfWork.Save();

                _viewDetailSettingsDto = SettingsMapper.ToViewDetail(setting);

                return _viewDetailSettingsDto;

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<ViewDetailSettingsDto> GetSettingsNow()
        {

            try
            {
                Settings setting = await _unitOfWork.SettingsRepository.GetById(1);

                if (setting is not null)
                {
                    _viewDetailSettingsDto = SettingsMapper.ToViewDetail(setting);
                    return _viewDetailSettingsDto;
                }

                return new ViewDetailSettingsDto();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
           
        }
    }
}
