﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;

namespace BibionERP.Application.Services
{
    /// <summary>
    /// Servicio que maneja el tipo de categorias de entrega
    /// </summary>
    public class CategoryDeliveryNoteService : ICategoryDeliveryNoteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private ILogger<CategoryDeliveryNoteService> _logger;

        public CategoryDeliveryNoteService(IUnitOfWork unitOfWork, 
            ILogger<CategoryDeliveryNoteService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        /// <summary>
        /// Función para eliminar la categoria
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna un bool si se realiza el borrado</returns>
        public async Task<bool> Delete(int id, string userName = "Not User")
        {
            CategoryDeliveryNote categoryDelivery = await _unitOfWork.CategoryDeliveryNoteRepository.GetById(id);

            if (categoryDelivery is not null)
            {
                if (categoryDelivery.IsDelete.Equals(false))
                {
                    categoryDelivery.DeleteBy = userName;

                    await _unitOfWork.CategoryDeliveryNoteRepository.Delete(categoryDelivery);
                    await _unitOfWork.Save();

                    _logger.LogInformation($"Se elimino la categoria {categoryDelivery.Name} - {userName}",categoryDelivery);
                    return true;
                }

            }
            _logger.LogInformation($"No se elimino la categoria con el ID - {id}", categoryDelivery);
            return false;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<IEnumerable<ViewDetailCategoryDeliveryNote>> Find(Expression<Func<CategoryDeliveryNote, bool>> predicate)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Funcion que retorna la lista de categorias de entrega
        /// </summary>
        /// <returns>Retorna un IEnumerable<ViewDetailCategoryDeliveryNote></returns>
        public async Task<IEnumerable<ViewDetailCategoryDeliveryNote>> GetAll()
        {
            try
            {

                List<CategoryDeliveryNote> listCategoryDelivery = (await _unitOfWork.CategoryDeliveryNoteRepository.GetAll()).ToList();

                List<ViewDetailCategoryDeliveryNote> listCategoryDeliveryDto = new List<ViewDetailCategoryDeliveryNote>();

                foreach (var category in listCategoryDelivery)
                {
                    listCategoryDeliveryDto.Add(CategoryDeliveryNoteMapper.ToCategoryDeliveryNote(category));
                }

                _logger.LogInformation($"Se devuelve la lista de categorias cantidad {listCategoryDeliveryDto.Count}");

                return listCategoryDeliveryDto;

            }
            catch (Exception ex)
            {
                _logger.LogError($"Error al listar las categorias {ex.Message}");
                return new List<ViewDetailCategoryDeliveryNote>();
            }
        }

        /// <summary>
        /// funcion que devuleve un categoria por el Id que se pasa
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorna un ViewDetailCategoryDeliveryNote</returns>
        public async Task<ViewDetailCategoryDeliveryNote> GetById(int id)
        {
            try
            {

                CategoryDeliveryNote categoryDelivery = await _unitOfWork.CategoryDeliveryNoteRepository.GetById(id);

                _logger.LogInformation($"Se devuelve la categoria Id {id} {categoryDelivery.Name}");

                return CategoryDeliveryNoteMapper.ToCategoryDeliveryNote(categoryDelivery);
            
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error al buscar Categoria con el Id {id} {ex.Message}");
                return new ViewDetailCategoryDeliveryNote();
            }
        }

        /// <summary>
        /// Funcion que inserta una nueva categoria para las entregas
        /// </summary>
        /// <param name="categoryDetailDto"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna una ViewDetailCategoryDeliveryNote </returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<ViewDetailCategoryDeliveryNote> Insert(CreateCategoryDeliveryNote categoryDetailDto, string userName = "Not User")
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Funcion que actualiza una categoria para las entregas
        /// </summary>
        /// <param name="categoryDetailDto"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna una ViewDetailCategoryDeliveryNote </returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<ViewDetailCategoryDeliveryNote> Update(CreateCategoryDeliveryNote categoryDetailDto, int id, string userName = "Not User")
        {
            throw new NotImplementedException();
        }
    }
}
