﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;

namespace BibionERP.Application.Services
{
    public class WarehouseService : IWarehouseService
    {

        private readonly IUnitOfWork _unitOfWork;
        private ILogger<WarehouseService> _logger;

        public WarehouseService(IUnitOfWork unitOfWork, ILogger<WarehouseService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<bool> Delete(int id, string userName = "Not User")
        {
            try
            {
                Warehouse warehouse = await _unitOfWork.WarehouseRepository.GetById(id);


                if (warehouse is not null)
                {
                    if (warehouse.IsDelete.Equals(false))
                    {

                        warehouse.DeleteBy = userName;

                        await _unitOfWork.WarehouseRepository.Delete(warehouse);
                        await _unitOfWork.Save();

                        _logger.LogInformation($"Se borro el almacen {warehouse.Id} {warehouse.Name}");
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<IEnumerable<ViewWarehouseDto>> Find(Expression<Func<Warehouse, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewWarehouseDto>> GetAll()
        {
            try
            {

                List<Warehouse> listWarehouse = (await _unitOfWork.WarehouseRepository.GetAll()).ToList();

                List<ViewWarehouseDto> listWarehouseDto = new List<ViewWarehouseDto>();

                foreach (var warehouse in listWarehouse)
                {
                    listWarehouseDto.Add(WarehouseMapper.ToViewWarehouseDto(warehouse));
                }

                _logger.LogInformation($"Lista de almacen almacen {listWarehouseDto.Count}");
                return listWarehouseDto;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<IEnumerable<ViewWarehouseDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public async Task<ViewDetailWarehouseDto> GetById(int id)
        {
            try
            {
                var warehouse = await _unitOfWork.WarehouseRepository.GetById(id);

                _logger.LogInformation($"Get almacen {warehouse.Id} {warehouse.Name}");

                return WarehouseMapper.ToViewDetailWarehouseDto(warehouse);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }

        }

        public async Task<ViewDetailWarehouseDto> Insert(CreateWarehouseDto warehouseDto, string userName = "Not User")
        {
            try
            {

                Warehouse warehouse = WarehouseMapper.ToWarehouse(warehouseDto);

                warehouse.CreateBy = userName;

                await _unitOfWork.WarehouseRepository.Insert(warehouse);

                await _unitOfWork.Save();

                _logger.LogInformation($"Almacén ingresado con exito {warehouse.Id} {warehouse.Name}");
                return WarehouseMapper.ToViewDetailWarehouseDto(warehouse);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<ViewDetailWarehouseDto> Update(CreateWarehouseDto warehouseDto, int id, string userName = "Not User")
        {

            try
            {
                var warehouse = await _unitOfWork.WarehouseRepository.GetById(id);

                warehouse.Description = warehouseDto.Description;
                warehouse.Name = warehouseDto.Name;
                warehouse.EditBy = userName;

                await _unitOfWork.WarehouseRepository.Update(warehouse);
                await _unitOfWork.Save();
                _logger.LogInformation($"Almacén actualizado con exito {warehouse.Id} {warehouse.Name}");
                return WarehouseMapper.ToViewDetailWarehouseDto(warehouse);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }


            

        }
    }
}
