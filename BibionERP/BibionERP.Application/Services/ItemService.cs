﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;

namespace BibionERP.Application.Services
{
    public class ItemService : IItemService
    {

        private readonly IUnitOfWork _unitOfWork;
        private ILogger<ItemService> _logger;

        public ItemService(IUnitOfWork unitOfWork, ILogger<ItemService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<bool> Delete(int id, string userName = "Not User")
        {
            try
            {
                Item item = await _unitOfWork.ItemRepository.GetById(id);

                if (item is not null)
                {
                    if (item.IsDelete.Equals(false))
                    {

                        item.DeleteBy = userName;

                        await _unitOfWork.ItemRepository.Delete(item);
                        await _unitOfWork.Save();

                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<IEnumerable<ViewItemDto>> Find(Expression<Func<Item, bool>> predicate, int id = 0)
        {
            try
            {
                List<Item> listItems = (await _unitOfWork.ItemRepository.Find(predicate,null, "Supplier,WarehouseItem",true)).ToList();



                List<ViewItemDto> listItemsDto = new List<ViewItemDto>();

            

                foreach (var item in listItems)
                {
                    var itemDto = ItemMapper.ToViewItemDto(item);

                    if(!id.Equals(0))
                    {
                        itemDto.Quantity = item.WarehouseItem.Where(wi => wi.WarehouseId.Equals(id)).Select(wi => wi.Quantity).FirstOrDefault();
                    }

                    listItemsDto.Add(itemDto);
                }

                return listItemsDto;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<IEnumerable<ViewItemDto>> GetAll()
        {
            try
            {

                List<Item> listItems = (await _unitOfWork.ItemRepository.GetAll("Supplier,WarehouseItem.Warehouse")).ToList();

                List<ViewItemDto> listItemsDto = new List<ViewItemDto>();

                foreach (var item in listItems)
                {
                    listItemsDto.Add(ItemMapper.ToViewItemDto(item));

                }

                return listItemsDto;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<IEnumerable<ViewItemDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public async Task<ViewDetailItemDto> GetById(int id)
        {
            try
            {

                Item item = (await _unitOfWork.ItemRepository.Find(i => i.Id.Equals(id),null, "Supplier,CategoryItem,WarehouseItem.Warehouse", true )).FirstOrDefault();


                ViewDetailItemDto itemDto = ItemMapper.ToViewDetailItemDto(item);

                itemDto.SupplierName = item.Supplier.Name;
                itemDto.CategoryName = item.CategoryItem.Name;

                List<ViewWarehoueItemDto> wi = new();


                if(item.WarehouseItem is not null)
                { 
                    foreach (var w in item.WarehouseItem)
                    {
                         wi.Add(new ViewWarehoueItemDto()
                             {
                             WarehouseId = w.WarehouseId,
                             WarehouseName = w.Warehouse.Name,
                             Quantity = w.Quantity,
                         });
                    }
                }
                

                itemDto.ViewWarehouseItemsDto = wi;
                              

                return itemDto;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<ViewDetailItemDto> Insert(CreateItemDto itemDto, string userName = "Not User")
        {
            try
            {

                Item item = ItemMapper.ToItem(itemDto);

                item.CategoryItem = await _unitOfWork.CategoryItemRepository.GetById(itemDto.CategoryId);

                item.Supplier = await _unitOfWork.SupplierRepository.GetById(itemDto.SupplierId);

                //item.Warehouse = await _unitOfWork.WarehouseRepository.GetById(itemDto.WarehouseId);

                item.CreateBy = userName;

                await _unitOfWork.ItemRepository.Insert(item);

                await _unitOfWork.Save();

                return ItemMapper.ToViewDetailItemDto(item);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<ViewDetailItemDto> Update(CreateItemDto itemDto, int id, string userName = "Not User")
        {
            throw new NotImplementedException();
        }
    }
}
