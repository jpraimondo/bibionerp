﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.Interface;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using BibionERP.Domain.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace BibionERP.Application.Services
{
    /// <summary>
    /// Servicio que controla la autenticacion de  los usuarios
    /// </summary>
    public class AuthService : IAuthService
    {

        private IUnitOfWork _unitOfWork;
        private ILogger<AuthService> _logger;
       

        public AuthService(IUnitOfWork unitOfWork, ILogger<AuthService> logger)
        {
            _unitOfWork = unitOfWork; 
            _logger = logger;
        }

        /// <summary>
        /// Función para cambiar el password en la aplicación
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Retorna un tipo ViewAuthDto</returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<ViewAuthDto> ChangePassword(RegisterAuthDto request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Servicio para el login del usuario en la aplicacion
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Retorna un tipo ViewAuthDto</returns>
        public async Task<ViewAuthDto> Login(CreateAuthDto request)
        {
            var msg = string.Empty;

            try
            {
                var user = (await _unitOfWork.UsersRepository.Find(x => x.Email.Equals(request.Email))).FirstOrDefault();

                
                if (user is null)
                {
                    msg += $"Error en el usuario o password";
                }
                else if(user.IsActive.Equals(false) || user.IsDelete.Equals(true))
                {
                    msg += $"Error en el usuario.";
                }
                else
                {
                user.Rol = await _unitOfWork.RolesRepository.GetById(user.RolId);

                var hasher = new PasswordHasher<ApplicationUser>();

                var resultado = hasher.VerifyHashedPassword(user, user.HashPassword, request.Password);

                if (resultado.Equals(PasswordVerificationResult.Failed))
                {
                    msg += $"Error en email o password ";
                }
                }
                if (!(msg.Length > 0))
                {
                    user.LastLogin = DateTime.Now;

                    await _unitOfWork.UsersRepository.Update(user);
                    await _unitOfWork.Save();

                    _logger.LogInformation($"login de usuario {user.UserName}", user);

                    return new ViewAuthDto
                    {
                        AuthResponse = new AuthResponse
                        {
                            UserId = user.Id,
                            EmployeeId = user.EmployeeId,
                            Email = user.Email,
                            FullName = $"{user.FirstName} {user.LastName}",
                            Role = user.Rol.NormalizedName
                        },
                        Message = "Usuario logueado con exito."

                    };

                }
                else{

                    return new ViewAuthDto
                    {
                        AuthResponse = null,
                        Message = msg

                    };
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error en el login de usuario {ex.Message}", request.Email);

                return new ViewAuthDto
                {
                    AuthResponse = null,
                    Message = ex.Message

                };
            }
        }


        /// <summary>
        /// Servicio para registrar un nuevo usuario en la aplicacion
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Retorna un tipo ViewAuthDto</returns>
        /// <exception cref="NotImplementedException"></exception>
        //public async Task<ViewAuthDto> Register(RegisterAuthDto request)
        //{
            //try
            //{
            //    AuthResponse authResponse = new AuthResponse();//await _identityService.Register(AuthMapper.ToRegisterRequest(request));

            //    return new ViewAuthDto
            //    {
            //        AuthResponse = authResponse,
            //        Message = "Usuario registrado con exito."

            //    };
            //}
            //catch (Exception ex)
            //{

            //    return new ViewAuthDto
            //    {
            //        AuthResponse = null,
            //        Message = ex.Message

            //    };
            //}
        //    throw new NotImplementedException();

        //}
    }
}
