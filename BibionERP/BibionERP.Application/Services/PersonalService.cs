﻿using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Helpers;
using BibionERP.Application.Interface;
using Microsoft.Extensions.Configuration;
using System.ComponentModel;

namespace BibionERP.Application.Services
{
    public class PersonalService : IPersonalService
    {

        readonly PersonalHelper personalHelper;

        List<ViewDetailPersonalDto> listaPersonal = null;

        DateTime _lastUpdate = DateTime.MinValue;


        PersonalData personalData = new PersonalData();

        public PersonalService(IConfiguration configuration)
        {
            personalHelper= new PersonalHelper(configuration);
        }

        public async Task<bool> Delete(int id)
        {
            var result = await personalHelper.ModificarPersonal(id);

            if (result)
            {
                listaPersonal = listaPersonal.Where(p => p.Id != id).ToList();
                return true;
            }

            return false;
        }

        public async Task<IEnumerable<ViewDetailPersonalDto>> GetAll()
        
        {
            if((_lastUpdate.AddMinutes(10) < DateTime.Now)) 
            {
                listaPersonal = (await personalHelper.ObtenerTodosPersonal()).ToList();
                _lastUpdate = DateTime.Now;
                return listaPersonal;
            }                
                return listaPersonal;
            
            
        }

        public async Task<PersonalData> GetPersonalData()

        {
            if(personalData.lastUpdate < DateTime.Now.AddMinutes(-10) )
            {
               personalData = await personalHelper.ObtenerDatosPersonal();
            }

                        
            return personalData;
        }
    }
}
