﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;

namespace BibionERP.Application.Services
{
    public class ServiceService : IServiceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private ILogger<ServiceService> _logger;

        public ServiceService(IUnitOfWork unitOfWork, ILogger<ServiceService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<bool> Delete(int id, string userName = "Not User")
        {
            try
            {
                Service service = await _unitOfWork.ServiceRepository.GetById(id);

                if (service is not null)
                {
                    if (service.IsDelete.Equals(false))
                    {

                        service.DeleteBy = userName;

                        await _unitOfWork.ServiceRepository.Delete(service);
                        await _unitOfWork.Save();

                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<IEnumerable<ViewServiceDto>> Find(Expression<Func<Service, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewServiceDto>> GetAll()
        {
            try
            {

                List<Service> listServices = (await _unitOfWork.ServiceRepository.GetAll()).ToList();

                List<ViewServiceDto> listServicesDto = new List<ViewServiceDto>();

                foreach (var service in listServices)
                {
                    listServicesDto.Add(ServiceMapper.ToViewServiceDto(service));
                }

                return listServicesDto;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<IEnumerable<ViewServiceDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public async Task<ViewDetailServiceDto> GetById(int id)
        {
            try
            {
                var service = await _unitOfWork.ServiceRepository.GetById(id);

                return ServiceMapper.ToViewDetailServiceDto(service);
            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<ViewDetailServiceDto> Insert(CreateServiceDto serviceDto, string userName = "Not User")
        {
            try
            {
                Service service = ServiceMapper.ToService(serviceDto);

                service.CreateBy=userName;

                await _unitOfWork.ServiceRepository.Insert(service);

                await _unitOfWork.Save();

                return ServiceMapper.ToViewDetailServiceDto(service);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<ViewDetailServiceDto> Update(CreateServiceDto serviceDto, int id, string userName = "Not User")
        {
            try
            {
                Service service = await _unitOfWork.ServiceRepository.GetById(id);

                service.Name = serviceDto.Name;
                service.Description = serviceDto.Description;
                service.CostPrice = serviceDto.CostPrice;
                service.TotalPrice = serviceDto.TotalPrice;

                service.EditBy = userName;

                await _unitOfWork.ServiceRepository.Update(service);

                await _unitOfWork.Save();

                return ServiceMapper.ToViewDetailServiceDto(service);
            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message);
                throw;
            }

        }
    }
}
