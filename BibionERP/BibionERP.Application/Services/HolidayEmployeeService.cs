﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;


namespace BibionERP.Application.Services
{
    public class HolidayEmployeeService : IHolidayEmployeeService
    {

        private readonly IUnitOfWork _unitOfWork;
        private ILogger<HolidayEmployeeService> _logger;

        public HolidayEmployeeService(IUnitOfWork unitOfWork, ILogger<HolidayEmployeeService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }


        public Task<bool> Delete(int id, string userName = "Not User")
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ViewHolidayEmployeeDto>> Find(Expression<Func<HolidayEmployee, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewHolidayEmployeeDto>> GetAll()
        {
            try
            {

                List<HolidayEmployee> listHolidayEmployee = (await _unitOfWork.HolidayEmployeeRepository.GetAll("Employee")).ToList();

                List<ViewHolidayEmployeeDto> listHolidayEmployeeDto = new();

                foreach (var item in listHolidayEmployee)
                {
                    listHolidayEmployeeDto.Add(HolidayEmployeeMapper.ToViewHolidayDto(item));
                }

                _logger.LogInformation($"Se listaron holiday employee {listHolidayEmployeeDto.Count}");
                return listHolidayEmployeeDto;

            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<IEnumerable<ViewHolidayEmployeeDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public Task<ViewHolidayEmployeeDto> GetById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<ViewHolidayEmployeeDto> Insert(CreateHolidayEmployeeDto holidayEmployeeDto, string userName = "Not User")
        {
            try
            {
                var holidayEmployee = HolidayEmployeeMapper.ToHoliday(holidayEmployeeDto);

                holidayEmployee.Employee = await _unitOfWork.EmployeeRepository.GetById(holidayEmployeeDto.EmployeeId); 
                holidayEmployee.CreateBy=userName;


                await _unitOfWork.HolidayEmployeeRepository.Insert(holidayEmployee);
                await _unitOfWork.Save();

                return HolidayEmployeeMapper.ToViewHolidayDto(holidayEmployee);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<ViewHolidayEmployeeDto> Update(CreateHolidayEmployeeDto holidayEmployeeDto, int id, string userName = "Not User")
        {
            throw new NotImplementedException();
        }
    }
}
