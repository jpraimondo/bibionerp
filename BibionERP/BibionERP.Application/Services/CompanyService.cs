﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;
using System.Xml.Schema;

namespace BibionERP.Application.Services
{
    /// <summary>
    /// Servicio que maneja las empresas a las que se venden servicios
    /// </summary>
    public class CompanyService : ICompanyService
    {

        private readonly IUnitOfWork _unitOfWork;
        private ILogger<CompanyService> _logger;

        public CompanyService(IUnitOfWork unitOfWork,ILogger<CompanyService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        /// <summary>
        /// Funcion que elimina a una empresa por un Id dado
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna un bool con el resultado de la operación</returns>
        public async Task<bool> Delete(int id, string userName = "Not User")
        {
            try
            {
                Company company = await _unitOfWork.CompanyRepository.GetById(id);

                if (company is not null)
                {
                    if (company.IsDelete.Equals(false))
                    {

                        company.DeleteBy = userName;

                        await _unitOfWork.CompanyRepository.Delete(company);
                        await _unitOfWork.Save();

                        _logger.LogInformation(message: $"Se elimino la empresa Id {id} {company.Name} | {userName}");
                        return true;
                    }

                }
                _logger.LogInformation(message: $"No se encontro la empresa Id {id}");
                return false;
            }
            catch (Exception ex)
            {
                
                _logger.LogError(message: $"Error al eliminar la empresa Id {id} {ex.Message}");
               throw;
            }
        }
        /// <summary>
        /// Función que busca una lista  de empresas por una expression dada
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>retorna una IEnumerable<ViewCompanyDto></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<IEnumerable<ViewCompanyDto>> Find(Expression<Func<Company, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Función que retorna una lista de empresas no borradas
        /// </summary>
        /// <returns>Retorna un IEnumerable<ViewCompanyDto></returns>
        public async Task<IEnumerable<ViewCompanyDto>> GetAll()
        {
            try
            {

                List<Company> listCompanies = (await _unitOfWork.CompanyRepository.GetAll()).ToList();

                List<ViewCompanyDto> listCompanyDto = new List<ViewCompanyDto>();

                foreach (var company in listCompanies)
                {
                    listCompanyDto.Add(CompanyMapper.ToViewCompanyDto(company));
                }

                _logger.LogInformation(message: $"Se genera una lista de de empresas cantidad {listCompanyDto.Count}");
                return listCompanyDto;

            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al listar las empresas {ex.Message}");
                return new List<ViewCompanyDto>();
            }
        }
        /// <summary>
        /// Devuelve un listado con todas las empresas, incluso las marcadas como borradas
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<IEnumerable<ViewCompanyDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Devuelve una empresa por un Id dado
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorna un ViewDetailCompanyDto</returns>
        
        public async Task<ViewDetailCompanyDto> GetById(int id)
        {
            try
            {
                Company company = await _unitOfWork.CompanyRepository.GetById(id, "Customer,CompanyServices,CompanyServices.Service");

                ViewDetailCompanyDto detailCompanyDto = CompanyMapper.ToViewDetailCompanyDto(company);

                detailCompanyDto.CompanyServices = new List<ViewCompanyServiceDto>();

                if (company.Customer is not null)
                {
                    detailCompanyDto.CustomerName = $"{company.Customer.FirstName} {company.Customer.LastName}";
                }

                if (company.CompanyServices is not null)
                {
                    foreach (var service in company.CompanyServices)
                    {
                        detailCompanyDto.CompanyServices.Add(CompanyServiceMapper.ToViewCompanyServiceDto(service));
                    }
                }

                    _logger.LogInformation(message:$"Se busca la empresa Id {id} {company.Name}");
                return detailCompanyDto;
            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al obtener la empresa con Id {id} {ex.Message}");
                throw;
            }
            
        }

        /// <summary>
        /// Funcion que crea una nueva empresa
        /// </summary>
        /// <param name="companyDto"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna una nueva ViewDetailCompanyDto</returns>
        public async Task<ViewDetailCompanyDto> Insert(CreateCompanyDto companyDto, string userName = "Not User")
        {
            try
            {

                Company company = CompanyMapper.ToCompany(companyDto);

                company.Customer = await _unitOfWork.CustomerRepository.GetById(companyDto.CustomerId);

                company.CreateBy = userName;

                await _unitOfWork.CompanyRepository.Insert(company);

                await _unitOfWork.Save();

                _logger.LogInformation(message:$"Se creo la empresa: {company.Id} {company.Name}");

                return CompanyMapper.ToViewDetailCompanyDto(company);

            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al crear la empresa {ex.Message}");
                return new ViewDetailCompanyDto();
            }
        }

        /// <summary>
        /// Funcion paa actualizar los datos la empresa
        /// </summary>
        /// <param name="companyDto"></param>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <returns>Retorna un ViewDetailCompanyDto</returns>

        public async Task<ViewDetailCompanyDto> Update(CreateCompanyDto companyDto, int id, string userName = "Not User")
        {
            try
            {
                Company company = await _unitOfWork.CompanyRepository.GetById(id);

                company.Name = companyDto.Name;
                company.Address = companyDto.Address;
                company.Phone = companyDto.Phone;
                company.Description = companyDto.Description;
                company.EditBy = userName;

                Customer customer = await _unitOfWork.CustomerRepository.GetById(companyDto.CustomerId);

                company.Customer = customer;
                company.CustomerId = customer.Id;

                await _unitOfWork.CompanyRepository.Update(company);
                await _unitOfWork.Save();

                _logger.LogInformation(message: $"Se actualizan los datos de la empresa Id {id} - {userName}");
                return CompanyMapper.ToViewDetailCompanyDto(company);

            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al actualizar los datos de la empresa Id {id} {ex.Message}");
                return new ViewDetailCompanyDto();
            }
        }

        public async Task<ViewDetailCompanyDto> UpdateServices(int id, List<CreateCompanyServiceDto> listServicesDto, string userName = "Not User")
        {
            

            try
            {
                Company company = await _unitOfWork.CompanyRepository.GetById(id, "Customer,CompanyServices,CompanyServices.Service");


                List<Domain.Entities.CompanyService> listServices = new();

                foreach (var serviceDto in listServicesDto)
                {
                    listServices.Add(CompanyServiceMapper.ToCompanyService(serviceDto));
                }



                company.CompanyServices = listServices;

                
                await _unitOfWork.CompanyRepository.Update(company);
                await _unitOfWork.Save();

                _logger.LogInformation(message: $"Se actualizan los datos de la empresa Id {id} - {userName}");
                return CompanyMapper.ToViewDetailCompanyDto(company);

            }
            catch (Exception ex)
            {
                _logger.LogError(message: $"Error al actualizar los datos de la empresa Id {id} {ex.Message}");
                throw;
            }


        }
    }


}
