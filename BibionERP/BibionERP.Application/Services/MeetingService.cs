﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;

namespace BibionERP.Application.Services
{
    public class MeetingService : IMeetingService
    {

        private readonly IUnitOfWork _unitOfWork;
        private ILogger<MeetingService> _logger;

        public MeetingService(IUnitOfWork unitOfWork, ILogger<MeetingService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public Task<bool> Delete(int id, string userName = "Not User")
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ViewMeetingDto>> Find(Expression<Func<Meeting, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewMeetingDto>> GetAll()
        {
            try
            {

                List<Meeting> listMeeting = (await _unitOfWork.MeetingRepository.GetAll("MeetingAttendance")).ToList();

                List<ViewAttendanceDto> attendanceList = new List<ViewAttendanceDto>();

               
                List<ViewMeetingDto> listMeetingDto = new List<ViewMeetingDto>();

                foreach (var meet in listMeeting)
                {
                    listMeetingDto.Add(MeetingMapper.ToViewMeetingDto(meet));
                }

                return listMeetingDto;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<IEnumerable<ViewMeetingDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public Task<ViewDetaiLeaveDto> GetById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<ViewDetaiLeaveDto> Insert(CreateMeetingDto meetingDto, string userName = "Not User")
        {
            try
            {
                Meeting meeting = MeetingMapper.ToMeeting(meetingDto);

                meeting.CreateBy = userName;

                await _unitOfWork.MeetingRepository.Insert(meeting);

                await _unitOfWork.Save();

                return MeetingMapper.ToViewDetailMeetingDto(meeting);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Task<ViewDetaiLeaveDto> Update(CreateMeetingDto meetingDto, int id, string userName = "Not User")
        {
            throw new NotImplementedException();
        }
    }
}
