﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Application.Interface;
using BibionERP.Application.Mapper;
using BibionERP.Application.Persistence.Interface;
using BibionERP.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;

namespace BibionERP.Application.Services
{

    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private ILogger<UserService> _logger;

        public UserService(IUnitOfWork unitOfWork, ILogger<UserService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<bool> Delete(int id, string userName = "Not User")
        {
            try
            {
                          
                ApplicationUser user = await _unitOfWork.UsersRepository.GetById(id);


                if (user is not null)
                {
                    if (user.IsDelete.Equals(false))
                    {

                        user.DeleteBy = userName;

                        await _unitOfWork.UsersRepository.Delete(user);
                        await _unitOfWork.Save();

                        return true;
                    }
                }
                return false;
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<IEnumerable<ViewUserDto>> Find(Expression<Func<ApplicationUser, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewUserDto>> GetAll()
        {
            try
            {

            List<ApplicationUser> listUser = (await _unitOfWork.UsersRepository.GetAll("Rol")).ToList();

            List <ViewUserDto> listUsersDto = new List<ViewUserDto>();

            foreach (var user in listUser)
            {
                listUsersDto.Add(UserMapper.toViewUserDto(user));
            }

            return listUsersDto;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public Task<IEnumerable<ViewUserDto>> GetAllAndDelete()
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<ViewDetailUserDto> GetById(int id)
        {
            try
            {
                ApplicationUser user = await _unitOfWork.UsersRepository.GetById(id, "Rol");

                //user.Rol = await _unitOfWork.RolesRepository.GetById(user.RolId);

                return UserMapper.toViewDetailUserDto(user);

            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<ViewDetailUserDto> Insert(CreateUserDto userDto, string userName = "Not User")
        {
            try
            {

                ApplicationUser user = UserMapper.toApplicationUser(userDto);

                if(userDto.RolId.Equals(0))
                {
                    user.Rol = await _unitOfWork.RolesRepository.GetById(2);
                }
                else
                {
                    user.Rol = await _unitOfWork.RolesRepository.GetById(userDto.RolId);
                }

                var hasher = new PasswordHasher<ApplicationUser>();

                user.HashPassword = hasher.HashPassword(user,userDto.Password);

                user.CreateBy = userName;

                await _unitOfWork.UsersRepository.Insert(user);

                await _unitOfWork.Save();

                return UserMapper.toViewDetailUserDto(user);

            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }



            }

        public async Task<ViewDetailUserDto> Update(CreateUserDto userDto, int id, string userName = "Not User")
        {
            try
            {
                ApplicationUser user = await _unitOfWork.UsersRepository.GetById(id);

 
                user.UserName = userDto.UserName;
                user.FirstName = userDto.FirstName;
                user.LastName = userDto.LastName;
                user.Address = userDto.Address;
                user.Document = userDto.Document;
                user.Email = userDto.Email;
                user.PhoneNumber = userDto.PhoneNumber;
                user.Location = userDto.Location;
                user.RolId = userDto.RolId;

                user.Rol = await _unitOfWork.RolesRepository.GetById(userDto.RolId);


                user.EditBy = userName;

                await _unitOfWork.UsersRepository.Update(user);
                await _unitOfWork.Save();

                return UserMapper.toViewDetailUserDto(user);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }
    

        public async Task<ViewDetailUserDto> ResetPassword(CreateUserDto userDto,  int id, string userName = "Not User")
        {
            try
            {
                ApplicationUser user = await _unitOfWork.UsersRepository.GetById(id);

                var hasher = new PasswordHasher<ApplicationUser>();

                user.HashPassword = hasher.HashPassword(user, userDto.Password);

                user.EditBy = userName;

                await _unitOfWork.UsersRepository.Update(user);
                await _unitOfWork.Save();

                return UserMapper.toViewDetailUserDto(user);

            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message);
                throw;
            }
        }
    
        
        public async Task<bool> EnableDisableUser(int id, string userName)
        {
            try
            {
                ApplicationUser user = await _unitOfWork.UsersRepository.GetById(id);

                user.IsActive = !user.IsActive;

                user.EditBy = userName;

                await _unitOfWork.UsersRepository.Update(user);
                await _unitOfWork.Save();

                return true;

            } catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }
    
    }
}
