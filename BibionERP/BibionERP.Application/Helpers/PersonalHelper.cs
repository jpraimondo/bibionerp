﻿using BibionERP.Application.DTOs.ViewDetail;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Net;

namespace BibionERP.Application.Helpers
{
    public class PersonalHelper
    {
        private readonly IConfiguration _configuration;

        private readonly string urlApi = "http://api.portman.com.uy/";

        private readonly string urlBase = "https://www.portman.com.uy/";

        public PersonalHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<IEnumerable<ViewDetailPersonalDto>> ObtenerTodosPersonal()
        {
            var token = _configuration.GetSection("ListToken").Value;
            try
            {


            List<ViewDetailPersonalDto> personalList = new List<ViewDetailPersonalDto>();
            using (var httpClient = new HttpClient())
            {

                    HttpResponseMessage response = await httpClient.GetAsync($"{urlApi}personal.php?token={token}");
                    //HttpResponseMessage response = await httpClient.GetAsync($"{urlApi}personal.php");

                    if (response.IsSuccessStatusCode)
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        personalList = JsonConvert.DeserializeObject<List<ViewDetailPersonalDto>>(apiResponse).Select(
                            s => new ViewDetailPersonalDto
                            {
                                Id = s.Id,
                                Nombre = s.Nombre,
                                Apellido = s.Apellido,
                                Localidad = s.Localidad,
                                Direccion = s.Direccion,
                                Email = s.Email,
                                Experiencia = s.Experiencia,
                                Masinfo = s.Masinfo,
                                Telefono = s.Telefono,
                                Pathfile = urlBase + s.Pathfile,
                            }
                            ).ToList();
                    }
                
            }
                return personalList;
            }
            catch (Exception ex)
            {

                return new List<ViewDetailPersonalDto>();
            }
            
        }


        public async Task<bool> ModificarPersonal(int id)
        {
            var token = _configuration.GetSection("DeleteToken").Value;
                        
            using (var httpClient = new HttpClient())
            {

                HttpResponseMessage response = await httpClient.PostAsync($"{urlApi}editpersonal.php?delete={id}&token={token}",null);
                
                if(response.StatusCode.Equals(HttpStatusCode.OK))
                {
                    return true;
                }
               
            return false;
               
                
            }
          
        }

        public async Task<PersonalData> ObtenerDatosPersonal()
        {
            PersonalData personalData = new PersonalData();
            List<PersonalData> datas = new List<PersonalData>();

            using (var httpClient = new HttpClient())
            {
                HttpResponseMessage response = await httpClient.GetAsync(urlApi + "personaldia.php");
                string apiResponse = await response.Content.ReadAsStringAsync();
                datas = JsonConvert.DeserializeObject<List<PersonalData>>(apiResponse)
                    .Select(s => new PersonalData
                    {
                        cantidadTotal= s.cantidadTotal,
                        cantidadDia = s.cantidadDia,
                        cantidadMes = s.cantidadMes
                    }).ToList();
 
            }
            personalData.cantidadTotal = datas[0].cantidadTotal;
            personalData.cantidadDia = datas[0].cantidadDia;
            personalData.cantidadMes = datas[0].cantidadMes;
            personalData.lastUpdate = DateTime.Now;

            return personalData;
        }

    }
}
