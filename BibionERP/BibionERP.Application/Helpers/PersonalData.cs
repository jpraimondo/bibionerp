﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Application.Helpers
{
    public class PersonalData
    {
        [JsonProperty("cantidadTotal")]
        public int cantidadTotal;
        [JsonProperty("cantidadDia")]
        public int cantidadDia;
        [JsonProperty("cantidadMes")]
        public int cantidadMes;

        public DateTime lastUpdate = DateTime.MinValue;


    }
}
