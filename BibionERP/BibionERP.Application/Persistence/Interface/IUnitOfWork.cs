﻿using BibionERP.Domain.Entities;

namespace BibionERP.Application.Persistence.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        public IGenericRepository<CategoryDeliveryNote> CategoryDeliveryNoteRepository { get; }
        public IGenericRepository<CategoryItem> CategoryItemRepository { get; }
        public IGenericRepository<Company> CompanyRepository { get; }
        public IGenericRepository<Customer> CustomerRepository { get; }
        public IGenericRepository<DeliveryNoteDetail> DeliveryNoteDetailRepository { get; }
        public IGenericRepository<DeliveryNote> DeliveryNoteRepository { get; }
        public IGenericRepository<Employee> EmployeeRepository { get; }
        public IGenericRepository<Item> ItemRepository { get; }
        public IGenericRepository<Meeting> MeetingRepository { get; }
        public IGenericRepository<Service> ServiceRepository { get; }
        public IGenericRepository<Supplier> SupplierRepository { get; }
        public IGenericRepository<Warehouse> WarehouseRepository { get; }
        public IGenericRepository<ApplicationRol> RolesRepository { get; }
        public IGenericRepository<ApplicationUser> UsersRepository { get; }
        public IGenericRepository<Settings> SettingsRepository { get; }
        public IGenericRepository<Holiday> HolidayRepository { get; }
        public IGenericRepository<HolidayEmployee> HolidayEmployeeRepository { get; }
        public IGenericRepository<Note> NoteRepository { get; }
        public IGenericRepository<Social> SocialRepository { get; }


        Task Save();

    }
}
