﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class CategoryItemMapper
    {

        internal static CategoryItem ToCategoryItem(CreateCategoryItemDto createCategoryItemDto)
        {
            return new CategoryItem()
            {
                Name = createCategoryItemDto.Name.Trim(),
                Description = createCategoryItemDto.Description.Trim(),
            };
        }

        internal static ViewCategoryItemDto ToViewCategoryItemDto(CategoryItem categoryItem)
        {
            return new ViewCategoryItemDto()
            {
                Id = categoryItem.Id,
                Name = categoryItem.Name,
            };
        }

        internal static ViewDetailCategoryItemDto ToViewDetailCategoryItemDto(CategoryItem categoryItem)
        {
            return new ViewDetailCategoryItemDto()
            {
                Id = categoryItem.Id,
                Name = categoryItem.Name,
                Description= categoryItem.Description,
            };
        }

    }
}
