﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    public class UserMapper
    {

        public static ApplicationUser toApplicationUser (CreateUserDto createUserDto)
        {
            return new ApplicationUser
            {
                UserName= createUserDto.UserName.Trim(),
                FirstName= createUserDto.FirstName.Trim(),
                LastName= createUserDto.LastName.Trim(),
                Email= createUserDto.Email.Trim(),
                Document= createUserDto.Document.Trim(),
                Birthday= createUserDto.Birthday,
                EmployeeId= createUserDto.EmployeeId,
                PhoneNumber= createUserDto.PhoneNumber.Trim(),
                Address= createUserDto.Address.Trim(),
                Location= createUserDto.Location.Trim(),
            };
        }

        public static ViewUserDto toViewUserDto(ApplicationUser user)
        {
            return new ViewUserDto
            {
                Id = user.Id,
                UserName = user.UserName,
                Document = user.Document,
                Email = user.Email,
                FullName = $"{user.FirstName} {user.LastName}",
                RolName = user.Rol.NormalizedName,
                LastLogin = user.LastLogin,
                IsActive = user.IsActive,
            };
        }

        public static ViewDetailUserDto toViewDetailUserDto(ApplicationUser user)
        {
            return new ViewDetailUserDto
            {
                Id = user.Id,
                UserName = user.UserName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Document = user.Document,
                Birthday = user.Birthday,
                EmployeeId = user.EmployeeId,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Address = user.Address,
                Location = user.Location,
                RolName = user.Rol.NormalizedName,
                LastLogin = user.LastLogin,
                IsActive = user.IsActive,
            };
        }

    }
}
