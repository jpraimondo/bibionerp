﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class MeetingMapper
    {
        internal static Meeting ToMeeting(CreateMeetingDto createMeetingDto)
        {
            List<MeetingAttendance> meetingAttendances = new List<MeetingAttendance>();

            if(createMeetingDto.AttendanceList is not null)
            {
                foreach (var item in createMeetingDto.AttendanceList)
                {
                    meetingAttendances.Add(
                        new MeetingAttendance()
                        {
                            AttendanceName = item.AttendanceName,
                            AttendanceId = item.AttendanceId,
                            AttendanceType = item.AttendanceType,
                           
                        });
                }
            }
            
            return new Meeting()
            {
                MeetingAttendance = meetingAttendances,
                Date= createMeetingDto.Date,
                Location= createMeetingDto.Location.Trim(),
                Notes= createMeetingDto.Notes.Trim(),
                Task= createMeetingDto.Task.Trim(),
            };
        }

        internal static ViewMeetingDto ToViewMeetingDto(Meeting meeting)
        {

            List<ViewAttendanceDto> listAttendanceDto = new List<ViewAttendanceDto>();

            foreach (var item in meeting.MeetingAttendance)
            {
                listAttendanceDto.Add(new ViewAttendanceDto
                {
                    AttendanceId= item.AttendanceId,
                    AttendanceName= item.AttendanceName,
                    AttendanceType= item.AttendanceType,
                });
            }


            return new ViewMeetingDto()
            {
                Id = meeting.Id,
                Attendances = listAttendanceDto,
                Date = meeting.Date,  
            };
        }

        internal static ViewDetaiLeaveDto ToViewDetailMeetingDto(Meeting meeting)
        {
            List<ViewAttendanceDto> listAttendanceDto = new List<ViewAttendanceDto>();

            foreach (var item in meeting.MeetingAttendance)
            {
                listAttendanceDto.Add(new ViewAttendanceDto
                {
                    AttendanceId = item.AttendanceId,
                    AttendanceName = item.AttendanceName,
                    AttendanceType = item.AttendanceType,
                });
            }

            return new ViewDetaiLeaveDto()
            {
                Id = meeting.Id,
                AttendanceList = listAttendanceDto,
                Date = meeting.Date,
                Location= meeting.Location,
                Notes = meeting.Notes,
                Task= meeting.Task,
            };
        }


    }
}
