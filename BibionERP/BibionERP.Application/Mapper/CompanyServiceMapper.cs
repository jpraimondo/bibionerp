﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Application.Mapper
{
    internal class CompanyServiceMapper
    {
        internal static CompanyService ToCompanyService(CreateCompanyServiceDto createCompanyService)
        {
            return new CompanyService
            {
                CompanyId = createCompanyService.CompanyId,
                ServiceId = createCompanyService.ServiceId,
                StartDate = createCompanyService.StartDate,
                EndDate = createCompanyService.EndDate,
                TotalCost = createCompanyService.TotalCost,
                DiscountPercentage = createCompanyService.DiscountPercentage,
                FinalPrice = createCompanyService.FinalPrice,
                RepeatDays = createCompanyService.RepeatDays,
                HoursWeek = createCompanyService.HoursWeek,
            };
        }

        internal static ViewCompanyServiceDto ToViewCompanyServiceDto(CompanyService companyService)
        {

            return new ViewCompanyServiceDto
            {
                CompanyId = companyService.CompanyId,
                ServiceId = companyService.ServiceId,
                ServiceName = companyService.Service.Name,
                StartDate = companyService.StartDate,
                EndDate = companyService.EndDate,
                TotalCost = companyService.TotalCost,
                DiscountPercentage = companyService.DiscountPercentage,
                FinalPrice = companyService.FinalPrice,
                RepeatDays = companyService.RepeatDays,
                HoursWeek = companyService.HoursWeek,

            };

        }


    }
}
