﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Domain.Identity;

namespace BibionERP.Application.Mapper
{
    public class AuthMapper
    {

        public static AuthRequest ToAuthRequest(CreateAuthDto authDto)
        {
            return new AuthRequest
            {
                Email = authDto.Email,
                Password = authDto.Password
            };
        }

        public static RegistrationRequest ToRegisterRequest(RegisterAuthDto authDto)
        {
            return new RegistrationRequest
            {
                FirstName = authDto.Nombre,
                LastName = authDto.Apellido,
                EmployeeId = authDto.EmployeeId,
                Email = authDto.Email,
                Password = authDto.Password
            };
        }

    }
}
