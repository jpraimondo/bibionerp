﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class SettingsMapper
    {
        internal static Settings ToSettings(CreateSettingsDto createSettings)
        {
            return new Settings
            {
                CompanyName= createSettings.CompanyName,

                CompanyType= createSettings.CompanyType,
                
                CompanyDescription= createSettings.CompanyDescription,
                
                CompanyPhoneNumber= createSettings.CompanyPhoneNumber,
                
                CompanyEmail= createSettings.CompanyEmail,  
                
                CompanyDirection= createSettings.CompanyDirection,
                
                CompanyLocation= createSettings.CompanyLocation,
                
                CompanyImageLogo= createSettings.CompanyImageLogo,
                
                CompanyImageTitle= createSettings.CompanyImageTitle,
                
                CompanyWebTitle= createSettings.CompanyWebTitle,
            };

        }

        internal static ViewDetailSettingsDto ToViewDetail(Settings settings)
        {
            return new ViewDetailSettingsDto
                {

                Id= settings.Id,

                CompanyName = settings.CompanyName,

                CompanyType = settings.CompanyType,
                
                CompanyDescription = settings.CompanyDescription,
                
                CompanyPhoneNumber = settings.CompanyPhoneNumber,
                
                CompanyEmail = settings.CompanyEmail,  
                
                CompanyDirection = settings.CompanyDirection,
                
                CompanyLocation = settings.CompanyLocation,
                
                CompanyImageLogo = settings.CompanyImageLogo,
                
                CompanyImageTitle = settings.CompanyImageTitle,
                
                CompanyWebTitle = settings.CompanyWebTitle,

            };
        }

    }
}
