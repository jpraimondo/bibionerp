﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class CustomerMapper
    {

        internal static Customer ToCustomer(CreateCustomerDto createCustomerDto)
        {
            return new Customer()
            {
                FirstName= createCustomerDto.FirstName.Trim(),
                LastName= createCustomerDto.LastName.Trim(),
                CustomerType= createCustomerDto.CustomerType.Trim(),
                Email= createCustomerDto.Email.Trim(),
                Phone= createCustomerDto.Phone.Trim(),
                Address= createCustomerDto.Address.Trim(),
                Notes= createCustomerDto.Notes.Trim(),
            };
        }

        internal static ViewCustomerDto ToViewCustomerDto(Customer customer)
        {
            return new ViewCustomerDto()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Phone= customer.Phone,
            };
        }

        internal static ViewDetailCustomerDto ToViewDetailCustomerDto(Customer customer)
        {
            return new ViewDetailCustomerDto()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                CustomerType= customer.CustomerType,
                Phone= customer.Phone,
                Email = customer.Email,
                Address= customer.Address,
                Notes= customer.Notes,
            };
        }
    }
}
