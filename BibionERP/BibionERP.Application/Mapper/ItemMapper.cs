﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class ItemMapper
    {
        internal static Item ToItem(CreateItemDto createitemDto)
        {
            return new Item()
            {
                Name = createitemDto.Name.Trim(),
                Description = createitemDto.Description.Trim(),
                CostPrice = createitemDto.CostPrice,
                TradePrice = createitemDto.TradePrice,
                CategoryItemId = createitemDto.CategoryId,
                SupplierId = createitemDto.SupplierId,
                Code = createitemDto.Code.Trim(),
                ManufactureDate = createitemDto.ManufactureDate.Value.ToDateTime(TimeOnly.MinValue),
                ExpirationDate = createitemDto.ExpirationDate.Value.ToDateTime(TimeOnly.MinValue),
                Note = createitemDto.Note.Trim(),
            };
        }

        internal static ViewItemDto ToViewItemDto(Item item)
        {
            return new ViewItemDto()
            {
                Id = item.Id,
                Name = item.Name,
                Code = item.Code,
                SupplierName = item.Supplier.Name,
                TradePrice = item.TradePrice,
                Quantity = item.WarehouseItem.Select(x => x.Quantity).Sum()
            };
        }

        internal static ViewDetailItemDto ToViewDetailItemDto(Item item)
        {
            return new ViewDetailItemDto()
            {
                Id = item.Id,
                Name = item.Name,
                Description= item.Description,
                CostPrice = item.CostPrice,
                TradePrice = item.TradePrice,
                Code= item.Code,
                ManufactureDate = DateOnly.FromDateTime(item.ManufactureDate.Value),
                ExpirationDate = DateOnly.FromDateTime(item.ExpirationDate.Value),
                Note = item.Note,
            };
        }

    }
}
