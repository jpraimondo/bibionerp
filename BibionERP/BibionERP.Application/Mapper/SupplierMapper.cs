﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class SupplierMapper
    {
        internal static Supplier ToSupplier(CreateSupplierDto createSupplierDto)
        {
            return new Supplier()
            {
                Name = createSupplierDto.Name.Trim(),
                Document=createSupplierDto.Document.Trim(),
                ContactPerson=createSupplierDto.ContactPerson.Trim(),
                Email=createSupplierDto.Email.Trim(),
                Phone=createSupplierDto.Phone.Trim(),
                Address=createSupplierDto.Address.Trim(),
                Location=createSupplierDto.Location.Trim(),
            };
        }

        internal static ViewSupplierDto ToViewSupplierDto(Supplier supplier)
        {
            return new ViewSupplierDto()
            {
                Id= supplier.Id,
                Name= supplier.Name,
                ContactPerson=supplier.ContactPerson,
                Email=supplier.Email,
            };
        }

        internal static ViewDetailSupplierDto ToViewDetailSupplierDto(Supplier supplier)
        {
            return new ViewDetailSupplierDto()
            {
                Id = supplier.Id,
                Name = supplier.Name,
                Document=supplier.Document,
                ContactPerson = supplier.ContactPerson,
                Email = supplier.Email,
                Phone=supplier.Phone,
                Address = supplier.Address,
                Location=supplier.Location,
            };
        }
    }
}
