﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class DeliveryNoteDetailMapper
    {
        internal static DeliveryNoteDetail ToDeliveryNote(CreateDeliveryNoteDetailDto createDeliveryNoteDetailDto)
        {
            return new DeliveryNoteDetail()
            {
                DeliveryNoteId = createDeliveryNoteDetailDto.DeliveryNoteId,
                ItemId = createDeliveryNoteDetailDto.ItemId,
                Quantity= createDeliveryNoteDetailDto.Quantity,
            }; 
        }


        internal static ViewDetailDeliveryNoteDetailDto ToViewDeliveryNoteDetail(DeliveryNoteDetail deliveryNoteDetail)
        {
            return new ViewDetailDeliveryNoteDetailDto()
            {
                Id = deliveryNoteDetail.DeliveryNoteId,
                ItemId = deliveryNoteDetail.ItemId,
                ItemName = deliveryNoteDetail.Item.Name,
                Quantity = deliveryNoteDetail.Quantity,
            };
        }
    }
}
