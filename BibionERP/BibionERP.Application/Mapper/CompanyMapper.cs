﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class CompanyMapper
    {
        internal static Company ToCompany(CreateCompanyDto createCompanyDto)
        {
            return new Company()
            {
                Name = createCompanyDto.Name.Trim(),
                Description = createCompanyDto.Description.Trim(),
                Document = createCompanyDto.Document.Trim(),
                Email = createCompanyDto.Email.Trim(),
                Address = createCompanyDto.Address.Trim(),
                Phone = createCompanyDto.Phone.Trim(),
            };
        }

        internal static ViewCompanyDto ToViewCompanyDto(Company company)
        {
            return new ViewCompanyDto()
            {
                Id = company.Id,
                Name = company.Name,
                Document = company.Document,
                Email = company.Email,
            };
        }

        internal static ViewDetailCompanyDto ToViewDetailCompanyDto(Company company)
        {
            return new ViewDetailCompanyDto()
            {
                Id = company.Id,
                Name = company.Name,
                Description = company.Description,
                Document = company.Document,
                Address= company.Address,
                Email = company.Email,
                Phone= company.Phone,
               
            };
        }


    }
}
