﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class WarehouseMapper
    {

        internal static Warehouse ToWarehouse (CreateWarehouseDto createWarehouseDto)
        {
            return new Warehouse()
            {
                Name= createWarehouseDto.Name.Trim(),
                Description= createWarehouseDto.Description.Trim(),
            };
        }

        internal static ViewWarehouseDto ToViewWarehouseDto(Warehouse warehouse)
        {
            return new ViewWarehouseDto()
            {
                Id= warehouse.Id,
                Name= warehouse.Name,
            };
        }

        internal static ViewDetailWarehouseDto ToViewDetailWarehouseDto(Warehouse warehouse)
        {
            return new ViewDetailWarehouseDto()
            {
                Id = warehouse.Id,
                Name = warehouse.Name,
                Description= warehouse.Description,
            };
        }

    }
}
