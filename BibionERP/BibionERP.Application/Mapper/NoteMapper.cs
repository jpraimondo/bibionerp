﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Domain.Entities;


namespace BibionERP.Application.Mapper
{
    internal class NoteMapper
    {
        internal static Note ToNote(CreateNoteDto createNoteDto)
        {
            string tags = string.Empty;

            if(createNoteDto.Tags.Any())
            {
                tags = string.Join(",", createNoteDto.Tags);
            }

            return new Note()
            {
                Title = createNoteDto.Title,
                Message = createNoteDto.Message,
                IsTask = createNoteDto.IsTask,
                IsCompleted = createNoteDto.IsCompleted,
                Tags = tags
            };
        }


        internal static ViewNoteDto ToViewNoteDto(Note note)
        {
            return new ViewNoteDto()
            {
                Id = note.Id,
                UserSenderId = note.UserSenderId,
                UserSenderName = $"{note.UserSender.FirstName} {note.UserSender.LastName}",
                UserReceiverId = note.UserReceiverId,
                UserReceiverName = $"{note.UserReceiver.FirstName} {note.UserReceiver.LastName}",
                Title = note.Title,
                Date = note.Date,
                Message = note.Message,
                IsTask = note.IsTask,
                IsCompleted = note.IsCompleted,
                Tags = new List<string>(note.Tags.Split(','))
        };
        }

    }
}
