﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Application.Mapper
{
    public class SocialMapper
    {

        internal static Social ToSocial(CreateSocialDto createSocial)
        {
            return new Social()
            {
                Name = createSocial.Name,
                URL = createSocial.URL,
                Icons = createSocial.Icons,
                Visible = createSocial.Visible,
            };
        }

        internal static ViewSocialDto ToViewSocialDto(Social social)
        {
            return new ViewSocialDto()
            {
                Id= social.Id,
                Name= social.Name,
                URL= social.URL,
                Icons= social.Icons,
                Visible= social.Visible,
            };
        }

    }
}
