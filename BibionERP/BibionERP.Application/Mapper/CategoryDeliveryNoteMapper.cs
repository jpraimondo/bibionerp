﻿using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class CategoryDeliveryNoteMapper
    {

        internal static ViewDetailCategoryDeliveryNote ToCategoryDeliveryNote(CategoryDeliveryNote categoryDeliveryNote)
        {
            return new ViewDetailCategoryDeliveryNote()
            {
                Id = categoryDeliveryNote.Id,
                Name = categoryDeliveryNote.Name.Trim(),
                Description = categoryDeliveryNote.Description.Trim(),
                addItem = categoryDeliveryNote.AddItem

            };
        }
    }
}
