﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using BibionERP.Domain.Enums;

namespace BibionERP.Application.Mapper
{
    internal class HolidayMapper
    {
        internal static Holiday ToHoliday(CreateHolidayDto holidayDto)
        {
            return new Holiday
            {
                EmployeeId = holidayDto.EmployeeId,
                StartHoliday = holidayDto.StartLeave,
                HolidayType = (HolidayType)holidayDto.HolidayTypeId,
                EndHoliday = holidayDto.EndLeave,
                ApproverId = holidayDto.ApproverId,
                Details = holidayDto.Details,

            };
        }

        internal static ViewHolidayDto ToViewHolidayDto(Holiday vacation)
        {
            return new ViewHolidayDto
            {
                Id = vacation.Id,
                EmployeeName = $"{vacation.Employee.FirstName} {vacation.Employee.LastName}",
                HolidayType = vacation.HolidayType.ToString(),
                StartHoliday = vacation.StartHoliday,
                EndHoliday = vacation.EndHoliday,
                ApproverName = $"{vacation.Approver.FirstName} {vacation.Approver.LastName}",
                HolidayStatus = vacation.HolidayStatus.ToString(),

            };
        }

        internal static ViewDetailHolidayDto ToViewDetailHolidayDto(Holiday holiday)
        {
            return new ViewDetailHolidayDto
            {
                Id = holiday.Id,
                HolidayType = holiday.HolidayType.ToString(),
                EmployeeName = $"{holiday.Employee.FirstName} {holiday.Employee.LastName}",
                StartHoliday = holiday.StartHoliday,
                EndHoliday = holiday.EndHoliday,
                ApproverName = $"{holiday.Approver.FirstName} {holiday.Approver.LastName}",
                HolidayStatus = holiday.HolidayStatus.ToString(),
                Details = holiday.Details,
            };
        }
    }
}
