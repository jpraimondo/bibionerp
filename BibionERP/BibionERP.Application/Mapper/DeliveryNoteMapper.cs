﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class DeliveryNoteMapper
    {
        internal static DeliveryNote ToDeliveryNote(CreateDeliveryNoteDto createDeliveryNoteDto)
        {
            return new DeliveryNote()
            {
                EmployeeId = createDeliveryNoteDto.EmployeeId,
                CategoryDeliveryNoteId = createDeliveryNoteDto.CategoryId,
                Date = createDeliveryNoteDto.Date,
                StartDate= createDeliveryNoteDto.StartDate.Value.ToDateTime(TimeOnly.MinValue),
                EndDate= createDeliveryNoteDto.EndDate.Value.ToDateTime(TimeOnly.MinValue),
                WarehouseId = createDeliveryNoteDto.WarehouseId,
                Location= createDeliveryNoteDto.Location.Trim(),
                Note= createDeliveryNoteDto.Note.Trim(),
            };

        }


        internal static ViewDeliveryNoteDto ToViewDeliveryNote(DeliveryNote deliveryNote) {

            return new ViewDeliveryNoteDto()
            {
                Id = deliveryNote.Id,
                EmployeeName = $"{deliveryNote.Employee?.FirstName} {deliveryNote.Employee?.LastName}",
                CategoryName = deliveryNote.CategoryDeliveryNote?.Name,
                Date = deliveryNote.Date,
                Location = deliveryNote.Location,
            };
        }

        internal static ViewDetailDeliveryNoteDto ToViewDetailDeliveryNote(DeliveryNote deliveryNote)
        {

            return new ViewDetailDeliveryNoteDto()
            {
                Id = deliveryNote.Id,
                EmployeeName = $"{deliveryNote.Employee?.FirstName} {deliveryNote.Employee?.LastName}",
                CategoryName = deliveryNote.CategoryDeliveryNote?.Name,
                Date = deliveryNote.Date,
                StartDate= DateOnly.FromDateTime(deliveryNote.StartDate.Value),
                EndDate= DateOnly.FromDateTime(deliveryNote.EndDate.Value),
                Location = deliveryNote.Location,
                Note = deliveryNote.Note,
            };
        }


    }
}
