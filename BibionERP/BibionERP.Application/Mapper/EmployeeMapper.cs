﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class EmployeeMapper
    {
        internal static Employee ToEmployee (CreateEmployeeDto createEmployeeDto)
        {
            return new Employee()
            {
                FirstName = createEmployeeDto.FirstName.Trim(),
                LastName = createEmployeeDto.LastName.Trim(),
                Document = createEmployeeDto.Document.Trim(),
                Birthday = createEmployeeDto.Birthday.Value.ToDateTime(TimeOnly.MinValue),
                Designature = createEmployeeDto.Designature.Trim(),
                Section = createEmployeeDto.Section.Trim(),
                JoiningDate = createEmployeeDto.JoiningDate.Value.ToDateTime(TimeOnly.MinValue),
                LeavingDate = createEmployeeDto.LeavingDate.Value.ToDateTime(TimeOnly.MinValue),
                Phone = createEmployeeDto.Phone.Trim(),
                Email = createEmployeeDto.Email.Trim(),
                Address = createEmployeeDto.Address.Trim(),
                Location = createEmployeeDto.Location.Trim(),
            };
        }


        internal static ViewEmployeeDto ToViewEmployeeDto(Employee employee)
        {
            return new ViewEmployeeDto()
            {
                Id= employee.Id,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Phone = employee.Phone,
                Document = employee.Document,
                Email = employee.Email,
            };
        }

        internal static ViewDetailEmployeeDto ToViewDetailEmployeeDto(Employee employee)
        {
            return new ViewDetailEmployeeDto()
            {
                Id = employee.Id,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Document = employee.Document,
                Birthday = DateOnly.FromDateTime(employee.Birthday.Value),
                Designature = employee.Designature,
                Section = employee.Section,
                JoiningDate = DateOnly.FromDateTime(employee.JoiningDate.Value),
                LeavingDate = DateOnly.FromDateTime(employee.LeavingDate.Value),
                Phone = employee.Phone,
                Email = employee.Email,
                Address = employee.Address,
                Location = employee.Location,
            };
        }


    }
}
