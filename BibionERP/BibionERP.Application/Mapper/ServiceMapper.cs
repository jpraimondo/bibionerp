﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;

namespace BibionERP.Application.Mapper
{
    internal class ServiceMapper
    {
        internal static Service ToService(CreateServiceDto createServiceDto)
        {
            return new Service()
            {
                Name= createServiceDto.Name.Trim(),
                Description= createServiceDto.Description.Trim(),
                CostPrice= createServiceDto.CostPrice,
                TotalPrice= createServiceDto.TotalPrice,
            };
        }

        internal static ViewServiceDto ToViewServiceDto(Service service)
        {
            return new ViewServiceDto()
            {
                Id= service.Id,
                Name= service.Name,
                Description= service.Description,
                TotalPrice= service.TotalPrice,
            };
        }

        internal static ViewDetailServiceDto ToViewDetailServiceDto(Service service)
        {
            return new ViewDetailServiceDto()
            {
                Id = service.Id,
                Name = service.Name,
                Description = service.Description,
                CostPrice= service.CostPrice,
                TotalPrice = service.TotalPrice,
            };
        }

    }
}
