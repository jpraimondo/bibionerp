﻿using BibionERP.Application.DTOs.Create;
using BibionERP.Application.DTOs.View;
using BibionERP.Application.DTOs.ViewDetail;
using BibionERP.Domain.Entities;
using BibionERP.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibionERP.Application.Mapper
{
    public class HolidayEmployeeMapper
    {
        internal static HolidayEmployee ToHoliday(CreateHolidayEmployeeDto holidayDto)
        {
            return new HolidayEmployee
            {
                HolidayName = holidayDto.HolidayName,
                EmployeeId = holidayDto.EmployeeId,
                HolidayExpiration = holidayDto.HolidayExpiration,
                HolidayType = (HolidayType)holidayDto.HolidayTypeId,
                HolidayDays = holidayDto.HolidayDays
                
            };
        }

        internal static ViewHolidayEmployeeDto ToViewHolidayDto(HolidayEmployee holidayEmployee)
        {
            return new ViewHolidayEmployeeDto
            {
                Id = holidayEmployee.Id,
                HolidayName = holidayEmployee.HolidayName,
                EmployeeName = $"{holidayEmployee.Employee.FirstName} {holidayEmployee.Employee.LastName}",
                HolidayType = holidayEmployee.HolidayType.ToString(),
                HolidayExpiration = holidayEmployee.HolidayExpiration,
                RequiredDocumentation = holidayEmployee.RequiredDocumentation,
                HolidayDays = holidayEmployee.HolidayDays,
        
            };
        }

        internal static ViewDetailHolidayDto ToViewDetailHolidayDto(Holiday holiday)
        {
            return new ViewDetailHolidayDto
            {
                Id = holiday.Id,
                HolidayType = holiday.HolidayType.ToString(),
                EmployeeName = $"{holiday.Employee.FirstName} {holiday.Employee.LastName}",
                StartHoliday = holiday.StartHoliday,
                EndHoliday = holiday.EndHoliday,
                ApproverName = $"{holiday.Approver.FirstName} {holiday.Approver.LastName}",
                HolidayStatus = holiday.HolidayStatus.ToString(),
                Details = holiday.Details,
            };
        }
    }
}
